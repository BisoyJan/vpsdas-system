﻿Imports MySql.Data.MySqlClient
Imports System.Data
Public Class Main_Dashboard
    Dim adapter As New MySqlDataAdapter
    Dim pagingDS As DataSet
    Dim scrollVal As Integer
    Dim Program_primary_id As String
    Dim Violation_primary_id As String
    Dim Properties_id As String
    Public Theme_plate_pk As String
    Public StudentName As String
    Public Student_primary_id As String
    Public Case_Primary_id As String
    Public Referral_primary_id As String
    Public Disciplinary_primary_id As String
    Public Disciplinary_action_primary_id As String

    Public row As DataGridViewRow

    Private Sub Main_Dashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Nav_btn_stud.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed
        Dashboard_panel.Visible = True
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Login.Close()
        Dashboard_data.DisplayDashboard()
    End Sub

    Private Sub Nav_btn_rewards_Click(sender As Object, e As EventArgs) Handles Nav_btn_rewards.Click
        Reward_Dashboard.Show()
    End Sub

    Private Sub Nav_btn_stud_Click(sender As Object, e As EventArgs) Handles Nav_btn_stud.Click
        Student_Panel.Visible = True
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayStudents()
    End Sub

    Private Sub Nav_btn_disciplinary_Click(sender As Object, e As EventArgs) Handles Nav_btn_disciplinary.Click
        If Nav_disciplinary_case.Visible = True Or Nav_disciplinary_action.Visible = True And Nav_btn_referral.Visible = True Then
            Nav_disciplinary_case.Visible = False

            If Dashboard_data.AccountType = "Admin" Then
                Nav_disciplinary_case.Visible = False
            End If

            Nav_disciplinary_action.Visible = False
            Nav_btn_referral.Visible = False
            Nav_btn_disciplinary.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Else

            If Dashboard_data.AccountType = "Admin" Then
                Nav_disciplinary_case.Visible = True
            End If

            Nav_disciplinary_action.Visible = True
            Nav_btn_referral.Visible = True
            Nav_btn_disciplinary.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed
            DisplayDisciplinaryAction()
        End If
    End Sub

    Private Sub Nav_btn_Dash_Click(sender As Object, e As EventArgs) Handles Nav_btn_Dash.Click
        Dashboard_panel.Visible = True
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        Dashboard_data.DisplayDashboard()
    End Sub

    Private Sub Nav_btn_file_path_Click(sender As Object, e As EventArgs) Handles Nav_btn_file_path.Click
        Document_filepath_panel.Visible = True
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_file_paths()
    End Sub

    Private Sub Nav_btn_program_Click(sender As Object, e As EventArgs) Handles Nav_btn_program.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = True
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayPrograms()
    End Sub

    Private Sub Nav_btn_offenses_Click(sender As Object, e As EventArgs) Handles Nav_btn_offenses.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = True
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        Displayviolations()
        Displayoffensestype()
    End Sub

    Private Sub Nav_btn_referral_Click(sender As Object, e As EventArgs) Handles Nav_btn_referral.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = True
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayReferral()
    End Sub

    Private Sub Nav_btn_lost_and_found_Click(sender As Object, e As EventArgs) Handles Nav_btn_lost_and_found.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = True
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayProperties()
    End Sub

    Private Sub Nav_disciplinary_action_Click(sender As Object, e As EventArgs) Handles Nav_disciplinary_action.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = True
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayDisciplinaryAction()
    End Sub

    Public Sub ShowDisciplinaryAction()
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = True
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = False
        Document_filepath_panel.Visible = False
        DisplayDisciplinaryAction()
    End Sub

    Private Sub Nav_disciplinary_case_Click(sender As Object, e As EventArgs) Handles Nav_disciplinary_case.Click
        Student_Panel.Visible = False
        Refferal_panel.Visible = False
        Lost_and_found_panel.Visible = False
        Program_panel.Visible = False
        Offenses_panel.Visible = False
        Disciplinary_action_panel.Visible = False
        Dashboard_panel.Visible = False
        Disciplinary_case_Panel.Visible = True
        Document_filepath_panel.Visible = False
        DisplayCase()
    End Sub

    Private Sub Nav_btn_accounts_Click(sender As Object, e As EventArgs) Handles Nav_btn_accounts.Click
        Account_Form.ShowDialog()
    End Sub


    'Start Student Dashboard Code'
    Private Sub Student_add_btn_Click(sender As Object, e As EventArgs) Handles Student_add_btn.Click
        Student_primary_id = 0
        Student_Form.Student_add_btn.Text = "Save"
        Student_Form.ShowDialog()
    End Sub

    Private Sub Student_edit_btn_Click(sender As Object, e As EventArgs) Handles Student_edit_btn.Click
        Student_Form.Student_add_btn.Text = "Edit"
        Student_Form.ShowDialog()
    End Sub

    Public Sub DisableStudentBTN()
        Student_edit_btn.Enabled = False
        Student_delete_btn.Enabled = False
        DisplayStudents()
    End Sub

    Private Sub Student_delete_btn_Click(sender As Object, e As EventArgs) Handles Student_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also deleting related data of this Student", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `students` WHERE id = '" & Student_primary_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableStudentBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub Student_refresh_btn_Click(sender As Object, e As EventArgs) Handles Student_refresh_btn.Click
        DisableStudentBTN()
    End Sub

    Public Sub DisplayStudents()
        If opendb() Then
            Dim query As String = "SELECT students.id as ID, students.student_no as StudentID, students.first_name as Firstname, students.middle_name as Middlename, students.last_name as Lastname, students.age as Age, students.gender as Gender, students.section as Section, programs.abbreviation as Abbreviation, programs.program_name as Program FROM students JOIN programs ON students.program_id=programs.id ORDER BY students.id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            'Dim dtreader As MySqlDataReader
            'Dim table As New DataTable()

            adapter.Fill(pagingDS, scrollVal, 15, "Student_table")

            Try

                'adapter.Fill(table)
                'Student_datagridview.DataSource = table
                Student_datagridview.DataSource = pagingDS
                Student_datagridview.DataMember = "Student_table"


            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Student_prev_btn_Click(sender As Object, e As EventArgs) Handles Student_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Student_table")
    End Sub

    Private Sub Student_next_btn_Click(sender As Object, e As EventArgs) Handles Student_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Student_table")
    End Sub

    Private Sub Student_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Student_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Student_datagridview.Rows(e.RowIndex)
            Student_primary_id = row.Cells("ID").Value.ToString
            Student_edit_btn.Enabled = True
            Student_delete_btn.Enabled = True
        End If
    End Sub

    Private Sub Student_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Student_search_txtbox.TextChange
        Try
            If Student_search_txtbox.Text.Trim(" ") = " " Then
                DisplayStudents()
            Else
                If opendb() Then
                    Dim query As String = "SELECT students.id as ID, students.student_no as StudentID, students.first_name as Firstname, students.middle_name as Middlename, students.last_name as Lastname, students.age as Age, students.gender as Gender, students.section as Section, programs.abbreviation as Abbreviation, programs.program_name as Program FROM students JOIN programs ON students.program_id=programs.id WHERE students.first_name LIKE '%" & Student_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Student_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Student_search_txtbox.Text & "%' OR students.student_no LIKE '%" & Student_search_txtbox.Text & "%' ORDER BY students.id DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Student_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub
    'End Student Dashboard Code

    'Start Program Dashboard Code'
    Private Sub Program_refresh_btn_Click(sender As Object, e As EventArgs) Handles Program_refresh_btn.Click
        DisplayPrograms()
    End Sub
    Private Sub Clear_Program_Textbox()
        Abbreviation_txtbox.Text = ""
        Program_name_txtbox.Text = ""
    End Sub

    Private Sub DisableProgamBTN()
        Program_edit_btn.Enabled = False
        Program_delete_btn.Enabled = False
        DisplayPrograms()
        Clear_Program_Textbox()
    End Sub

    Private Sub DisplayPrograms()
        If opendb() Then
            Dim query As String = "SELECT id as ID, abbreviation as Abbreviation, program_name as Program FROM programs ORDER BY id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Program_table")

            Try
                Program_datagridview.DataSource = pagingDS
                Program_datagridview.DataMember = "Program_table"
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Program_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Program_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Program_datagridview.Rows(e.RowIndex)
            Program_primary_id = row.Cells("ID").Value.ToString
            Program_name_txtbox.Text = row.Cells("Program").Value.ToString
            Abbreviation_txtbox.Text = row.Cells("Abbreviation").Value.ToString
            Program_edit_btn.Enabled = True
            Program_delete_btn.Enabled = True
        End If
    End Sub

    Private Sub Program_add_btn_Click(sender As Object, e As EventArgs) Handles Program_add_btn.Click
        If Abbreviation_txtbox.Text.Trim("") = "" Or Program_name_txtbox.Text.Trim("") = "" Then
            MsgBox("Please input all fields")
        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `programs`( `abbreviation`, `program_name`) VALUES 
                                   ('" & Abbreviation_txtbox.Text & "', '" & Program_name_txtbox.Text & "')"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")


                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()

                    DisableProgamBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Program_edit_btn_Click(sender As Object, e As EventArgs) Handles Program_edit_btn.Click
        If opendb() Then
            Dim query As String = "UPDATE `programs` SET `program_name` = '" & Program_name_txtbox.Text & "', `abbreviation`= '" & Abbreviation_txtbox.Text & "' WHERE id = '" & Program_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader
            Try

                dtreader = cmd.ExecuteReader
                MessageBox.Show("Succesfuly Edited.",
                "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()

                DisableProgamBTN()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Program_delete_btn_Click(sender As Object, e As EventArgs) Handles Program_delete_btn.Click

        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this program", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `programs` WHERE id = '" & Program_primary_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()

                    DisableProgamBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Program_next_btn_Click(sender As Object, e As EventArgs) Handles Program_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Program_table")
    End Sub

    Private Sub Program_prev_btn_Click(sender As Object, e As EventArgs) Handles Program_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Program_table")
    End Sub

    Private Sub Program_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Program_search_txtbox.TextChange
        Try
            If Program_search_txtbox.Text.Trim("") = "" Then
                DisplayPrograms()
            Else
                If opendb() Then
                    Dim query As String = "SELECT id as ID, abbreviation as Abbreviation, program_name as Program FROM programs WHERE abbreviation LIKE '%" & Program_search_txtbox.Text & "%' OR program_name LIKE '%" & Program_search_txtbox.Text & "%' ORDER BY id DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Program_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub
    'End Program Dashbord Code'

    'Start Violations Dashboard Code'
    Private Sub Displayoffensestype()
        If opendb() Then
            Dim query As String = "SELECT * FROM `offenses`"

            Dim adapter As New MySqlDataAdapter(query, conn)
            Dim table As New DataTable()

            Try
                adapter.Fill(table)

                Offenses_drpdown.DataSource = table
                Offenses_drpdown.DisplayMember = "offense"
                Offenses_drpdown.ValueMember = "id"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Displayviolations()
        If opendb() Then
            Dim query As String = "SELECT
                violations.id AS ID,
                offenses.offense AS Category,
                violations.code AS CODE,
                violations.violation AS Violation
            FROM
                violations
            JOIN offenses ON violations.offenses_id = offenses.id
            ORDER BY
                violations.id
            DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            Try
                adapter.Fill(pagingDS, scrollVal, 15, "Violations_table")


                Violence_datagridview.DataSource = pagingDS
                Violence_datagridview.DataMember = "Violations_table"


            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Violations_refresh_btn_Click(sender As Object, e As EventArgs) Handles Violations_refresh_btn.Click
        Displayviolations()
    End Sub

    Private Sub Clear_violations_textbox()
        Offense_code_txtbox.Text = ""
        Offenses_violation_txtbox.Text = ""
    End Sub

    Private Sub DisableViolationBTN()
        Violation_edit_btn.Enabled = False
        Violation_delete_btn.Enabled = False
        Clear_violations_textbox()
        Displayviolations()
    End Sub

    Private Sub Violation_prev_btn_Click(sender As Object, e As EventArgs) Handles Violation_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Violations_table")
    End Sub

    Private Sub Violation_next_btn_Click(sender As Object, e As EventArgs) Handles Violation_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Violations_table")
    End Sub

    Private Sub Violence_datagridview_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles Violence_datagridview.CellContentClick
        If e.RowIndex >= 0 Then
            row = Me.Violence_datagridview.Rows(e.RowIndex)
            Violation_primary_id = row.Cells("ID").Value.ToString
            Offenses_drpdown.SelectedIndex = Offenses_drpdown.FindStringExact(row.Cells("Category").Value.ToString)
            Offense_code_txtbox.Text = row.Cells("Code").Value.ToString
            Offenses_violation_txtbox.Text = row.Cells("Violation").Value.ToString
            Violation_edit_btn.Enabled = True
            Violation_delete_btn.Enabled = True
        End If
    End Sub

    Private Sub Violation_add_btn_click(sender As Object, e As EventArgs) Handles Violation_add_btn.Click
        If Offense_code_txtbox.Text.Trim("") = "" Or Offenses_violation_txtbox.Text.Trim("") = "" Then
            MsgBox("please input all fields")
        Else
            If opendb() Then
                Dim query As String = "insert into `violations`( `offenses_id`, `code`, `violation`) values 
                                   ('" & Offenses_drpdown.SelectedValue & "', '" & Offense_code_txtbox.Text & "', '" & Offenses_violation_txtbox.Text & "' )"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("succesfuly added.",
                    "important message")


                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableViolationBTN()
                End Try
            Else
                MsgBox("no database connections")
            End If
        End If
    End Sub

    Private Sub Violation_edit_btn_Click(sender As Object, e As EventArgs) Handles Violation_edit_btn.Click
        If opendb() Then
            Dim query As String = "UPDATE `violations` SET `offenses_id` = '" & Offenses_drpdown.SelectedValue & "', `code`= '" & Offense_code_txtbox.Text & "', `violation`= '" & Offenses_violation_txtbox.Text & "' WHERE id = '" & Violation_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader
            Try

                dtreader = cmd.ExecuteReader
                MessageBox.Show("Succesfuly Edited.",
                "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                DisableViolationBTN()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Violation_delete_btn_Click(sender As Object, e As EventArgs) Handles Violation_delete_btn.Click

        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this Violation", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `violations` WHERE id = '" & Violation_primary_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                    "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableViolationBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub Violations_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Violations_search_txtbox.TextChange
        Try
            If Violations_search_txtbox.Text.Trim(" ") = " " Then
                DisplayPrograms()
            Else
                If opendb() Then
                    Dim query As String = "SELECT violations.id as ID, offenses.offense as Category, violations.code as Code, violations.violation as Violation FROM violations JOIN offenses ON violations.offenses_id=offenses.id WHERE violations.code LIKE '" & Violations_search_txtbox.Text & "%' OR violations.violation LIKE '" & Violations_search_txtbox.Text & "%' OR offenses.offense LIKE '" & Violations_search_txtbox.Text & "%' ORDER BY offenses.id DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try
                        adapter.Fill(table)
                        Violence_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub
    'End Violations Dashboard Code'

    'Start Properties Dashboard Code'
    Public Sub DisablePropertiesBTN()
        Properties_edit_btn.Enabled = False
        Properties_delete_btn.Enabled = False
        Properties_view_btn.Enabled = False
        DisplayProperties()
    End Sub

    Private Sub Properties_add_btn_Click(sender As Object, e As EventArgs) Handles Properties_add_btn.Click
        Lost_and_found_Form.Properties_primary_id = "0"
        Lost_and_found_Form.Properties_save_btn.Text = "Save"
        Lost_and_found_Form.ShowDialog()
    End Sub

    Private Sub Properties_edit_btn_Click(sender As Object, e As EventArgs) Handles Properties_edit_btn.Click
        Lost_and_found_Form.Properties_save_btn.Text = "Edit"
        Lost_and_found_Form.ShowDialog()
    End Sub

    Private Sub Properties_refresh_btn_Click(sender As Object, e As EventArgs) Handles Properties_refresh_btn.Click
        DisplayProperties()
    End Sub

    Private Sub Properties_view_btn_Click(sender As Object, e As EventArgs) Handles Properties_view_btn.Click

        Lost_and_found_Display.ShowDialog()
    End Sub

    Private Sub Properties_prev_btn_Click(sender As Object, e As EventArgs) Handles Properties_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Lost_and_found_table")
    End Sub

    Private Sub Properties_next_btn_Click(sender As Object, e As EventArgs) Handles Properties_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Lost_and_found_table")
    End Sub

    Public Sub DisplayProperties()
        If opendb() Then
            Dim query As String = "SELECT id, student_id as ReturneeID, retrieval_id as RetrievalID, date_found as DateFound, date_retrieved as DateRetrieved, date_surrendered as Surrendered, type as Type, description as Description, remarks as Remarks FROM properties ORDER BY properties.id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Lost_and_found_table")

            Try

                Lost_and_found_datagridview.DataSource = pagingDS
                Lost_and_found_datagridview.DataMember = "Lost_and_found_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Lost_and_found_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Lost_and_found_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Lost_and_found_datagridview.Rows(e.RowIndex)
            Lost_and_found_Display.DisplayLostItemPrimaryKey = row.Cells("id").Value.ToString
            Lost_and_found_Form.Properties_primary_id = row.Cells("id").Value.ToString
            Properties_id = row.Cells("id").Value.ToString
            Properties_edit_btn.Enabled = True
            Properties_delete_btn.Enabled = True
            Properties_view_btn.Enabled = True
        End If
    End Sub

    Private Sub Properties_delete_btn_Click(sender As Object, e As EventArgs) Handles Properties_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?", "Confirmation Dialog", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `properties` WHERE id = '" & Properties_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
            "Important Message")
                    DisplayProperties()
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisplayPrograms()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Properties_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Properties_search_txtbox.TextChange
        Try
            If Properties_search_txtbox.Text.Trim("") = "" Then
                DisplayPrograms()
            Else
                If opendb() Then
                    Dim query As String = "SELECT properties.id, students.student_no as ReturneeID, properties.retrieval_id as RetrievalID, programs.program_name as Program, properties.date_found as Found, properties.date_retrieved as Retrieved, properties.date_surrendered as Surrendered, properties.type as Type, properties.description as Description, properties.remarks as Remarks FROM properties JOIN students ON properties.student_id=students.id JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Properties_search_txtbox.Text & "%' OR students.first_name LIKE '%" & Properties_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Properties_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Properties_search_txtbox.Text & "%' ORDER BY properties.id DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Lost_and_found_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub
    'End Properties Dashboard Code'


    'Start Referral Dashboard Code'
    Private Sub Referral_refresh_btn_Click(sender As Object, e As EventArgs) Handles Referral_refresh_btn.Click
        DisableReferralBTN()
    End Sub

    Private Sub Referral_prev_btn_Click(sender As Object, e As EventArgs) Handles Referral_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Referral_table")
    End Sub

    Private Sub Referral_next_btn_Click(sender As Object, e As EventArgs) Handles Referral_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Referral_table")
    End Sub

    Public Sub DisableReferralBTN()
        Referral_edit_btn.Enabled = False
        Referral_delete_btn.Enabled = False
        Referral_view_btn.Enabled = False
        Referral_action_btn.Enabled = False
        DisplayReferral()
    End Sub
    Private Sub Referral_action_btn_Click(sender As Object, e As EventArgs) Handles Referral_action_btn.Click
        Case_form.Case_save_btn.Text = "Save"
        Disciplinary_form.ShowDialog()
    End Sub

    Private Sub Referral_add_btn_Click(sender As Object, e As EventArgs) Handles Referral_add_btn.Click
        Referral_form.referral_primary_id = 0
        Referral_form.Referral_save_btn.Text = "Save"
        Referral_form.ShowDialog()
    End Sub

    Private Sub Referral_edit_btn_Click(sender As Object, e As EventArgs) Handles Referral_edit_btn.Click
        Referral_form.Referral_save_btn.Text = "Edit"
        Referral_form.ShowDialog()
    End Sub

    Private Sub Referral_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Referral_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Referral_datagridview.Rows(e.RowIndex)
            Referral_form.referral_primary_id = row.Cells("ID").Value.ToString
            Referral_primary_id = row.Cells("ID").Value.ToString
            Referral_edit_btn.Enabled = True
            Referral_delete_btn.Enabled = True
            Referral_view_btn.Enabled = True
            Referral_action_btn.Enabled = True
        End If
    End Sub

    Private Sub DisplayReferral()
        If opendb() Then
            Dim query As String = "SELECT
                referrals.id as ID,
                students.student_no as StudentID,
                students.first_name as FirstName,
                students.middle_name as MiddleName,
                students.last_name as LastName,
                students.age as Age,
                students.gender as Gender,
                students.section as Section,
                programs.abbreviation AS Program,
                violations.code as ViolationCode,
  	            employee_name as EmployeeName,
                referred as Referred,
                date as Date
            FROM
                referrals
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            ORDER BY
                referrals.id
            DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Referral_table")

            Try

                Referral_datagridview.DataSource = pagingDS
                Referral_datagridview.DataMember = "Referral_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Referral_search_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Referral_search_txtbox.KeyPress
        Try
            If Referral_search_txtbox.Text.Trim("") = "" Then
                DisplayReferral()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                    referrals.id as ID,
                    students.student_no as StudentID,
                    students.first_name as FirstName,
                    students.middle_name as MiddleName,
                    students.last_name as LastName,
                    students.age as Age,
                    students.gender as Gender,
                    students.section as Section,
                    programs.abbreviation AS Program,
                    violations.code as ViolationCode,
  	                referrals.employee_name as EmployeeName,
                    referrals.referred as Referred,
                    referrals.date as Date
                FROM
                    referrals
                JOIN students ON referrals.student_id = students.id
                JOIN programs ON students.program_id = programs.id
                JOIN violations ON referrals.violation_id = violations.id
                WHERE
                    students.student_no LIKE '%" & Referral_search_txtbox.Text & "%' OR students.first_name LIKE '%" & Referral_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Referral_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Referral_search_txtbox.Text & "%'
                ORDER BY
                    referrals.id
                DESC
                    "

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Referral_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub Referral_delete_btn_Click(sender As Object, e As EventArgs) Handles Referral_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `referrals` WHERE id = '" & Referral_form.referral_primary_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
            "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableReferralBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Referral_view_btn_Click(sender As Object, e As EventArgs) Handles Referral_view_btn.Click
        Referral_form.ViewReferralCrystal()
        'View_referral()
    End Sub

    Private Sub View_referral()
        If opendb() Then

            Dim query As String = "SELECT
                    referrals.id AS ID,
                    students.id AS StudentID,
                    students.first_name AS FirstName,
                    students.middle_name AS MiddleName,
                    students.last_name AS LastName,
                    students.age AS Age,
                    students.gender AS Gender,
                    students.section AS Section,
                    programs.abbreviation AS Program,
                    violations.violation AS Violations,
                    employee_name AS EmployeeName,
                    referred AS Referred,
                    DATE AS DATE
                FROM
                   `referrals`
                JOIN students ON referrals.student_id = students.id
                JOIN programs ON students.program_id = programs.id
                JOIN violations ON referrals.violation_id = violations.id
                WHERE
	                referrals.id = '" & Referral_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Referral_display_form.Referral_ID = dtreader.GetString("ID")
                    Referral_display_form.Referral_StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    Referral_display_form.Referral_Referred = dtreader.GetString("Referred")
                    Referral_display_form.Referral_EmployeeName = dtreader.GetString("EmployeeName")
                    Referral_display_form.Referral_Description = dtreader.GetString("Violations")
                    Referral_display_form.Referral_WhatDate = dtreader.GetDateTime("Date").ToLongDateString()
                    Referral_display_form.Referral_StudenID = dtreader.GetString("StudentID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Referral_display_form.ShowDialog()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub
    'End Referral Dashboard Code'

    'Start Disciplinary Action Dashboard Code'
    Private Sub Discipline_refresh_btn_Click(sender As Object, e As EventArgs) Handles Discipline_refresh_btn.Click
        DisableDisciplinarActionBTN()
    End Sub

    Private Sub Discipline_counsel_btn_Click(sender As Object, e As EventArgs) Handles Discipline_counsel_btn.Click
        Case_form.Case_save_btn.Text = "Save"
        Case_form.ShowDialog()
    End Sub


    Public Sub DisableDisciplinarActionBTN()
        Disciplinary_edit_btn.Enabled = False
        Disciplinary_delete_btn.Enabled = False
        Disciplinary_view_btn.Enabled = False
        Discipline_counsel_btn.Enabled = False
        DisplayDisciplinaryAction()
    End Sub

    Private Sub Disciplinary_view_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_view_btn.Click
        Disciplinary_form.ViewActionCrystal()
        'View_Disciplinary_Action()
    End Sub

    Public Sub View_Disciplinary_Action()
        If opendb() Then
            Dim query As String = "SELECT
                disciplinary_action.id,
                students.id AS studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                offenses.offense,
                referrals.referred,
                referrals.employee_name,
                disciplinary_action.committed_date,
                disciplinary_action.committed_time,
                disciplinary_action.counselling_date,
                disciplinary_action.counselling_time,
                disciplinary_action.issual_date
            FROM
                `disciplinary_action`
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                disciplinary_action.id = '" + Disciplinary_primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Disciplinary_action_view_form.Disciplinary_IDStudent = dtreader.GetString("studentID")
                    Disciplinary_action_view_form.Disciplinary_StudentID = dtreader.GetString("studentID")
                    Disciplinary_action_view_form.Disciplinary_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Disciplinary_action_view_form.Disciplinary_Section = dtreader.GetString("section")
                    Disciplinary_action_view_form.Disciplinary_Committed_Date = dtreader.GetDateTime("committed_date")
                    Disciplinary_action_view_form.Disciplinary_Committed_Time = dtreader.GetString("committed_time")
                    Disciplinary_action_view_form.Disciplinary_Employee_name = dtreader.GetString("employee_name")
                    Disciplinary_action_view_form.Disciplinary_Counseling_Time = dtreader.GetString("counselling_time")
                    Disciplinary_action_view_form.Disciplinary_Issued_Date = dtreader.GetDateTime("issual_date")
                    Disciplinary_action_view_form.Disciplinary_Counseling_Date = dtreader.GetDateTime("counselling_date")
                    Disciplinary_action_view_form.Disciplinary_VPSDAS_Name = dtreader.GetString("referred")
                    Disciplinary_action_view_form.Disciplinary_Offense = dtreader.GetString("offense")

                    StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Disciplinary_action_view_form.ShowDialog()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Discipline_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Discipline_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Discipline_datagridview.Rows(e.RowIndex)
            Disciplinary_form.primaryid_discplinary_action = row.Cells("ID").Value.ToString
            Disciplinary_primary_id = row.Cells("ID").Value.ToString
            Disciplinary_action_primary_id = row.Cells("ID").Value.ToString


            Disciplinary_edit_btn.Enabled = True
            Disciplinary_delete_btn.Enabled = True
            Disciplinary_view_btn.Enabled = True
            Discipline_counsel_btn.Enabled = True

        End If
    End Sub

    Private Sub Disciplinary_edit_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_edit_btn.Click
        Disciplinary_form.Disciplinary_save_btn.Text = "Edit"
        Disciplinary_form.ShowDialog()
    End Sub

    Private Sub Disciplinary_delete_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete? Deleting this will also Delete Counselling Report if already existed", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `disciplinary_action` WHERE id = '" & Disciplinary_form.primaryid_discplinary_action & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
            "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableDisciplinarActionBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub DisplayDisciplinaryAction()
        If opendb() Then
            Dim query As String = "SELECT
                disciplinary_action.id AS ID,
                students.student_no AS StudentID,
                students.first_name AS FirstName,
                students.middle_name AS MiddleName,
                students.last_name AS LastName,
                students.section AS Section,
                programs.abbreviation AS Program,
                violations.code AS ViolationCode,
                disciplinary_action.committed_date AS Comitted,
                disciplinary_action.counselling_date as CounsellingDate,
                disciplinary_action.issual_date AS Issued,
                disciplinary_action.remarks AS STATUS
            FROM
                disciplinary_action
            JOIN referrals ON  disciplinary_action.referral_id = referrals.id 
            JOIN students on referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            ORDER BY
                disciplinary_action.id
            DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Discipline_table")

            Try

                Discipline_datagridview.DataSource = pagingDS
                Discipline_datagridview.DataMember = "Discipline_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Disciplinary_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Disciplinary_search_txtbox.TextChange
        Try
            If Disciplinary_search_txtbox.Text.Trim("") = "" Then
                DisplayDisciplinaryAction()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                        disciplinary_action.id AS ID,
                        students.student_no AS StudentID,
                        students.first_name AS FirstName,
                        students.middle_name AS MiddleName,
                        students.last_name AS LastName,
                        students.section AS Section,
                        programs.abbreviation AS Program,
                        violations.code AS ViolationCode,
                        disciplinary_action.committed_date AS Comitted,
                        disciplinary_action.counselling_date as CounsellingDate,
                        disciplinary_action.issual_date AS Issued,
                        disciplinary_action.remarks AS STATUS
                    FROM
                        disciplinary_action
                    JOIN referrals ON  disciplinary_action.referral_id = referrals.id 
                    JOIN students on referrals.student_id = students.id
                    JOIN programs ON students.program_id = programs.id
                    JOIN violations ON referrals.violation_id = violations.id
                    WHERE
                        students.student_no LIKE '%" & Disciplinary_search_txtbox.Text & "%' OR students.first_name LIKE '%" & Disciplinary_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Disciplinary_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Disciplinary_search_txtbox.Text & "%'
                    ORDER BY
                        disciplinary_action.id
                    DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Discipline_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub Disciplinary_next_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Discipline_table")
    End Sub

    Private Sub Disciplinary_prev_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Discipline_table")
    End Sub
    'End Disciplinary Action Dashboard Code'

    'Start Disciplinary Case Dashboard Code'
    Public Sub DisplayCase()
        If opendb() Then
            Dim query As String = "SELECT
                cases.id AS ID,
                referrals.id AS ReferralID,
                students.student_no AS StudentID,
                students.first_name AS FirstName,
                students.middle_name AS MiddleName,
                students.last_name AS LastName,
                programs.abbreviation AS Program,
                students.section AS Section,
                offenses.offense AS Offense,
                cases.recommend AS Recommendation
            FROM
                `cases`
            JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            ORDER BY
                cases.id
            DESC
                "

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Case_table")

            Try

                Case_datagridview.DataSource = pagingDS
                Case_datagridview.DataMember = "Case_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Case_refresh_btn_Click(sender As Object, e As EventArgs) Handles Case_refresh_btn.Click
        DisplayCase()
    End Sub

    Private Sub Case_add_btn_Click(sender As Object, e As EventArgs)
        Case_form.Case_save_btn.Text = "Save"
        Case_form.ShowDialog()
    End Sub

    Private Sub Case_edit_btn_Click(sender As Object, e As EventArgs) Handles Case_edit_btn.Click
        Case_form.Case_save_btn.Text = "Edit"
        Case_form.ShowDialog()
    End Sub

    Public Sub DisableCaseBTN()
        Case_edit_btn.Enabled = False
        Case_view_btn.Enabled = False
        Case_delete_btn.Enabled = False
        DisplayCase()
    End Sub

    Private Sub Case_next_btn_Click(sender As Object, e As EventArgs) Handles Case_next_btn.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Case_table")
    End Sub

    Private Sub Case_prev_btn_Click(sender As Object, e As EventArgs) Handles Case_prev_btn.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Case_table")
    End Sub

    Private Sub Case_delete_btn_Click(sender As Object, e As EventArgs) Handles Case_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `cases` WHERE id = '" & Case_Primary_id & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
            "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableCaseBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Case_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Case_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Case_datagridview.Rows(e.RowIndex)
            Case_Primary_id = row.Cells("ID").Value.ToString
            Case_edit_btn.Enabled = True
            Case_view_btn.Enabled = True
            Case_delete_btn.Enabled = True
        End If
    End Sub

    Private Sub Case_search_txtbox_TextChange(sender As Object, e As EventArgs) Handles Case_search_txtbox.TextChange
        Try
            If Case_search_txtbox.Text.Trim("") = "" Then
                DisplayCase()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                        cases.id AS ID,
                        referrals.id AS ReferralID,
                        students.student_no AS StudentID,
                        students.first_name AS FirstName,
                        students.middle_name AS MiddleName,
                        students.last_name AS LastName,
                        programs.abbreviation AS Program,
                        students.section AS Section,
                        offenses.offense AS Offense,
                        cases.recommend AS Recommendation
                    FROM
                        `cases`
                    JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
                    JOIN referrals ON disciplinary_action.referral_id = referrals.id
                    JOIN violations ON referrals.violation_id = violations.id
                    JOIN students ON referrals.student_id = students.id
                    JOIN programs ON students.program_id = programs.id
                    JOIN offenses ON violations.offenses_id = offenses.id
                    WHERE
                        students.student_no LIKE '%" & Case_search_txtbox.Text & "%' OR students.first_name LIKE '%" & Case_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Case_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Case_search_txtbox.Text & "%'
                    ORDER BY
                        disciplinary_action.id
                    DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Case_datagridview.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Public Sub View_case()
        If opendb() Then
            Dim query As String = "SELECT
                cases.id AS ID,
                disciplinary_action.id AS disciplinaryID,
                students.id as studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.age,
                students.gender,
                violations.code,
                violations.violation,
                programs.abbreviation,
                programs.program_name,
                offenses.offense,
                cases.recommend,
                cases.resolution,
                cases.date,
                cases.chairman,
                cases.members,
                cases.report
            FROM
                `cases`
            JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
            cases.id = '" + Case_Primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Disciplinary_case_view_form.Case_Primarykey = dtreader.GetString("ID")
                    Disciplinary_case_view_form.student_id = dtreader.GetString("studentID")
                    Disciplinary_case_view_form.Case_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Disciplinary_case_view_form.Case_Program_Section = dtreader.GetString("section")
                    Disciplinary_case_view_form.Case_StudentID = dtreader.GetString("student_no")

                    Disciplinary_case_view_form.Case_Violation = dtreader.GetString("violation")

                    Disciplinary_case_view_form.Case_Resolution = dtreader.GetString("resolution")
                    Disciplinary_case_view_form.Case_Incident_Report = dtreader.GetString("report")

                    Disciplinary_case_view_form.Case_members = dtreader.GetString("members")
                    Disciplinary_case_view_form.Case_Chairmain = dtreader.GetString("chairman")

                    Disciplinary_case_view_form.Case_Resolution = dtreader.GetString("recommend")

                    Disciplinary_case_view_form.Case_Date = dtreader.GetDateTime("date")


                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Disciplinary_case_view_form.ShowDialog()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Case_view_btn_Click(sender As Object, e As EventArgs) Handles Case_view_btn.Click
        'View_case()
        Case_form.ViewCounsellingCrystal()
    End Sub
    'End of case code

    Private Sub Document_file_paths()
        If opendb() Then
            Dim query As String = "SELECT
                `id`,
                `name`,
                `path`,
                `save_path`
            FROM
                `themeplate_filepath`
            ORDER BY id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 15, "Document_path")

            Try

                Document_path_datagridview.DataSource = pagingDS
                Document_path_datagridview.DataMember = "Document_path"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub



    Private Sub Edit_document_themeplate_path_btn_Click(sender As Object, e As EventArgs) Handles Edit_document_themeplate_path_btn.Click
        themeplates_file_path_form.save_btn.Text = "Edit"
        themeplates_file_path_form.ShowDialog()
    End Sub

    Private Sub Document_path_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Document_path_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Document_path_datagridview.Rows(e.RowIndex)
            Theme_plate_pk = row.Cells("id").Value.ToString
            Document_delete_btn.Enabled = True
            Edit_document_themeplate_path_btn.Enabled = True
        End If
    End Sub

    Private Sub Add_document_themeplate_btn_Click(sender As Object, e As EventArgs) Handles Add_document_themeplate_btn.Click
        themeplates_file_path_form.save_btn.Text = "Save"
        themeplates_file_path_form.ShowDialog()
    End Sub

    Private Sub Document_settings_refresh_btn_Click(sender As Object, e As EventArgs) Handles Document_settings_refresh_btn.Click
        Document_file_paths()
        disabledocumentpathbtns()
    End Sub

    Private Sub disabledocumentpathbtns()
        Document_delete_btn.Enabled = False
        Edit_document_themeplate_path_btn.Enabled = False
    End Sub

    Private Sub Document_delete_btn_Click(sender As Object, e As EventArgs) Handles Document_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `themeplate_filepath` WHERE id = '" & Theme_plate_pk & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
            "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    disabledocumentpathbtns()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Main_Dashboard_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Login.Show()
    End Sub

    Private Sub Sanction_Chart_Click(sender As Object, e As EventArgs) Handles Sanction_Chart.Click
        Crystal_report_summary.DisplayCrystalReport()
        CrystalReportViewer.ShowDialog()
    End Sub

    Private Sub Rewards_chart_Click(sender As Object, e As EventArgs) Handles Rewards_chart.Click
        Crystal_report_summary.DisplayCrystalAward()
        CrystalReportViewer.ShowDialog()
    End Sub





    'Private Sub student_dashboard_number_Click(sender As Object, e As EventArgs) Handles student_dashboard_number.Click
    '    Excel_export.StudenttoExcel()
    'End Sub

    'Private Sub counsel_dashboard_number_Click(sender As Object, e As EventArgs) Handles counsel_dashboard_number.Click
    '    Excel_export.CounseltoExcel()
    'End Sub

    'Private Sub action_dashboard_number_Click(sender As Object, e As EventArgs) Handles action_dashboard_number.Click

    'End Sub
End Class

