﻿Imports MySql.Data.MySqlClient
Imports System.Data

Public Class File_path_dashboard
    Dim adapter As New MySqlDataAdapter
    Dim pagingDS As DataSet
    Dim scrollVal As Integer

    Public row As DataGridViewRow

    Public Theme_plate_file_path_PK As String
    Public Theme_plate_pk As String

    Private Sub Action_dashboard_btn_Click(sender As Object, e As EventArgs) Handles Action_dashboard_btn.Click
        Case_filepath_dashboard_panel.Visible = False
        Referral_filapath_dashboard_panel.Visible = False
        Action_filepath_dashboard_panel.Visible = True
        Document_filepath_panel.Visible = False
    End Sub
    Private Sub Case_dashboard_btn_Click(sender As Object, e As EventArgs) Handles Case_dashboard_btn.Click
        Action_filepath_dashboard_panel.Visible = False
        Referral_filapath_dashboard_panel.Visible = False
        Document_filepath_panel.Visible = False
        Case_filepath_dashboard_panel.Visible = True
    End Sub

    Private Sub Nav_btn_documents_Click(sender As Object, e As EventArgs) Handles Nav_btn_documents.Click
        Action_filepath_dashboard_panel.Visible = False
        Referral_filapath_dashboard_panel.Visible = False
        Case_filepath_dashboard_panel.Visible = False
        Document_filepath_panel.Visible = True
    End Sub

    Private Sub Referral_dashboard_btn_Click(sender As Object, e As EventArgs) Handles Referral_dashboard_btn.Click
        Action_filepath_dashboard_panel.Visible = False
        Case_filepath_dashboard_panel.Visible = False
        Referral_filapath_dashboard_panel.Visible = True
        Document_filepath_panel.Visible = False
    End Sub

    Public Sub DisplayDocumentPath()
        If opendb() Then
            Dim query As String = "SELECT
                                id AS ID,
                                name AS Name,
                                path AS Path,
                                save_path as SavePath
                            FROM
                                `themeplate_filepath`"

            adapter = New MySqlDataAdapter(query, conn)
            Dim table As New DataTable()

            Try

                adapter.Fill(table)

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Public Sub DisplayCasePath()
        If opendb() Then
            Dim query As String = "SELECT
                                    document_filepath.id AS ID,
                                   students.student_no AS StudentID,
                                    document_filepath.name AS NAME,
                                    document_filepath.path AS PATH
                                FROM
                                    `document_filepath`
                                JOIN students ON document_filepath.student_id = students.id
                                WHERE
                                    document_filepath.name = 'Case'"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Case_table")

            Try

                Case_path_datagridview.DataSource = pagingDS
                Case_path_datagridview.DataMember = "Case_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Public Sub DisplayActionPath()
        If opendb() Then
            Dim query As String = "SELECT
                                    document_filepath.id AS ID,
                                    students.student_no AS StudentID,
                                    document_filepath.name AS NAME,
                                    document_filepath.path AS PATH
                                FROM
                                    `document_filepath`
                               JOIN students ON document_filepath.student_id = students.id
                                WHERE
                                    document_filepath.name = 'Action'"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Action_table")

            Try

                Action_path_datagridview.DataSource = pagingDS
                Action_path_datagridview.DataMember = "Action_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Public Sub DisplayReferralPath()
        If opendb() Then
            Dim query As String = "SELECT
                document_filepath.id AS ID,
                students.student_no AS StudentID,
                document_filepath.name AS NAME,
                document_filepath.path AS PATH
            FROM
             `document_filepath`
            JOIN students ON document_filepath.student_id = students.id
            WHERE
                document_filepath.name = 'Referral'"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Referral_table")

            Try

                Referral_path_datagridview.DataSource = pagingDS
                Referral_path_datagridview.DataMember = "Referral_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub



    Private Sub Case_refresh_btn_Click(sender As Object, e As EventArgs) Handles Case_refresh_btn.Click
        DisplayCasePath()
    End Sub

    Private Sub Action_refresh_btn_Click(sender As Object, e As EventArgs) Handles Action_refresh_btn.Click
        DisplayActionPath()
    End Sub

    Private Sub Referral_refresh_btn_Click(sender As Object, e As EventArgs) Handles Referral_refresh_btn.Click
        DisplayReferralPath()
    End Sub

    Private Sub Document_path_btn_Click(sender As Object, e As EventArgs) Handles Edit_document_themeplate_path_btn.Click
        themeplates_file_path_form.save_btn.Text = "Edit"
        themeplates_file_path_form.ShowDialog()
    End Sub

    Private Sub Document_file_paths()
        If opendb() Then
            Dim query As String = "SELECT
                `id`,
                `name`,
                `path`,
                `save_path`
            FROM
                `themeplate_filepath`
            ORDER BY id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Document_path")

            Try

                Document_path_datagridview.DataSource = pagingDS
                Document_path_datagridview.DataMember = "Document_path"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Document_settings_refresh_btn_Click(sender As Object, e As EventArgs) Handles Document_settings_refresh_btn.Click
        Document_file_paths()
    End Sub

    Private Sub Document_path_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Document_path_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Document_path_datagridview.Rows(e.RowIndex)
            Theme_plate_pk = row.Cells("id").Value.ToString
            Document_delete_btn.Enabled = True
            Edit_document_themeplate_path_btn.Enabled = True
        End If
    End Sub

    Private Sub Add_document_themeplate_btn_Click(sender As Object, e As EventArgs) Handles Add_document_themeplate_btn.Click

    End Sub

End Class