﻿Imports MySql.Data.MySqlClient
Imports System.Data

Public Class Reward_Dashboard
    Dim adapter As New MySqlDataAdapter
    Dim pagingDS As DataSet
    Dim scrollVal As Integer

    Public row As DataGridViewRow

    Private Sub Nav_sports_btn_Click(sender As Object, e As EventArgs) Handles Nav_sports_btn.Click
        If Nav_outstanding_athlete_btn.Visible = True And Nav_mvp_btn.Visible = True Then
            Nav_outstanding_athlete_btn.Visible = False
            Nav_mvp_btn.Visible = False
            Nav_sports_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Else
            Nav_outstanding_athlete_btn.Visible = True
            Nav_mvp_btn.Visible = True
            Nav_sports_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed
        End If
    End Sub

    Private Sub Nav_btn_academic_Click(sender As Object, e As EventArgs) Handles Nav_btn_academic.Click
        If Nav_academic_cum_laude.Visible = True And Nav_academic_honors.Visible = True Then
            Nav_academic_cum_laude.Visible = False
            Nav_academic_honors.Visible = False
            Nav_btn_academic.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Else
            Nav_academic_cum_laude.Visible = True
            Nav_academic_honors.Visible = True
            Nav_btn_academic.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed
        End If
    End Sub

    Private Sub Nav_kindly_act_btn_Click(sender As Object, e As EventArgs) Handles Nav_kindly_act_btn.Click
        Kindly_act_panel.Visible = True
        Honors_panel.Visible = False
        Outstanding_athlete_panel.Visible = False

        Leader_ship_panel.Visible = False
        Cum_laude_panel.Visible = False
        MVP_panel.Visible = False
        DisplayKindlyAct()
    End Sub

    Private Sub Nav_leadership_btn_Click(sender As Object, e As EventArgs) Handles Nav_leadership_btn.Click
        Leader_ship_panel.Visible = True
        Kindly_act_panel.Visible = False
        Honors_panel.Visible = False
        Outstanding_athlete_panel.Visible = False

        Cum_laude_panel.Visible = False
        MVP_panel.Visible = False
        DisplayLeadership()
    End Sub

    Private Sub Nav_academic_cum_laude_Click(sender As Object, e As EventArgs) Handles Nav_academic_cum_laude.Click
        Cum_laude_panel.Visible = True
        Leader_ship_panel.Visible = False
        Kindly_act_panel.Visible = False
        Honors_panel.Visible = False
        Outstanding_athlete_panel.Visible = False

        MVP_panel.Visible = False
        DisplayCumlaude()
    End Sub

    Private Sub Nav_academic_honors_Click(sender As Object, e As EventArgs) Handles Nav_academic_honors.Click
        Honors_panel.Visible = True
        Outstanding_athlete_panel.Visible = False

        Kindly_act_panel.Visible = False
        Leader_ship_panel.Visible = False
        Cum_laude_panel.Visible = False
        MVP_panel.Visible = False
        DisplayHonors()
    End Sub

    Private Sub Nav_outstanding_athlete_btn_Click(sender As Object, e As EventArgs) Handles Nav_outstanding_athlete_btn.Click
        Outstanding_athlete_panel.Visible = True
        MVP_panel.Visible = False
        Honors_panel.Visible = False

        Kindly_act_panel.Visible = False
        Leader_ship_panel.Visible = False
        Cum_laude_panel.Visible = False
        DisplayOutstandingRewards()
    End Sub

    Private Sub Nav_mvp_btn_Click(sender As Object, e As EventArgs) Handles Nav_mvp_btn.Click
        MVP_panel.Visible = True
        Outstanding_athlete_panel.Visible = False
        Honors_panel.Visible = False

        Kindly_act_panel.Visible = False
        Leader_ship_panel.Visible = False
        Cum_laude_panel.Visible = False
        DisplayMVPRewards()
    End Sub

    'Start Honors Code
    Private Sub Honor_add_btn_Click(sender As Object, e As EventArgs) Handles Honor_add_btn.Click
        Honor_form.Honor_save_btn.Text = "Save"
        Honor_form.ShowDialog()
    End Sub

    Private Sub Honor_edit_btn_Click(sender As Object, e As EventArgs) Handles Honor_edit_btn.Click
        Honor_form.Honor_save_btn.Text = "Edit"
        Honor_form.ShowDialog()
    End Sub

    Private Sub Honor_view_btn_Click(sender As Object, e As EventArgs) Handles Honor_view_btn.Click
        Open_document.HonorCondition()
    End Sub

    Private Sub honors_prev_Click(sender As Object, e As EventArgs) Handles honors_prev.Click
        scrollVal = scrollVal - 10
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 10, "Honors_table")
    End Sub

    Private Sub hornors_next_Click(sender As Object, e As EventArgs) Handles hornors_next.Click
        scrollVal = scrollVal + 10
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 10, "Honors_table")
    End Sub

    Public Sub DisplayHonors()
        If opendb() Then
            Dim query As String = "SELECT
	                    honors.id as ID,
                        students.student_no AS StudentID,
                        students.first_name AS Firstname,
                        students.middle_name AS Middlename,
                        students.last_name AS Lastname,
                        students.age AS Age,
                        students.gender AS Gender,
                        students.section AS Section,
                        programs.abbreviation AS Abbreviation,
   	                    honors.gwa as GWA,
                        honors.school_year as SchoolYear
                    FROM
                        honors
                    JOIN students on honors.student_id = students.id
                    JOIN programs ON students.program_id = programs.id
                    ORDER BY
                        honors.id
                    DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Honors_table")

            Try
                Honor_datagrid_view.DataSource = pagingDS
                Honor_datagrid_view.DataMember = "Honors_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Honor_datagrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Honor_datagrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Honor_datagrid_view.Rows(e.RowIndex)
            Honor_form.PrimaryKey_Honor = row.Cells("ID").Value.ToString
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            Honor_edit_btn.Enabled = True
            Honor_delete_btn.Enabled = True
            Honor_view_btn.Enabled = True
        End If
    End Sub

    Private Sub Honor_delete_btn_Click(sender As Object, e As EventArgs) Handles Honor_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this program", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `Honors` WHERE id = '" & Honor_form.PrimaryKey_Honor & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisplayHonors()
                    DisableHonorbtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Honor_refresh_btn_Click(sender As Object, e As EventArgs) Handles Honor_refresh_btn.Click
        DisplayHonors()
        DisableHonorbtn()
    End Sub

    Public Sub DisableHonorbtn()
        Honor_delete_btn.Enabled = False
        Honor_edit_btn.Enabled = False
        Honor_view_btn.Enabled = False
    End Sub
    'End of Honor Codes

    'Outstanding Athlete Awards
    Private Sub Outstanding_refresh_btn_Click(sender As Object, e As EventArgs) Handles Outstanding_refresh_btn.Click
        DisplayOutstandingRewards()
        DisableOutstandingbtn()
    End Sub

    Public Sub DisplayOutstandingRewards()
        If opendb() Then
            Dim query As String = "SELECT
                            outstanding_athlete.id AS ID,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            outstanding_athlete.sports as Sports, 
                            outstanding_athlete.date_issued as DateIssued
                        FROM
                            outstanding_athlete
                        JOIN students ON outstanding_athlete.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                        ORDER BY
                            outstanding_athlete.id
                        DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Outstanding_table")

            Try
                Outstanding_athlete_datagrid_view.DataSource = pagingDS
                Outstanding_athlete_datagrid_view.DataMember = "Outstanding_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Outstanding_add_btn_Click(sender As Object, e As EventArgs) Handles Outstanding_add_btn.Click
        Outstanding_athlete_form.Outstanding_athlete_save_btn.Text = "Save"
        Outstanding_athlete_form.ShowDialog()
    End Sub
    Private Sub Outstanding_athlete_datagrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Outstanding_athlete_datagrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Outstanding_athlete_datagrid_view.Rows(e.RowIndex)
            Outstanding_athlete_form.PrimaryKey_Outstanding_athlete = row.Cells("ID").Value.ToString
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            Outstanding_edit_btn.Enabled = True
            Outstanding_delete_btn.Enabled = True
            outstanding_view_btn.Enabled = True
        End If
    End Sub

    Private Sub Outstanding_edit_btn_Click(sender As Object, e As EventArgs) Handles Outstanding_edit_btn.Click
        Outstanding_athlete_form.Outstanding_athlete_save_btn.Text = "Edit"
        Outstanding_athlete_form.ShowDialog()
    End Sub

    Private Sub outstanding_view_btn_Click(sender As Object, e As EventArgs) Handles outstanding_view_btn.Click
        Open_document.OutstandingAthleteCondition()
    End Sub

    Private Sub Outstanding_delete_btn_Click(sender As Object, e As EventArgs) Handles Outstanding_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also deleting related data of this Student", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `outstanding_athlete` WHERE id = '" & Outstanding_athlete_form.PrimaryKey_Outstanding_athlete & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisplayOutstandingRewards()
                    File_path_dashboard.Document_delete_btn.Enabled = False
                    File_path_dashboard.Edit_document_themeplate_path_btn.Enabled = False
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Outstanding_search_txtbox_KeyPress(sender As Object, e As EventArgs) Handles Outstanding_search_txtbox.KeyPress
        Try
            If Outstanding_search_txtbox.Text.Trim(" ") = " " Then
                DisplayOutstandingRewards()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                                outstanding_athlete.id AS ID,
                                students.student_no AS StudentID,
                                students.first_name AS Firstname,
                                students.middle_name AS Middlename,
                                students.last_name AS Lastname,
                                students.age AS Age,
                                students.gender AS Gender,
                                students.section AS Section,
                                programs.abbreviation AS Abbreviation,
                                outstanding_athlete.sports as Sports, 
                                outstanding_athlete.date_issued as DateIssued
                            FROM
                                outstanding_athlete
                            JOIN students ON outstanding_athlete.student_id = students.id
                            JOIN programs ON students.program_id = programs.id
                            WHERE
                                students.first_name LIKE '%" & Outstanding_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & Outstanding_search_txtbox.Text & "%' OR students.last_name LIKE '%" & Outstanding_search_txtbox.Text & "%' OR students.student_no LIKE '%" & Outstanding_search_txtbox.Text & "%'
                            ORDER BY
                                outstanding_athlete.id
                            DESC
                                "

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        Outstanding_athlete_datagrid_view.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Public Sub DisableOutstandingbtn()
        Outstanding_delete_btn.Enabled = False
        Outstanding_edit_btn.Enabled = False
        outstanding_view_btn.Enabled = False
    End Sub
    'Outstanding End Code


    Private Sub mvp_add_btn_Click(sender As Object, e As EventArgs) Handles mvp_add_btn.Click
        MVP_form.MVP_save_btn.Text = "Save"
        MVP_form.ShowDialog()
    End Sub

    Private Sub mvp_edit_btn_Click(sender As Object, e As EventArgs) Handles mvp_edit_btn.Click
        MVP_form.MVP_save_btn.Text = "Edit"
        MVP_form.ShowDialog()
    End Sub
    Private Sub MVP_view_btn_Click(sender As Object, e As EventArgs) Handles MVP_view_btn.Click
        Open_document.MVPCondition()
    End Sub

    Private Sub mvp_next_Click(sender As Object, e As EventArgs) Handles mvp_next.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "MVP_table")
    End Sub

    Private Sub mvp_prev_Click(sender As Object, e As EventArgs) Handles mvp_prev.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "MVP_table")
    End Sub

    Private Sub mvp_delete_btn_Click(sender As Object, e As EventArgs) Handles mvp_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also deleting related data of this Student", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `mvp_athletes` WHERE id = '" & MVP_form.PrimaryKey_MVP & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisplayOutstandingRewards()
                    disableMVPbtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub DisplayMVPRewards()
        If opendb() Then
            Dim query As String = "SELECT
                            mvp_athletes.id AS ID,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            mvp_athletes.sports as Sports, 
                            mvp_athletes.date_issued as DateIssued
                        FROM
                            mvp_athletes
                        JOIN students ON mvp_athletes.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                        ORDER BY
                            mvp_athletes.id
                        DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "MVP_table")

            Try
                mvp_datagrid_view.DataSource = pagingDS
                mvp_datagrid_view.DataMember = "MVP_table"

            Catch ex As Exception
                MsgBox(ex.Message + "MVP Display")
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub mvp_datagrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles mvp_datagrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = Me.mvp_datagrid_view.Rows(e.RowIndex)
            MVP_form.PrimaryKey_MVP = row.Cells("ID").Value.ToString
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            mvp_edit_btn.Enabled = True
            mvp_delete_btn.Enabled = True
            MVP_view_btn.Enabled = True
        End If
    End Sub

    Private Sub mvp_refresh_btn_Click(sender As Object, e As EventArgs) Handles mvp_refresh_btn.Click
        DisplayMVPRewards()
        disableMVPbtn()
    End Sub

    Private Sub mvp_search_txtbox_KeyPress(sender As Object, e As EventArgs) Handles mvp_search_txtbox.KeyPress
        Try
            If mvp_search_txtbox.Text.Trim(" ") = " " Then
                DisplayMVPRewards()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                                mvp_athletes.id AS ID,
                                students.student_no AS StudentID,
                                students.first_name AS Firstname,
                                students.middle_name AS Middlename,
                                students.last_name AS Lastname,
                                students.age AS Age,
                                students.gender AS Gender,
                                students.section AS Section,
                                programs.abbreviation AS Abbreviation,
                                mvp_athletes.sports as Sports, 
                                mvp_athletes.date_issued as DateIssued
                            FROM
                                mvp_athletes
                            JOIN students ON mvp_athletes.student_id = students.id
                            JOIN programs ON students.program_id = programs.id
                            WHERE
                                students.first_name LIKE '%" & mvp_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & mvp_search_txtbox.Text & "%' OR students.last_name LIKE '%" & mvp_search_txtbox.Text & "%' OR students.student_no LIKE '%" & mvp_search_txtbox.Text & "%'
                            ORDER BY
                                mvp_athletes.id
                            DESC
                                "

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        mvp_datagrid_view.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message + "MVP Search")
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Public Sub disableMVPbtn()
        mvp_delete_btn.Enabled = False
        mvp_edit_btn.Enabled = False
        MVP_view_btn.Enabled = False
    End Sub
    'End of MVP code

    Private Sub kindly_act_refresh_btn_Click(sender As Object, e As EventArgs) Handles kindly_act_refresh_btn.Click
        DisplayKindlyAct()
    End Sub

    Private Sub kindly_act_add_btn_Click(sender As Object, e As EventArgs) Handles kindly_act_add_btn.Click
        kindly_act_form.Kindly_save_btn.Text = "Save"
        kindly_act_form.ShowDialog()
    End Sub

    Private Sub kindly_act_edit_btn_Click(sender As Object, e As EventArgs) Handles kindly_act_edit_btn.Click
        kindly_act_form.Kindly_save_btn.Text = "Edit"
        kindly_act_form.ShowDialog()
    End Sub
    Private Sub kindly_act_view_btn_Click(sender As Object, e As EventArgs) Handles kindly_act_view_btn.Click
        Open_document.KindlyActCondition
    End Sub

    Private Sub kindlyact_next_Click(sender As Object, e As EventArgs) Handles kindlyact_next.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Kindly_table")
    End Sub

    Private Sub kindlyact_prev_Click(sender As Object, e As EventArgs) Handles kindlyact_prev.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Kindly_table")
    End Sub

    Private Sub kindly_act_datagrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles kindly_act_datagrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = kindly_act_datagrid_view.Rows(e.RowIndex)
            kindly_act_form.PrimaryKey_kindly = row.Cells("ID").Value.ToString
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            kindly_act_edit_btn.Enabled = True
            kindly_act_delete_btn.Enabled = True
            kindly_act_view_btn.Enabled = True
        End If
    End Sub

    Public Sub DisplayKindlyAct()
        If opendb() Then
            Dim query As String = "SELECT
                kindly_acts.id AS ID,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                kindly_acts.kindly_act,
                kindly_acts.date_issued
            FROM
                kindly_acts
            JOIN students ON kindly_acts.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            ORDER BY
                    kindly_acts.id
             DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Kindly_table")

            Try
                kindly_act_datagrid_view.DataSource = pagingDS
                kindly_act_datagrid_view.DataMember = "Kindly_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub kindly_act_search_txtbox_KeyPress(sender As Object, e As EventArgs) Handles kindly_act_search_txtbox.KeyPress
        Try
            If kindly_act_search_txtbox.Text.Trim(" ") = " " Then
                DisplayKindlyAct()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                                kindly_acts.id AS ID,
                                students.student_no AS StudentID,
                                students.first_name AS Firstname,
                                students.middle_name AS Middlename,
                                students.last_name AS Lastname,
                                students.age AS Age,
                                students.gender AS Gender,
                                students.section AS Section,
                                programs.abbreviation AS Abbreviation,
                                programs.program_name,
                                kindly_acts.kindly_act,
                                kindly_acts.date_issued
                            FROM
                                kindly_acts
                            JOIN students ON kindly_acts.student_id = students.id
                            JOIN programs ON students.program_id = programs.id
                            WHERE
                                students.first_name LIKE '%" & kindly_act_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & kindly_act_search_txtbox.Text & "%' OR students.last_name LIKE '%" & kindly_act_search_txtbox.Text & "%' OR students.student_no LIKE '%" & kindly_act_search_txtbox.Text & "%'
                            ORDER BY
                                kindly_acts.id
                            DESC
                                "

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        mvp_datagrid_view.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        disableKindlybtn()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub kindly_act_delete_btn_Click(sender As Object, e As EventArgs) Handles kindly_act_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this program", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `kindly_acts` WHERE id = '" & kindly_act_form.PrimaryKey_kindly & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisplayKindlyAct()
                    disableKindlybtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub
    Public Sub disableKindlybtn()
        kindly_act_edit_btn.Enabled = False
        kindly_act_delete_btn.Enabled = False
        kindly_act_view_btn.Enabled = False
    End Sub
    'End of kindly code


    Public Sub DisplayLeadership()
        If opendb() Then
            Dim query As String = "SELECT
                leaderships.id AS ID,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name AS Program,
                leaderships.event_title AS EventTitle,
                leaderships.date_issued AS DateIssued
            FROM
                leaderships
            JOIN students ON leaderships.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            ORDER BY
                leaderships.id
            DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Leadership_table")

            Try
                leadership_datagrid_view.DataSource = pagingDS
                leadership_datagrid_view.DataMember = "Leadership_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub leadership_add_btn_Click(sender As Object, e As EventArgs) Handles leadership_add_btn.Click
        Leadership_form.leader_save_btn.Text = "Save"
        Leadership_form.ShowDialog()
    End Sub

    Private Sub leadershipt_edit_btn_Click(sender As Object, e As EventArgs) Handles leadershipt_edit_btn.Click
        Leadership_form.leader_save_btn.Text = "Edit"
        Leadership_form.ShowDialog()
    End Sub
    Private Sub leadership_view_btn_Click(sender As Object, e As EventArgs) Handles leadership_view_btn.Click
        Open_document.LeadershipCondition
    End Sub

    Private Sub leadership_datagrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles leadership_datagrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = leadership_datagrid_view.Rows(e.RowIndex)
            Leadership_form.PrimaryKey_Leader = row.Cells("ID").Value.ToString
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            leadershipt_edit_btn.Enabled = True
            leadership_delete_btn.Enabled = True
            leadership_view_btn.Enabled = True
        End If
    End Sub

    Private Sub leadership_delete_btn_Click(sender As Object, e As EventArgs) Handles leadership_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this program", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `leaderships` WHERE id = '" & Leadership_form.PrimaryKey_Leader & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    DisableLeadershipbtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub DisableLeadershipbtn()
        DisplayLeadership()
        leadershipt_edit_btn.Enabled = False
        leadership_delete_btn.Enabled = False
        leadership_view_btn.Enabled = False
    End Sub

    Private Sub Leadership_search_btn_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Leadership_search_btn.KeyPress
        Try
            If kindly_act_search_txtbox.Text.Trim(" ") = " " Then
                DisableLeadershipbtn()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                            leaderships.id AS ID,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            programs.program_name AS Program,
                            leaderships.event_title AS EventTitle,
                            leaderships.date_issued AS DateIssued
                        FROM
                            leaderships
                        JOIN students ON leaderships.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                            WHERE
                                students.first_name LIKE '%" & Leadership_search_btn.Text & "%' OR students.middle_name LIKE '%" & Leadership_search_btn.Text & "%' OR students.last_name LIKE '%" & Leadership_search_btn.Text & "%' OR students.student_no LIKE '%" & Leadership_search_btn.Text & "%'
                            ORDER BY
                                leaderships.id
                            DESC
                                "

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        leadership_datagrid_view.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        DisableLeadershipbtn()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub leadershit_refresh_btn_Click(sender As Object, e As EventArgs) Handles leadership_refresh_btn.Click
        DisableLeadershipbtn()
    End Sub
    'End of Leadership Code

    Public Sub DisplayCumlaude()
        If opendb() Then
            Dim query As String = "SELECT
                cum_laudes.id AS ID,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name AS Program,
                cum_laudes.date_issued AS Date,
                cum_laudes.school_year AS SchoolYear 
            FROM
                cum_laudes
            JOIN students ON cum_laudes.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            ORDER BY
                cum_laudes.id
            DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Cumlaude_table")

            Try
                cum_laude_datadrid_view.DataSource = pagingDS
                cum_laude_datadrid_view.DataMember = "Cumlaude_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub cum_laude_refresh_btn_Click(sender As Object, e As EventArgs) Handles cum_laude_refresh_btn.Click
        Open_document.ThemeplateName = "Cum Laude"
        disableCumLaudebtn()
    End Sub

    Private Sub cum_laude_add_btn_Click(sender As Object, e As EventArgs) Handles cum_laude_add_btn.Click
        Cum_laude_form.cumlaude_save_btn.Text = "Save"
        Cum_laude_form.ShowDialog()
    End Sub

    Private Sub cum_laude_edit_btn_Click(sender As Object, e As EventArgs) Handles cum_laude_edit_btn.Click
        Cum_laude_form.cumlaude_save_btn.Text = "Edit"
        Cum_laude_form.ShowDialog()
    End Sub

    Private Sub cum_laude_datadrid_view_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles cum_laude_datadrid_view.CellClick
        If e.RowIndex >= 0 Then
            row = cum_laude_datadrid_view.Rows(e.RowIndex)
            Open_document.StudentName = row.Cells("FirstName").Value.ToString + " " + row.Cells("MiddleName").Value.ToString + " " + row.Cells("LastName").Value.ToString
            Open_document.UniversalPrimaryKey = row.Cells("ID").Value.ToString
            Cum_laude_form.PrimaryKey_Cumlaude = row.Cells("ID").Value.ToString
            cum_laude_delete_btn.Enabled = True
            cum_laude_edit_btn.Enabled = True
            cum_laude_view_btn.Enabled = True
        End If
    End Sub

    Private Sub cum_laude_view_btn_Click(sender As Object, e As EventArgs) Handles cum_laude_view_btn.Click
        Open_document.CumLaudeCondition()
    End Sub

    Private Sub cumlaude_next_Click(sender As Object, e As EventArgs) Handles cumlaude_next.Click
        scrollVal = scrollVal + 15
        If scrollVal > 50 Then
            scrollVal = 20
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Cumlaude_table")
    End Sub

    Private Sub cumlaude_prev_Click(sender As Object, e As EventArgs) Handles cumlaude_prev.Click
        scrollVal = scrollVal - 15
        If scrollVal <= 0 Then
            scrollVal = 0
        End If
        pagingDS.Clear()
        adapter.Fill(pagingDS, scrollVal, 15, "Cumlaude_table")
    End Sub

    Private Sub cum_laude_delete_btn_Click(sender As Object, e As EventArgs) Handles cum_laude_delete_btn.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure want to delete?" & vbCrLf & "Deleting this will also delete related data under of this program", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then
            If opendb() Then
                Dim query As String = "DELETE FROM `cum_laudes` WHERE id = '" & Cum_laude_form.PrimaryKey_Cumlaude & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Deleted.",
                "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    disableCumLaudebtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub disableCumLaudebtn()
        DisplayCumlaude()
        cum_laude_edit_btn.Enabled = False
        cum_laude_delete_btn.Enabled = False
        cum_laude_view_btn.Enabled = False
    End Sub

    Private Sub cumlaude_search_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cumlaude_search_txtbox.KeyPress
        Try
            If kindly_act_search_txtbox.Text.Trim(" ") = " " Then
                DisplayCumlaude()
            Else
                If opendb() Then
                    Dim query As String = "SELECT
                            cum_laudes.id AS ID,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            programs.program_name AS Program,
                            cum_laudes.date_issued AS Date,
                            cum_laudes.school_year AS SchoolYear 
                        FROM
                            cum_laudes
                        JOIN students ON cum_laudes.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                            WHERE
                                students.first_name LIKE '%" & cumlaude_search_txtbox.Text & "%' OR students.middle_name LIKE '%" & cumlaude_search_txtbox.Text & "%' OR students.last_name LIKE '%" & cumlaude_search_txtbox.Text & "%' OR students.student_no LIKE '%" & cumlaude_search_txtbox.Text & "%'
                            ORDER BY
                                cum_laudes.id
                            DESC"

                    adapter = New MySqlDataAdapter(query, conn)
                    Dim table As New DataTable()

                    Try

                        adapter.Fill(table)
                        leadership_datagrid_view.DataSource = table

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        disableCumLaudebtn()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub Reward_Dashboard_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main_Dashboard.Show()

    End Sub

    Private Sub Reward_Dashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Main_Dashboard.Close()
    End Sub


End Class