﻿Imports MySql.Data.MySqlClient
Module Crystal_report_summary

    Dim No_students As String
    Dim No_referrals As String
    Dim No_disciplinary_actioned As String
    Dim No_counseled As String

    Dim No_lightoffense As String
    Dim No_seriousoffense As String
    Dim No_veryseriousoffense As String
    Dim Report As New CrystalReportSummary

    Dim No_Oustanding As String
    Dim No_Mvp As String
    Dim No_Leadership As String
    Dim No_Kindlyact As String
    Dim No_Honors As String
    Dim No_CumLaude As String
    Dim Reward As New CrystalReportRewards

    Private Sub crystalreport_data()
        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `students`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_students = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("total students", No_students)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `referrals`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_referrals = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("total referred", No_referrals)
                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `disciplinary_action`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_disciplinary_actioned = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("total disciplinary", No_disciplinary_actioned)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `cases`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_counseled = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("total counseled", No_counseled)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT
                COUNT(offenses.id)
            FROM
                referrals
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                offenses.id = 1"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_lightoffense = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("light offenses", No_lightoffense)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT
                COUNT(offenses.id)
            FROM
                referrals
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                offenses.id = 2"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_seriousoffense = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("serious offense", No_seriousoffense)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT
                COUNT(offenses.id)
            FROM
                referrals
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                offenses.id = 3"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_veryseriousoffense = cmd.ExecuteScalar().ToString()
                Report.SetParameterValue("very serious offense", No_veryseriousoffense)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Report

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If



    End Sub

    Private Sub crsytalreportAward()
        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `outstanding_athlete`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_Oustanding = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("OutstandingAthleteAwards", No_Oustanding)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `mvp_athletes`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_Mvp = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("MVPAthleteAwards", No_Mvp)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `leaderships`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_Leadership = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("LeadershipsAwards", No_Leadership)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `kindly_acts`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_Kindlyact = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("KindlyActAwards", No_Kindlyact)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `honors`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_honor = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("HonorsAwards", No_honor)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `cum_laudes`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_CumLaude = cmd.ExecuteScalar().ToString()
                Reward.SetParameterValue("CumLaudeAwards", No_CumLaude)

                CrystalReportViewer.CrystalReportViewer1.ReportSource = Reward

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Public Sub DisplayCrystalAward()
        crsytalreportAward()
    End Sub

    Public Sub DisplayCrystalReport()
        crystalreport_data()
    End Sub

End Module
