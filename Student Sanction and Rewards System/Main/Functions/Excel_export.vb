﻿Imports System.Data
Imports MySql.Data.MySqlClient
Imports excel = Microsoft.Office.Interop.Excel
Module Excel_export
    Dim adapter As New MySqlDataAdapter
    Dim dataset As DataSet

    Dim i As Integer
    Dim fullpath As String

    Dim xlapp As excel.Application
    Dim xlworkbook As excel.Workbook
    Dim xlworksheet As excel.Worksheet
    Dim misvalue As Object = Reflection.Missing.Value
    Public Sub StudenttoExcel()
        Dim result As DialogResult = MessageBox.Show("Do you want to create Excel Report?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then

            If opendb() Then
                Dim query As String = "SELECT
                students.id AS ID,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name AS Program
            FROM
                students
            JOIN programs ON students.program_id = programs.id
            ORDER BY
                students.id ASC"

                adapter = New MySqlDataAdapter(query, conn)
                Dim table As New DataTable()

                Try

                    adapter.Fill(table)
                    Datagridview.Universal_datagridview.DataSource = table

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If


            Dim filename = "student.xlsx"
            xlapp = New excel.Application
            xlworkbook = xlapp.Workbooks.Add(misvalue)
            xlworksheet = xlworkbook.Sheets("Sheet1")

            Dim columnsCount As Integer = Datagridview.Universal_datagridview.Columns.Count
            For Each column In Datagridview.Universal_datagridview.Columns
                xlworksheet.Cells(1, column.Index + 1).Value = column.Name
            Next
            For i = 0 To Datagridview.Universal_datagridview.Rows.Count - 1
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    xlworksheet.Cells(i + 2, columnIndex + 1).Value = Datagridview.Universal_datagridview.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next

            If (Datagridview.FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
                fullpath = Datagridview.FolderBrowserDialog1.SelectedPath + "\" + filename
                xlworksheet.SaveAs(fullpath)
            End If

            xlworkbook.Close()
            xlapp.Quit()

            Myobj(xlapp)
            Myobj(xlworkbook)
            Myobj(xlworksheet)
        End If
    End Sub

    Public Sub CounseltoExcel()
        Dim result As DialogResult = MessageBox.Show("Do you want to create Excel Report?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then

            If opendb() Then
                Dim query As String = "SELECT
                referrals.id AS ID,
                students.student_no as StudentID,
                students.first_name as FirstName,
                students.middle_name as MiddleName,
                students.last_name as LastName,
                students.age as Age,
                students.gender as Gender,
                students.section as Section,
                students.email as Email,
                programs.program_name as Program,
                offenses.offense as Offense,
                violations.code as OffenseCode,
                violations.violation as Violations,
                referrals.employee_name as Complainant,
                referrals.referred as Referred
            FROM
                referrals
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            ORDER By
	            referrals.id ASC"

                adapter = New MySqlDataAdapter(query, conn)
                Dim table As New DataTable()

                Try

                    adapter.Fill(table)
                    Datagridview.Universal_datagridview.DataSource = table

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If


            Dim filename = "Counsel.xlsx"
            xlapp = New excel.Application
            xlworkbook = xlapp.Workbooks.Add(misvalue)
            xlworksheet = xlworkbook.Sheets("Sheet1")

            Dim columnsCount As Integer = Datagridview.Universal_datagridview.Columns.Count
            For Each column In Datagridview.Universal_datagridview.Columns
                xlworksheet.Cells(1, column.Index + 1).Value = column.Name
            Next
            For i = 0 To Datagridview.Universal_datagridview.Rows.Count - 1
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    xlworksheet.Cells(i + 2, columnIndex + 1).Value = Datagridview.Universal_datagridview.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next

            If (Datagridview.FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
                fullpath = Datagridview.FolderBrowserDialog1.SelectedPath + "\" + filename
                xlworksheet.SaveAs(fullpath)
            End If

            xlworkbook.Close()
            xlapp.Quit()

            Myobj(xlapp)
            Myobj(xlworkbook)
            Myobj(xlworksheet)

        End If
    End Sub

    Public Sub ActiontoExcel()
        Dim result As DialogResult = MessageBox.Show("Do you want to create Excel Report?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            MessageBox.Show("Cancelled", "Confirmation Dialog")
        ElseIf result = DialogResult.Yes Then

            If opendb() Then
                Dim query As String = "SELECT
                referrals.id AS ID,
                students.student_no as StudentID,
                students.first_name as FirstName,
                students.middle_name as MiddleName,
                students.last_name as LastName,
                students.age as Age,
                students.gender as Gender,
                students.section as Section,
                students.email as Email,
                programs.program_name as Program,
                offenses.offense as Offense,
                violations.code as OffenseCode,
                violations.violation as Violations,
                referrals.employee_name as Complainant,
                referrals.referred as Referred
            FROM
                referrals
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            ORDER By
	            referrals.id ASC"

                adapter = New MySqlDataAdapter(query, conn)
                Dim table As New DataTable()

                Try

                    adapter.Fill(table)
                    Datagridview.Universal_datagridview.DataSource = table

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If


            Dim filename = "Action.xlsx"
            xlapp = New excel.Application
            xlworkbook = xlapp.Workbooks.Add(misvalue)
            xlworksheet = xlworkbook.Sheets("Sheet1")

            Dim columnsCount As Integer = Datagridview.Universal_datagridview.Columns.Count
            For Each column In Datagridview.Universal_datagridview.Columns
                xlworksheet.Cells(1, column.Index + 1).Value = column.Name
            Next
            For i = 0 To Datagridview.Universal_datagridview.Rows.Count - 1
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    xlworksheet.Cells(i + 2, columnIndex + 1).Value = Datagridview.Universal_datagridview.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next

            If (Datagridview.FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
                fullpath = Datagridview.FolderBrowserDialog1.SelectedPath + "\" + filename
                xlworksheet.SaveAs(fullpath)
            End If

            xlworkbook.Close()
            xlapp.Quit()

            Myobj(xlapp)
            Myobj(xlworkbook)
            Myobj(xlworksheet)

        End If
    End Sub

    Private Sub Myobj(ByVal obj As Object)
        Try
            Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

End Module
