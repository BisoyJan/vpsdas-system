﻿Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports MySql.Data.MySqlClient


Module Open_document

    Private DocumentSavedPath As String
    Private Path As String
    Private Save As String
    Private sFileName As String
    Private fullpath As String
    Private DatabasePath As String

    Private wdApp As Word.Application
    Private wdDocs As Word.Documents

    Public UniversalPrimaryKey As String
    Public StudentName As String
    Public ThemeplateName As String

    Private StudentID As String
    Private IssuedDate As String
    Private SchoolYear As String


    Private Program As String
    Private GWA As String
    Private UnitHead As String
    Private UnitHeadTitle As String
    Private CoachName As String
    Private OrganizerName As String
    Private Sports As String
    Private EventTitle As String
    Private VPSDASname As String
    Private ReturnItem As String

    Private Sub DocumentPath()
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + ThemeplateName + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        Path = dtreader.GetString("path")
                        Save = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub ExistingDocument()
        sFileName = StudentName + "_" + UniversalPrimaryKey
        fullpath = Save + "\" + sFileName + ".docx"
    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Public Sub CumLaudeCondition()
        ThemeplateName = "Cum Laude"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start(fullpath)
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                CumLaudeCreateDocument()
            End Try
        Else
            CumLaudeCreateDocument()
        End If

    End Sub

    Private Sub CumLaudeCreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                            cum_laudes.id AS ID,
                            students.id AS studid,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            programs.program_name,
                            cum_laudes.date_issued,
                            cum_laudes.school_year
                        FROM
                            cum_laudes
                        JOIN students ON cum_laudes.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                    WHERE
                     cum_laudes.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("studid")

                    IssuedDate = dtreader.GetString("date_issued")
                    Program = dtreader.GetString("Abbreviation")
                    SchoolYear = dtreader.GetString("school_year")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("Program").Range.Text = Program
            wdBooks("DateIssued").Range.Text = IssuedDate
            wdBooks("SchoolYear").Range.Text = SchoolYear
            wdBooks("StudentName").Range.Text = StudentName


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub HonorCondition()
        ThemeplateName = "Honors"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start(fullpath)
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                HonorCreateDocument()
            End Try
        Else
            HonorCreateDocument()
        End If
    End Sub

    Private Sub HonorCreateDocument()
        If opendb() Then
            Dim query As String = "SELECT
                            honors.id AS ID,
                            students.id as IDstudent,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            honors.gwa AS GWA,
                            honors.school_year AS SchoolYear,
                            honors.date,
                            honors.unit_head,
                            honors.unit_head_title
                        FROM
                            honors
                        JOIN students ON honors.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                    WHERE
                     honors.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("IDstudent")


                    GWA = dtreader.GetString("GWA")
                    IssuedDate = dtreader.GetString("date")
                    UnitHead = dtreader.GetString("unit_head")
                    SchoolYear = dtreader.GetString("SchoolYear")
                    UnitHeadTitle = dtreader.GetString("unit_head_title")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"

            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("GWA").Range.Text = GWA
            wdBooks("DateGiven").Range.Text = IssuedDate
            wdBooks("SchoolYear").Range.Text = SchoolYear
            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("UnitHead").Range.Text = UnitHead
            wdBooks("UnitHeadTitle").Range.Text = UnitHeadTitle

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub MVPCondition()
        ThemeplateName = "MVP"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start(fullpath)
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                MVPCreateDocument()
            End Try
        Else
            MVPCreateDocument()
        End If
    End Sub

    Private Sub MVPCreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                mvp_athletes.id AS ID,
                students.id as IDstudent,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                mvp_athletes.sports,
                mvp_athletes.date_issued,
                mvp_athletes.coach_name,
                mvp_athletes.organizer_name
            FROM
                mvp_athletes
            JOIN students ON mvp_athletes.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE mvp_athletes.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("IDstudent")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

                    Sports = dtreader.GetString("sports")
                    CoachName = dtreader.GetString("coach_name")
                    OrganizerName = dtreader.GetString("organizer_name")
                    IssuedDate = dtreader.GetString("date_issued")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("CoachName").Range.Text = CoachName
            wdBooks("DateIssued").Range.Text = IssuedDate
            wdBooks("OrganizerName").Range.Text = OrganizerName
            wdBooks("SportName").Range.Text = Sports
            wdBooks("SportName2").Range.Text = Sports
            wdBooks("StudentName").Range.Text = StudentName

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub LeadershipCondition()
        ThemeplateName = "LeaderShip"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start(fullpath)
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                LeadershipCreateDocument()
            End Try
        Else
            LeadershipCreateDocument()
        End If
    End Sub

    Private Sub LeadershipCreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                leaderships.id AS ID,
                students.id AS studid,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                leaderships.date_issued,
                leaderships.event_title,
                leaderships.vpsdas_name
            FROM
                leaderships
            JOIN students ON leaderships.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                leaderships.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("studid")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    IssuedDate = dtreader.GetString("date_issued")
                    EventTitle = dtreader.GetString("event_title")
                    VPSDASName = dtreader.GetString("vpsdas_name")

                End While
            Catch ex As Exception
                MsgBox(ex.Message + "Create Document")
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("DateIssued").Range.Text = IssuedDate
            wdBooks("EventTitle").Range.Text = EventTitle
            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("VPSDASName").Range.Text = VPSDASName


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Public Sub KindlyActCondition()
        ThemeplateName = "Kindly Act"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start($"{fullpath}.docx")
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                KindlyActCreateDocument()
            End Try
        Else
            KindlyActCreateDocument()
        End If
    End Sub

    Private Sub KindlyActCreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                kindly_acts.id AS ID,
                students.id AS IDstudent,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                kindly_acts.kindly_act,
                kindly_acts.date_issued
            FROM
                kindly_acts
            JOIN students ON kindly_acts.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                kindly_acts.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("IDstudent")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")


                    ReturnItem = dtreader.GetString("kindly_act")
                    IssuedDate = dtreader.GetString("date_issued")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("DateIssued").Range.Text = IssuedDate
            wdBooks("ReturnedItem").Range.Text = ReturnItem


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub


    Public Sub OutstandingAthleteCondition()
        ThemeplateName = "Outstanding Athlete"
        DocumentPath()
        ExistingDocument()
        If String.IsNullOrEmpty(Path) Then
            MsgBox("Themeplat Path and Save Path not Yet Set")
        ElseIf String.IsNullOrEmpty(fullpath) Then
            Try
                Process.Start(fullpath)
            Catch ex As Exception
                MsgBox("The File is been Deleted Or move Will be creating Document Shortly")
                OutstandingAthleteCreateDocument()
            End Try
        Else
            OutstandingAthleteCreateDocument()
        End If
    End Sub

    Private Sub OutstandingAthleteCreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                outstanding_athlete.id AS ID,
                students.id AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                outstanding_athlete.sports,
                outstanding_athlete.date_issued,
                outstanding_athlete.coach_name,
                outstanding_athlete.organizer_name
            FROM
                outstanding_athlete
            JOIN students ON outstanding_athlete.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE outstanding_athlete.id = '" & UniversalPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    StudentID = dtreader.GetString("StudentID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

                    Sports = dtreader.GetString("sports")
                    CoachName = dtreader.GetString("coach_name")
                    OrganizerName = dtreader.GetString("organizer_name")
                    IssuedDate = dtreader.GetString("date_issued")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + UniversalPrimaryKey
            fullpath = Save + "\" + sFileName + ".docx"
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(Path)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("CoachName").Range.Text = CoachName
            wdBooks("DatePresented").Range.Text = IssuedDate
            wdBooks("Organizername").Range.Text = OrganizerName
            wdBooks("Sports").Range.Text = Sports
            wdBooks("Sports2").Range.Text = Sports
            wdBooks("StudentName").Range.Text = StudentName

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()

            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Path = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & StudentID & "','" & ThemeplateName & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Process.Start(fullpath)
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

End Module
