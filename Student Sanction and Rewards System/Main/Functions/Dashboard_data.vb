﻿Imports MySql.Data.MySqlClient
Imports System.Windows.Forms.DataVisualization.Charting
Module Dashboard_data

    Public No_students As String
    Public No_counsell As String
    Public No_action As String
    Public No_referred As String
    Public No_items As String
    Public No_honor As String
    Public No_cumlaude As String
    Public No_leadership As String

    Public No_mvp As String
    Public No_outstanding As String
    Public No_kindly As String

    Public AccountType As String


    Public Sub Check_Account()
        If AccountType = "Admin" Then
            Account_Form.backup_database_btn.Visible = False
        Else
            Account_Form.backup_database_btn.Visible = False
            Main_Dashboard.Nav_btn_accounts.Visible = False
            Main_Dashboard.Nav_disciplinary_case.Visible = False
            Main_Dashboard.Discipline_counsel_btn.Visible = False
            Main_Dashboard.Discipline_counsel_btn.Visible = False
        End If
    End Sub


    Private Sub DashboardDatabaseNumbers()
        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `students`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_students = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `cases`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_counsell = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `disciplinary_action`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_action = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `referrals`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_referred = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `properties`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_items = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `honors`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_honor = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `cum_laudes`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_cumlaude = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `leaderships`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_leadership = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `mvp_athletes`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_mvp = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `outstanding_athlete`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_outstanding = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        If opendb() Then
            Dim query As String = "SELECT COUNT(id) FROM `kindly_acts`"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                No_kindly = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub


    Private Sub DisplayPieChart()
        Dim yValues As Integer() = {CInt(No_referred), CInt(No_action), CInt(No_counsell)} ' Getting values from Textboxes 

        Dim xValues As String() = {"Referral", "Action", "Counselling"} ' Headings
        Dim seriesName As String = Nothing

        ' Note 1 : Clear chart before fill - VERY IMPORTANT and can generate exception if you are generating
        '          multiple charts in loop and have not included below lines !
        ' Note 2 : Chrt variable here is the Name of your Chart 
        Main_Dashboard.Sanction_Chart.Series.Clear()
        Main_Dashboard.Sanction_Chart.Titles.Clear()

        ' Give unique Series Name
        seriesName = "Sanctioning"
        Main_Dashboard.Sanction_Chart.Series.Add(seriesName)

        ' Bind X and Y values
        Main_Dashboard.Sanction_Chart.Series(seriesName).Points.DataBindXY(xValues, yValues)

        ' Define Custom Chart Colors
        Main_Dashboard.Sanction_Chart.Series(seriesName).Palette = ChartColorPalette.Pastel

        '    Chart1.Series(seriesName).Points(0).Color = Color.MediumSeaGreen
        '    Chart1.Series(seriesName).Points(1).Color = Color.PaleGreen
        '    Chart1.Series(seriesName).Points(2).Color = Color.LawnGreen
        '    Chart1.Series(seriesName).Points(3).Color = Color.Blue
        '    Chart1.Series(seriesName).Points(4).Color = Color.Red
        '    Chart1.Series(seriesName).Points(5).Color = Color.Yellow

        ' Define Chart Type
        Main_Dashboard.Sanction_Chart.Series(seriesName).ChartType = DataVisualization.Charting.SeriesChartType.Pie
        Main_Dashboard.Sanction_Chart.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True

        ' If you want to show Chart Legends
        Main_Dashboard.Sanction_Chart.Legends(0).Enabled = True

        For Each dp As DataPoint In Main_Dashboard.Sanction_Chart.Series(seriesName).Points
            If dp.YValues(0) = 0 Then
                dp.LabelForeColor = Color.Transparent
            End If
        Next

        Main_Dashboard.Sanction_Chart.Series("Sanctioning").IsValueShownAsLabel = True
    End Sub



    Private Sub DisplayBarChart()
        Dim yValues As Integer() = {CInt(No_cumlaude), CInt(No_honor), CInt(No_kindly), CInt(No_mvp), CInt(No_leadership), CInt(No_outstanding)} ' Getting values from Textboxes 

        Dim xValues As String() = {"Cum Laude", "Honors", "Kindly Act", "Leadership", "MVP", "Oustanding Athlete"} ' Headings
        Dim seriesName As String = Nothing

        ' Note 1 : Clear chart before fill - VERY IMPORTANT and can generate exception if you are generating
        '          multiple charts in loop and have not included below lines !
        ' Note 2 : Chrt variable here is the Name of your Chart 
        Main_Dashboard.Rewards_chart.Series.Clear()
        Main_Dashboard.Rewards_chart.Titles.Clear()

        ' Give unique Series Name
        seriesName = "Rewards"
        Main_Dashboard.Rewards_chart.Series.Add(seriesName)

        ' Bind X and Y values
        Main_Dashboard.Rewards_chart.Series(seriesName).Points.DataBindXY(xValues, yValues)

        ' Define Custom Chart Colors
        Main_Dashboard.Rewards_chart.Series(seriesName).Palette = ChartColorPalette.Pastel

        ' Define Chart Type
        Main_Dashboard.Rewards_chart.Series(seriesName).ChartType = DataVisualization.Charting.SeriesChartType.Column
        Main_Dashboard.Rewards_chart.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True

        ' If you want to show Chart Legends
        Main_Dashboard.Sanction_Chart.Legends(0).Enabled = True

        For Each dp As DataPoint In Main_Dashboard.Rewards_chart.Series(seriesName).Points
            If dp.YValues(0) = 0 Then
                dp.LabelForeColor = Color.Transparent
            End If
        Next

        Main_Dashboard.Rewards_chart.Series("Rewards").IsValueShownAsLabel = True
    End Sub

    'Private Sub DisplayPieChart()
    '    'With Main_Dashboard.Sanction_Chart
    '    '    .Series.Clear()
    '    '    .Series.Add("ChartData")
    '    'End With
    '    Dim SeriesSY As Series

    '    SeriesSY = Main_Dashboard.Sanction_Chart.Series("Sanction_Chart")

    '    SeriesSY.ChartType = SeriesChartType.Pie
    '    SeriesSY.Palette = ChartColorPalette.Pastel
    '    'SeriesSY.Name = "Sanctioning Chart"
    '    SeriesSY.Points.AddXY("Referral", Convert.ToInt32(No_referred))
    '    SeriesSY.Points.AddXY("Action", Convert.ToInt32(No_action))
    '    SeriesSY.Points.AddXY("Counselling", Convert.ToInt32(No_counsell))
    'End Sub

    'Private Sub DisplayBarChart()
    '    'With Main_Dashboard.Rewards_chart
    '    '    .Series.Clear()
    '    '    .Series.Add("BarChart")
    '    'End With
    '    Dim SeriesSY As Series


    '    SeriesSY = Main_Dashboard.Rewards_chart.Series("Rewards Chart")

    '    SeriesSY.ChartType = SeriesChartType.Column
    '    SeriesSY.Palette = ChartColorPalette.Pastel
    '    'SeriesSY.Name = "Rewards Chart"
    '    SeriesSY.Points.AddXY("Cum Laude", Convert.ToInt32(No_cumlaude))
    '    SeriesSY.Points.AddXY("Honors", Convert.ToInt32(No_honor))
    '    SeriesSY.Points.AddXY("MVP", Convert.ToInt32(No_honor))
    '    SeriesSY.Points.AddXY("Kindly Act", Convert.ToInt32(No_kindly))
    '    SeriesSY.Points.AddXY("Leadership", Convert.ToInt32(No_leadership))
    '    SeriesSY.Points.AddXY("Oustanding Athlete", Convert.ToInt32(No_outstanding))
    'End Sub

    Private Sub DisplayDashboardNumbers()
        Main_Dashboard.dashboard_number_action_txtbox.Text = No_action
        Main_Dashboard.dashboard_number_counsell_txtbox.Text = No_counsell
        Main_Dashboard.dashboard_number_referral_txtbox.Text = No_referred

        Main_Dashboard.dashboard_number_cumlaude_txtbox.Text = No_cumlaude
        Main_Dashboard.dashboard_number_honor_txtbox.Text = No_honor
        Main_Dashboard.dashboard_number_items_txtbox.Text = No_items
        Main_Dashboard.dashboard_number_leadership_txtbox.Text = No_leadership

        Main_Dashboard.dashboard_number_students_txtbox.Text = No_students
    End Sub

    Public Sub DisplayDashboard()
        DashboardDatabaseNumbers()
        DisplayPieChart()
        DisplayBarChart()
        DisplayDashboardNumbers()
    End Sub



End Module
