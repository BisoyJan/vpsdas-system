﻿Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports MySql.Data.MySqlClient
Class Disciplinary_action_view_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String
    Public Disciplinary_IDStudent As String
    Public Disciplinary_StudentID As String
    Public Disciplinary_StudentName As String
    Public Disciplinary_Section As String
    Public Disciplinary_Offense As String
    Public Disciplinary_Committed_Date As String
    Public Disciplinary_Committed_Time As DateTime
    Public Disciplinary_Employee_name As String
    Public Disciplinary_Counseling_Date As String
    Public Disciplinary_Counseling_Time As DateTime
    Public Disciplinary_Issued_Date As String
    Public Disciplinary_VPSDAS_Name As String

    Dim Action As String
    Dim DatabasePath As String
    Dim ActionPath As String
    Dim SavePath As String
    Dim fullpath As String

    Private Sub BunifuButton2_Click(sender As Object, e As EventArgs) Handles BunifuButton2.Click
        Action = "action"

        If opendb() Then
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Action + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    ActionPath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisplayCase()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Disciplinary_primary_id
            fullpath = SavePath + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(ActionPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("DateTimeOffense").Range.Text = Disciplinary_Committed_Date + " " + Disciplinary_Committed_Time.ToShortTimeString
            wdBooks("Offense").Range.Text = Disciplinary_Offense

            wdBooks("ClearanceDate").Range.Text = Disciplinary_Counseling_Date
            wdBooks("ClearanceTime").Range.Text = Disciplinary_Counseling_Time.ToShortTimeString
            wdBooks("IssuedDate").Range.Text = Disciplinary_Issued_Date

            wdBooks("StudentName").Range.Text = Disciplinary_StudentName
            wdBooks("Section").Range.Text = Disciplinary_Section

            wdBooks("Employee").Range.Text = Disciplinary_Employee_name
            wdBooks("VPSDAS").Range.Text = Disciplinary_VPSDAS_Name

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()

            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If opendb() Then
            Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Disciplinary_StudentID & "','" & Action & "','" & DatabasePath & "')"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                MessageBox.Show("Succesfuly Created.",
        "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisableDisciplinarActionBTN()
            End Try
        Else
            MsgBox("NO database connections")
        End If


    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub BunifuButton4_Click(sender As Object, e As EventArgs) Handles BunifuButton4.Click
        Action = "action"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Action + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ActionPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Disciplinary_primary_id
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Disciplinary_action_view_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Action = "action"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Action + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ActionPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Disciplinary_primary_id
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Open_btn_Click(sender As Object, e As EventArgs) Handles Open_btn.Click
        Action = "action"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Action + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ActionPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Disciplinary_primary_id
            fullpath = SavePath + "\" + sFileName
            Process.Start($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub
End Class