﻿Imports MySql.Data.MySqlClient
Imports System.IO
Public Class Lost_and_found_Display
    Dim id_student As String
    Dim Retrieval_id_student As String
    Public DisplayLostItemPrimaryKey As String
    Private Sub Lost_and_found_Display_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Edit_property()
    End Sub

    Private Sub Properties_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Properties_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Properties_studentid_txtbox.Text.Trim(" ") = " " Then
                    Edit_property()
                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Properties_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("student_no")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub Retrieval_student_id_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Retrieval_student_id_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Retrieval_student_id_txtbox.Text.Trim(" ") = " " Then
                    Edit_property()
                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Retrieval_student_id_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Retrieval_student_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                                Retrieval_gender_txtbox.Text = dtreader.GetString("gender")
                                Retrieval_age_txtbox.Text = dtreader.GetInt32("age")
                                Retrieval_section_txtbox.Text = dtreader.GetString("section")
                                Retrieval_course_txtbox.Text = dtreader.GetString("program_name")
                                Retrieval_id_student = dtreader.GetInt32("student_no")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub
    Private Sub Edit_property()
        If opendb() Then
            Dim query As String = "SELECT `student_id`, `retrieval_id`, `date_found`, `date_retrieved`, `date_surrendered`, `type`, `description`, `picture`, `remarks` FROM `properties` WHERE id = '" & DisplayLostItemPrimaryKey & "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Dim img() As Byte

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read
                    Description_txtbox.Text = dtreader.GetString("description")
                    Item_type_txtbox.Text = dtreader.GetString("type")

                    Retrieval_student_id_txtbox.Text = dtreader.GetInt32("retrieval_id")
                    Properties_studentid_txtbox.Text = dtreader.GetInt32("student_id")

                    Surrendered_datepickers.Value = dtreader.GetDateTime("date_surrendered")
                    Retrieved_datepicker.Value = dtreader.GetDateTime("date_retrieved")
                    Found_datepicker.Value = dtreader.GetDateTime("date_found")

                    img = dtreader("picture")
                    Dim ms As New MemoryStream(img)


                    Properties_image_picturebox.Image = Image.FromStream(ms)

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

End Class