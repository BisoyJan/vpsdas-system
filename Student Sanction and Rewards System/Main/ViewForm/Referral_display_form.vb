﻿Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports MySql.Data.MySqlClient
Imports Word = Microsoft.Office.Interop.Word

Public Class Referral_display_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String
    Public Referral_ID As String
    Public Referral_StudenID As String
    Public Referral_WhatDate As String
    Public Referral_Description As String
    Public Referral_EmployeeName As String
    Public Referral_Referred As String
    Public Referral_StudentName As String

    Dim Referral As String
    Dim DatabasePath As String
    Dim ReferralPath As String
    Dim SavePath As String
    Dim fullpath As String

    Private Sub BunifuButton2_Click(sender As Object, e As EventArgs) Handles BunifuButton2.Click
        Referral = "Referral"

        If opendb() Then
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Referral + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    ReferralPath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisplayCase()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Referral_primary_id
            fullpath = SavePath + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(ReferralPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("Date").Range.Text = Referral_WhatDate
            wdBooks("Description").Range.Text = Referral_Description
            wdBooks("EmployeeName").Range.Text = Referral_EmployeeName
            wdBooks("Referred").Range.Text = Referral_Referred
            wdBooks("StudentName").Range.Text = Referral_StudentName

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If opendb() Then
            Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Referral_StudenID & "','" & Referral & "','" & DatabasePath & "')"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                MessageBox.Show("Succesfuly Created.",
        "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub BunifuButton4_Click(sender As Object, e As EventArgs) Handles BunifuButton4.Click
        Referral = "Referral"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Referral + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ReferralPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisplayCase()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Referral_primary_id
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Referral_display_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Referral = "Referral"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Referral + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ReferralPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisplayCase()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Referral_primary_id
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Referral_display_form_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Main_Dashboard.DisableReferralBTN()
    End Sub

    Private Sub Open_btn_Click(sender As Object, e As EventArgs) Handles Open_btn.Click
        Referral = "Referral"
        Try
            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Referral + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        ReferralPath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisplayCase()
                End Try
            Else
                MsgBox("NO database connections")
            End If
            sFileName = Main_Dashboard.StudentName + "_" + Main_Dashboard.Referral_primary_id
            fullpath = SavePath + "\" + sFileName
            Process.Start($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub
End Class