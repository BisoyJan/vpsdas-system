﻿Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports MySql.Data.MySqlClient

Public Class Disciplinary_case_view_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Public Case_StudentID As String
    Public Case_Program_Section As String
    Public Case_Violation As String
    Public Case_Incident_Report As String
    Public Case_Resolution As String
    Public Case_Recommendations As String
    Public Case_Date As String
    Public Case_Chairmain As String
    Public Case_members As String
    Public Case_StudentName As String
    Public Case_Primarykey As String
    Public student_id As String

    Dim DatabasePath As String
    Dim CasePath As String
    Dim SavePath As String
    Dim fullpath As String
    Dim Cases As String

    Private Sub BunifuButton2_Click(sender As Object, e As EventArgs) Handles BunifuButton2.Click
        Cases = "case"
        'create
        If opendb() Then
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Cases + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    CasePath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try
            sFileName = Case_StudentName + "_" + Case_Primarykey
            fullpath = SavePath + "\" + sFileName 'save path with filename
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(CasePath) 'Locate Themeplate
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("StudentName").Range.Text = Case_StudentName
            wdBooks("StudentID").Range.Text = Case_StudentID
            wdBooks("Section").Range.Text = Case_Program_Section
            wdBooks("Violation").Range.Text = Case_Violation
            wdBooks("Report").Range.Text = Case_Incident_Report
            wdBooks("Resolution").Range.Text = Case_Resolution

            If Case_Resolution = "Closed/Resolve" Then
                wdBooks("Resolve").Range.Text = "X"
                wdBooks("Hearing").Range.Text = ""
                wdBooks("Investagation").Range.Text = ""
            ElseIf Case_Resolution = "For Investagation" Then
                wdBooks("Investagation").Range.Text = "X"
                wdBooks("Hearing").Range.Text = ""
                wdBooks("Resolve").Range.Text = ""
            ElseIf Case_Resolution = "Hearing Continuation" Then
                wdBooks("Hearing").Range.Text = "X"
                wdBooks("Investagation").Range.Text = ""
                wdBooks("Resolve").Range.Text = ""
                wdBooks("Date").Range.Text = Case_Date
            End If

            wdBooks("Chairman").Range.Text = Case_Chairmain
            wdBooks("Members").Range.Text = Case_members

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()

            DatabasePath = fullpath.Replace("\", "\\")

        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If opendb() Then
            Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & student_id & "','" & Cases & "','" & DatabasePath & "')"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                MessageBox.Show("Succesfuly Created.",
        "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisableCaseBTN()
            End Try
        Else
            MsgBox("NO database connections")
        End If

    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub BunifuButton4_Click(sender As Object, e As EventArgs) Handles BunifuButton4.Click
        'View
        'Try
        '    RichEditControl1.LoadDocument($"C:\Users\bisoy\My Drive (vpsdas.docx@gmail.com)\Students Documents\Cases\{Case_StudentName + "_" + Case_Primarykey}.docx")
        'Catch ex As Exception
        '    MsgBox("File not found, Deleted or Not Created")
        'End Try

        Cases = "case"
        Try

            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Cases + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        CasePath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

            sFileName = Case_StudentName + "_" + Case_Primarykey
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Disciplinary_case_view_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cases = "case"
        Try

            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Cases + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        CasePath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

            sFileName = Case_StudentName + "_" + Case_Primarykey
            fullpath = SavePath + "\" + sFileName
            RichEditControl1.LoadDocument($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("Document File not found, Deleted or Not Created")
        End Try
    End Sub

    Private Sub Disciplinary_case_view_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main_Dashboard.DisableCaseBTN()
    End Sub

    Private Sub Open_btn_Click(sender As Object, e As EventArgs) Handles Open_btn.Click
        Cases = "case"
        Try

            If opendb() Then
                Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Cases + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    While dtreader.Read

                        CasePath = dtreader.GetString("path")
                        SavePath = dtreader.GetString("save_path")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

            sFileName = Case_StudentName + "_" + Case_Primarykey
            fullpath = SavePath + "\" + sFileName
            Process.Start($"{fullpath}.docx")
        Catch ex As Exception
            MsgBox("Document File not found, Deleted or Not Created")
        End Try
 
    End Sub
End Class