﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions
Public Class Login
    Dim encrypted As String
    Dim decrypted As String
    Dim email As String
    Dim password As String

    Dim Name As String

    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Private Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function

    Private Sub Login_btn_Click(sender As Object, e As EventArgs) Handles Login_btn.Click

        If Email_txtbox.Text = "superadmin@gmail.com" And Password_txtbox.Text = "superadmin" Then

            Account_Form.Show()
        Else
            If opendb() Then
                Dim query As String = "SELECT `email`, `password`, `type`, `last_name`  FROM `users` WHERE email = '" & Email_txtbox.Text & "' AND password = '" & Me.Encrypt(Password_txtbox.Text) & "'"

                Dim cmd As New MySqlDataAdapter(query, conn)
                Dim ds As New DataSet
                Dim table As New DataTable()

                cmd.Fill(ds, "users")

                Try

                    If ds.Tables("users").Rows.Count = 0 Then
                        MsgBox("Email or password are not Correct",
                              MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
                                  "Invalid")
                    Else
                        If ds.Tables("users").Rows(0).Item("type").ToString = "Admin" Then
                            Name = ds.Tables("users").Rows(0).Item("last_name").ToString()
                            MsgBox("Welcome! " + Name,
                                 MsgBoxStyle.OkOnly Or MsgBoxStyle.Information,
                                     "Success")
                            Dashboard_data.AccountType = "Admin"
                            Dashboard_data.Check_Account()
                            Function_select_form.Show()
                            Me.Close()

                        Else
                            Name = ds.Tables("users").Rows(0).Item("last_name").ToString()
                            MsgBox("Welcome! " + Name,
                                 MsgBoxStyle.OkOnly Or MsgBoxStyle.Information,
                                     "Invalid")
                            Dashboard_data.AccountType = "User"
                            Dashboard_data.Check_Account()
                            Function_select_form.Show()
                            Me.Close()
                        End If
                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub Password_show_chckbox_CheckedChanged(sender As Object, e As Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs) Handles Password_show_chckbox.CheckedChanged
        If Password_show_chckbox.Checked Then
            Password_txtbox.PasswordChar = ""
        Else
            Password_txtbox.PasswordChar = "*"
        End If
    End Sub

    'Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    '    Me.Hide()
    'End Sub
End Class