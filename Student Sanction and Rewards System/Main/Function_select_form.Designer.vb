﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Function_select_form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Function_select_form))
        Dim BorderEdges1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges2 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Me.Disciplinary_view_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.BunifuButton1 = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.SuspendLayout()
        '
        'Disciplinary_view_btn
        '
        Me.Disciplinary_view_btn.AllowAnimations = True
        Me.Disciplinary_view_btn.AllowMouseEffects = True
        Me.Disciplinary_view_btn.AllowToggling = False
        Me.Disciplinary_view_btn.AnimationSpeed = 200
        Me.Disciplinary_view_btn.AutoGenerateColors = False
        Me.Disciplinary_view_btn.AutoRoundBorders = False
        Me.Disciplinary_view_btn.AutoSizeLeftIcon = True
        Me.Disciplinary_view_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Disciplinary_view_btn.AutoSizeRightIcon = True
        Me.Disciplinary_view_btn.BackColor = System.Drawing.Color.Transparent
        Me.Disciplinary_view_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.BackgroundImage = CType(resources.GetObject("Disciplinary_view_btn.BackgroundImage"), System.Drawing.Image)
        Me.Disciplinary_view_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Disciplinary_view_btn.ButtonText = "Sanctioning"
        Me.Disciplinary_view_btn.ButtonTextMarginLeft = 0
        Me.Disciplinary_view_btn.ColorContrastOnClick = 45
        Me.Disciplinary_view_btn.ColorContrastOnHover = 45
        Me.Disciplinary_view_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges1.BottomLeft = True
        BorderEdges1.BottomRight = True
        BorderEdges1.TopLeft = True
        BorderEdges1.TopRight = True
        Me.Disciplinary_view_btn.CustomizableEdges = BorderEdges1
        Me.Disciplinary_view_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Disciplinary_view_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Disciplinary_view_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Disciplinary_view_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Disciplinary_view_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Disciplinary_view_btn.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Disciplinary_view_btn.ForeColor = System.Drawing.Color.White
        Me.Disciplinary_view_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Disciplinary_view_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Disciplinary_view_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.Disciplinary_view_btn.IconMarginLeft = 11
        Me.Disciplinary_view_btn.IconPadding = 10
        Me.Disciplinary_view_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Disciplinary_view_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Disciplinary_view_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Disciplinary_view_btn.IconSize = 1
        Me.Disciplinary_view_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.IdleBorderRadius = 30
        Me.Disciplinary_view_btn.IdleBorderThickness = 1
        Me.Disciplinary_view_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.IdleIconLeftImage = Nothing
        Me.Disciplinary_view_btn.IdleIconRightImage = Nothing
        Me.Disciplinary_view_btn.IndicateFocus = True
        Me.Disciplinary_view_btn.Location = New System.Drawing.Point(137, 109)
        Me.Disciplinary_view_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Disciplinary_view_btn.Name = "Disciplinary_view_btn"
        Me.Disciplinary_view_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Disciplinary_view_btn.OnDisabledState.BorderRadius = 30
        Me.Disciplinary_view_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Disciplinary_view_btn.OnDisabledState.BorderThickness = 1
        Me.Disciplinary_view_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Disciplinary_view_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Disciplinary_view_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Disciplinary_view_btn.OnDisabledState.IconRightImage = Nothing
        Me.Disciplinary_view_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.onHoverState.BorderRadius = 30
        Me.Disciplinary_view_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Disciplinary_view_btn.onHoverState.BorderThickness = 1
        Me.Disciplinary_view_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Disciplinary_view_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Disciplinary_view_btn.onHoverState.IconLeftImage = Nothing
        Me.Disciplinary_view_btn.onHoverState.IconRightImage = Nothing
        Me.Disciplinary_view_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.OnIdleState.BorderRadius = 30
        Me.Disciplinary_view_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Disciplinary_view_btn.OnIdleState.BorderThickness = 1
        Me.Disciplinary_view_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Disciplinary_view_btn.OnIdleState.IconLeftImage = Nothing
        Me.Disciplinary_view_btn.OnIdleState.IconRightImage = Nothing
        Me.Disciplinary_view_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Disciplinary_view_btn.OnPressedState.BorderRadius = 30
        Me.Disciplinary_view_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Disciplinary_view_btn.OnPressedState.BorderThickness = 1
        Me.Disciplinary_view_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Disciplinary_view_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Disciplinary_view_btn.OnPressedState.IconLeftImage = Nothing
        Me.Disciplinary_view_btn.OnPressedState.IconRightImage = Nothing
        Me.Disciplinary_view_btn.Size = New System.Drawing.Size(226, 220)
        Me.Disciplinary_view_btn.TabIndex = 14
        Me.Disciplinary_view_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Disciplinary_view_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Disciplinary_view_btn.TextMarginLeft = 0
        Me.Disciplinary_view_btn.TextPadding = New System.Windows.Forms.Padding(5, 6, 0, 0)
        Me.Disciplinary_view_btn.UseDefaultRadiusAndThickness = True
        '
        'BunifuButton1
        '
        Me.BunifuButton1.AllowAnimations = True
        Me.BunifuButton1.AllowMouseEffects = True
        Me.BunifuButton1.AllowToggling = False
        Me.BunifuButton1.AnimationSpeed = 200
        Me.BunifuButton1.AutoGenerateColors = False
        Me.BunifuButton1.AutoRoundBorders = False
        Me.BunifuButton1.AutoSizeLeftIcon = True
        Me.BunifuButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BunifuButton1.AutoSizeRightIcon = True
        Me.BunifuButton1.BackColor = System.Drawing.Color.Transparent
        Me.BunifuButton1.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.BackgroundImage = CType(resources.GetObject("BunifuButton1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuButton1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.BunifuButton1.ButtonText = "Rewards"
        Me.BunifuButton1.ButtonTextMarginLeft = 0
        Me.BunifuButton1.ColorContrastOnClick = 45
        Me.BunifuButton1.ColorContrastOnHover = 45
        Me.BunifuButton1.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges2.BottomLeft = True
        BorderEdges2.BottomRight = True
        BorderEdges2.TopLeft = True
        BorderEdges2.TopRight = True
        Me.BunifuButton1.CustomizableEdges = BorderEdges2
        Me.BunifuButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.BunifuButton1.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BunifuButton1.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.BunifuButton1.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.BunifuButton1.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.BunifuButton1.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuButton1.ForeColor = System.Drawing.Color.White
        Me.BunifuButton1.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuButton1.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.BunifuButton1.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.BunifuButton1.IconMarginLeft = 11
        Me.BunifuButton1.IconPadding = 10
        Me.BunifuButton1.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BunifuButton1.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.BunifuButton1.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.BunifuButton1.IconSize = 1
        Me.BunifuButton1.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.IdleBorderRadius = 30
        Me.BunifuButton1.IdleBorderThickness = 1
        Me.BunifuButton1.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.IdleIconLeftImage = Nothing
        Me.BunifuButton1.IdleIconRightImage = Nothing
        Me.BunifuButton1.IndicateFocus = True
        Me.BunifuButton1.Location = New System.Drawing.Point(434, 109)
        Me.BunifuButton1.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.BunifuButton1.Name = "BunifuButton1"
        Me.BunifuButton1.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BunifuButton1.OnDisabledState.BorderRadius = 30
        Me.BunifuButton1.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.BunifuButton1.OnDisabledState.BorderThickness = 1
        Me.BunifuButton1.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.BunifuButton1.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.BunifuButton1.OnDisabledState.IconLeftImage = Nothing
        Me.BunifuButton1.OnDisabledState.IconRightImage = Nothing
        Me.BunifuButton1.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.onHoverState.BorderRadius = 30
        Me.BunifuButton1.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.BunifuButton1.onHoverState.BorderThickness = 1
        Me.BunifuButton1.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuButton1.onHoverState.ForeColor = System.Drawing.Color.White
        Me.BunifuButton1.onHoverState.IconLeftImage = Nothing
        Me.BunifuButton1.onHoverState.IconRightImage = Nothing
        Me.BunifuButton1.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.OnIdleState.BorderRadius = 30
        Me.BunifuButton1.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.BunifuButton1.OnIdleState.BorderThickness = 1
        Me.BunifuButton1.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.BunifuButton1.OnIdleState.IconLeftImage = Nothing
        Me.BunifuButton1.OnIdleState.IconRightImage = Nothing
        Me.BunifuButton1.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuButton1.OnPressedState.BorderRadius = 30
        Me.BunifuButton1.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.BunifuButton1.OnPressedState.BorderThickness = 1
        Me.BunifuButton1.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuButton1.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.BunifuButton1.OnPressedState.IconLeftImage = Nothing
        Me.BunifuButton1.OnPressedState.IconRightImage = Nothing
        Me.BunifuButton1.Size = New System.Drawing.Size(226, 220)
        Me.BunifuButton1.TabIndex = 15
        Me.BunifuButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BunifuButton1.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.BunifuButton1.TextMarginLeft = 0
        Me.BunifuButton1.TextPadding = New System.Windows.Forms.Padding(5, 6, 0, 0)
        Me.BunifuButton1.UseDefaultRadiusAndThickness = True
        '
        'Function_select_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.BunifuButton1)
        Me.Controls.Add(Me.Disciplinary_view_btn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "Function_select_form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Disciplinary_view_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents BunifuButton1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton
End Class
