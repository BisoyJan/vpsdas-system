﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Account_Dialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Account_Dialog))
        Dim BorderEdges3 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim StateProperties49 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties50 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties51 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties52 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties53 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties54 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties55 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties56 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties57 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties58 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties59 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties60 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties61 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties62 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties63 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties64 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties65 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties66 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties67 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties68 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties69 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties70 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties71 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties72 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Me.Program_panel = New Bunifu.UI.WinForms.BunifuShadowPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.Account_add_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuLabel10 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.Password_show_chckbox = New Bunifu.UI.WinForms.BunifuCheckBox()
        Me.BunifuLabel9 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Password_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.Repassword_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.AcountType_drpdown = New Bunifu.UI.WinForms.BunifuDropdown()
        Me.BunifuLabel8 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuLabel7 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuLabel6 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuLabel2 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Fname_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.BunifuLabel3 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Mname_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.BunifuLabel4 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Lname_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.BunifuLabel5 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Email_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.Program_panel.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Program_panel
        '
        Me.Program_panel.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Program_panel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Program_panel.BorderRadius = 0
        Me.Program_panel.BorderThickness = 0
        Me.Program_panel.Controls.Add(Me.TableLayoutPanel1)
        Me.Program_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Program_panel.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Gradient
        Me.Program_panel.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Horizontal
        Me.Program_panel.Location = New System.Drawing.Point(0, 0)
        Me.Program_panel.Name = "Program_panel"
        Me.Program_panel.Padding = New System.Windows.Forms.Padding(5)
        Me.Program_panel.PanelColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Program_panel.PanelColor2 = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Program_panel.ShadowColor = System.Drawing.Color.Gray
        Me.Program_panel.ShadowDept = 2
        Me.Program_panel.ShadowDepth = 3
        Me.Program_panel.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded
        Me.Program_panel.ShadowTopLeftVisible = False
        Me.Program_panel.Size = New System.Drawing.Size(646, 549)
        Me.Program_panel.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat
        Me.Program_panel.TabIndex = 36
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel9, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(5, 5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.84458!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.15542!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(636, 539)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 3
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001!))
        Me.TableLayoutPanel9.Controls.Add(Me.Account_add_btn, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 460)
        Me.TableLayoutPanel9.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(636, 79)
        Me.TableLayoutPanel9.TabIndex = 9
        '
        'Account_add_btn
        '
        Me.Account_add_btn.AllowAnimations = True
        Me.Account_add_btn.AllowMouseEffects = True
        Me.Account_add_btn.AllowToggling = False
        Me.Account_add_btn.AnimationSpeed = 200
        Me.Account_add_btn.AutoGenerateColors = False
        Me.Account_add_btn.AutoRoundBorders = False
        Me.Account_add_btn.AutoSizeLeftIcon = True
        Me.Account_add_btn.AutoSizeRightIcon = True
        Me.Account_add_btn.BackColor = System.Drawing.Color.Transparent
        Me.Account_add_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.BackgroundImage = CType(resources.GetObject("Account_add_btn.BackgroundImage"), System.Drawing.Image)
        Me.Account_add_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.ButtonText = "Save"
        Me.Account_add_btn.ButtonTextMarginLeft = 0
        Me.Account_add_btn.ColorContrastOnClick = 45
        Me.Account_add_btn.ColorContrastOnHover = 45
        Me.Account_add_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges3.BottomLeft = True
        BorderEdges3.BottomRight = True
        BorderEdges3.TopLeft = True
        BorderEdges3.TopRight = True
        Me.Account_add_btn.CustomizableEdges = BorderEdges3
        Me.Account_add_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Account_add_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_add_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_add_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_add_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Account_add_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_add_btn.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Account_add_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Account_add_btn.IconLeftPadding = New System.Windows.Forms.Padding(35, 3, 3, 3)
        Me.Account_add_btn.IconMarginLeft = 11
        Me.Account_add_btn.IconPadding = 10
        Me.Account_add_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Account_add_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Account_add_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Account_add_btn.IconSize = 25
        Me.Account_add_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.IdleBorderRadius = 30
        Me.Account_add_btn.IdleBorderThickness = 1
        Me.Account_add_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.IdleIconLeftImage = CType(resources.GetObject("Account_add_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Account_add_btn.IdleIconRightImage = Nothing
        Me.Account_add_btn.IndicateFocus = True
        Me.Account_add_btn.Location = New System.Drawing.Point(242, 10)
        Me.Account_add_btn.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.Account_add_btn.Name = "Account_add_btn"
        Me.Account_add_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.BorderRadius = 30
        Me.Account_add_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnDisabledState.BorderThickness = 1
        Me.Account_add_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Account_add_btn.OnDisabledState.IconRightImage = Nothing
        Me.Account_add_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.onHoverState.BorderRadius = 30
        Me.Account_add_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.onHoverState.BorderThickness = 1
        Me.Account_add_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.onHoverState.IconLeftImage = Nothing
        Me.Account_add_btn.onHoverState.IconRightImage = Nothing
        Me.Account_add_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.OnIdleState.BorderRadius = 30
        Me.Account_add_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnIdleState.BorderThickness = 1
        Me.Account_add_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Account_add_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Account_add_btn.OnIdleState.IconRightImage = Nothing
        Me.Account_add_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.OnPressedState.BorderRadius = 30
        Me.Account_add_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnPressedState.BorderThickness = 1
        Me.Account_add_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.OnPressedState.IconLeftImage = Nothing
        Me.Account_add_btn.OnPressedState.IconRightImage = Nothing
        Me.Account_add_btn.Size = New System.Drawing.Size(151, 52)
        Me.Account_add_btn.TabIndex = 26
        Me.Account_add_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Account_add_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Account_add_btn.TextMarginLeft = 0
        Me.Account_add_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Account_add_btn.UseDefaultRadiusAndThickness = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuLabel10, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(636, 50)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'BunifuLabel10
        '
        Me.BunifuLabel10.AllowParentOverrides = False
        Me.BunifuLabel10.AutoEllipsis = False
        Me.BunifuLabel10.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel10.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel10.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel10.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel10.Location = New System.Drawing.Point(10, 10)
        Me.BunifuLabel10.Margin = New System.Windows.Forms.Padding(10, 10, 0, 0)
        Me.BunifuLabel10.Name = "BunifuLabel10"
        Me.BunifuLabel10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel10.Size = New System.Drawing.Size(300, 21)
        Me.BunifuLabel10.TabIndex = 7
        Me.BunifuLabel10.Text = "Double Check if the Information is corect"
        Me.BunifuLabel10.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel10.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel5, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 50)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(636, 410)
        Me.TableLayoutPanel3.TabIndex = 3
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.TableLayoutPanel6, 0, 6)
        Me.TableLayoutPanel5.Controls.Add(Me.Password_txtbox, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.Repassword_txtbox, 0, 5)
        Me.TableLayoutPanel5.Controls.Add(Me.AcountType_drpdown, 0, 9)
        Me.TableLayoutPanel5.Controls.Add(Me.BunifuLabel8, 0, 8)
        Me.TableLayoutPanel5.Controls.Add(Me.BunifuLabel7, 0, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.BunifuLabel6, 0, 1)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(318, 0)
        Me.TableLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 13
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.92357!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.28662!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.19745!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(318, 410)
        Me.TableLayoutPanel5.TabIndex = 1
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel6.Controls.Add(Me.Password_show_chckbox, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.BunifuLabel9, 1, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 193)
        Me.TableLayoutPanel6.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(318, 40)
        Me.TableLayoutPanel6.TabIndex = 54
        '
        'Password_show_chckbox
        '
        Me.Password_show_chckbox.AllowBindingControlAnimation = True
        Me.Password_show_chckbox.AllowBindingControlColorChanges = False
        Me.Password_show_chckbox.AllowBindingControlLocation = True
        Me.Password_show_chckbox.AllowCheckBoxAnimation = False
        Me.Password_show_chckbox.AllowCheckmarkAnimation = True
        Me.Password_show_chckbox.AllowOnHoverStates = True
        Me.Password_show_chckbox.AutoCheck = True
        Me.Password_show_chckbox.BackColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.BackgroundImage = CType(resources.GetObject("Password_show_chckbox.BackgroundImage"), System.Drawing.Image)
        Me.Password_show_chckbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Password_show_chckbox.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right
        Me.Password_show_chckbox.BorderRadius = 12
        Me.Password_show_chckbox.Checked = False
        Me.Password_show_chckbox.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked
        Me.Password_show_chckbox.Cursor = System.Windows.Forms.Cursors.Default
        Me.Password_show_chckbox.CustomCheckmarkImage = Nothing
        Me.Password_show_chckbox.Location = New System.Drawing.Point(30, 10)
        Me.Password_show_chckbox.Margin = New System.Windows.Forms.Padding(30, 10, 3, 3)
        Me.Password_show_chckbox.MinimumSize = New System.Drawing.Size(17, 17)
        Me.Password_show_chckbox.Name = "Password_show_chckbox"
        Me.Password_show_chckbox.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue
        Me.Password_show_chckbox.OnCheck.BorderRadius = 12
        Me.Password_show_chckbox.OnCheck.BorderThickness = 2
        Me.Password_show_chckbox.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue
        Me.Password_show_chckbox.OnCheck.CheckmarkColor = System.Drawing.Color.White
        Me.Password_show_chckbox.OnCheck.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnDisable.BorderColor = System.Drawing.Color.LightGray
        Me.Password_show_chckbox.OnDisable.BorderRadius = 12
        Me.Password_show_chckbox.OnDisable.BorderThickness = 2
        Me.Password_show_chckbox.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray
        Me.Password_show_chckbox.OnDisable.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverChecked.BorderRadius = 12
        Me.Password_show_chckbox.OnHoverChecked.BorderThickness = 2
        Me.Password_show_chckbox.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White
        Me.Password_show_chckbox.OnHoverChecked.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverUnchecked.BorderRadius = 12
        Me.Password_show_chckbox.OnHoverUnchecked.BorderThickness = 1
        Me.Password_show_chckbox.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.OnUncheck.BorderColor = System.Drawing.Color.DarkGray
        Me.Password_show_chckbox.OnUncheck.BorderRadius = 12
        Me.Password_show_chckbox.OnUncheck.BorderThickness = 1
        Me.Password_show_chckbox.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.Size = New System.Drawing.Size(21, 21)
        Me.Password_show_chckbox.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu
        Me.Password_show_chckbox.TabIndex = 52
        Me.Password_show_chckbox.ThreeState = False
        Me.Password_show_chckbox.ToolTipText = Nothing
        '
        'BunifuLabel9
        '
        Me.BunifuLabel9.AllowParentOverrides = False
        Me.BunifuLabel9.AutoEllipsis = False
        Me.BunifuLabel9.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel9.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuLabel9.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel9.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel9.Location = New System.Drawing.Point(59, 5)
        Me.BunifuLabel9.Margin = New System.Windows.Forms.Padding(5, 5, 0, 0)
        Me.BunifuLabel9.Name = "BunifuLabel9"
        Me.BunifuLabel9.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
        Me.BunifuLabel9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel9.Size = New System.Drawing.Size(259, 35)
        Me.BunifuLabel9.TabIndex = 53
        Me.BunifuLabel9.Text = "Show Password"
        Me.BunifuLabel9.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel9.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Password_txtbox
        '
        Me.Password_txtbox.AcceptsReturn = False
        Me.Password_txtbox.AcceptsTab = False
        Me.Password_txtbox.AnimationSpeed = 200
        Me.Password_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Password_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Password_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Password_txtbox.BackgroundImage = CType(resources.GetObject("Password_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Password_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Password_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Password_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Password_txtbox.BorderRadius = 10
        Me.Password_txtbox.BorderThickness = 1
        Me.Password_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Password_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Password_txtbox.DefaultText = ""
        Me.Password_txtbox.FillColor = System.Drawing.Color.White
        Me.Password_txtbox.HideSelection = True
        Me.Password_txtbox.IconLeft = Nothing
        Me.Password_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.IconPadding = 10
        Me.Password_txtbox.IconRight = Nothing
        Me.Password_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.Lines = New String(-1) {}
        Me.Password_txtbox.Location = New System.Drawing.Point(30, 57)
        Me.Password_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Password_txtbox.MaxLength = 32767
        Me.Password_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Password_txtbox.Modified = False
        Me.Password_txtbox.Multiline = False
        Me.Password_txtbox.Name = "Password_txtbox"
        StateProperties49.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties49.FillColor = System.Drawing.Color.Empty
        StateProperties49.ForeColor = System.Drawing.Color.Empty
        StateProperties49.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnActiveState = StateProperties49
        StateProperties50.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties50.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties50.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties50.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Password_txtbox.OnDisabledState = StateProperties50
        StateProperties51.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties51.FillColor = System.Drawing.Color.Empty
        StateProperties51.ForeColor = System.Drawing.Color.Empty
        StateProperties51.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnHoverState = StateProperties51
        StateProperties52.BorderColor = System.Drawing.Color.Silver
        StateProperties52.FillColor = System.Drawing.Color.White
        StateProperties52.ForeColor = System.Drawing.Color.Empty
        StateProperties52.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnIdleState = StateProperties52
        Me.Password_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Password_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Password_txtbox.PlaceholderText = ""
        Me.Password_txtbox.ReadOnly = False
        Me.Password_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Password_txtbox.SelectedText = ""
        Me.Password_txtbox.SelectionLength = 0
        Me.Password_txtbox.SelectionStart = 0
        Me.Password_txtbox.ShortcutsEnabled = True
        Me.Password_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Password_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Password_txtbox.TabIndex = 37
        Me.Password_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Password_txtbox.TextMarginBottom = 0
        Me.Password_txtbox.TextMarginLeft = 3
        Me.Password_txtbox.TextMarginTop = 0
        Me.Password_txtbox.TextPlaceholder = ""
        Me.Password_txtbox.UseSystemPasswordChar = False
        Me.Password_txtbox.WordWrap = True
        '
        'Repassword_txtbox
        '
        Me.Repassword_txtbox.AcceptsReturn = False
        Me.Repassword_txtbox.AcceptsTab = False
        Me.Repassword_txtbox.AnimationSpeed = 200
        Me.Repassword_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Repassword_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Repassword_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Repassword_txtbox.BackgroundImage = CType(resources.GetObject("Repassword_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Repassword_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Repassword_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Repassword_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Repassword_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Repassword_txtbox.BorderRadius = 10
        Me.Repassword_txtbox.BorderThickness = 1
        Me.Repassword_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Repassword_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Repassword_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Repassword_txtbox.DefaultText = ""
        Me.Repassword_txtbox.FillColor = System.Drawing.Color.White
        Me.Repassword_txtbox.HideSelection = True
        Me.Repassword_txtbox.IconLeft = Nothing
        Me.Repassword_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Repassword_txtbox.IconPadding = 10
        Me.Repassword_txtbox.IconRight = CType(resources.GetObject("Repassword_txtbox.IconRight"), System.Drawing.Image)
        Me.Repassword_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Repassword_txtbox.Lines = New String(-1) {}
        Me.Repassword_txtbox.Location = New System.Drawing.Point(30, 153)
        Me.Repassword_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Repassword_txtbox.MaxLength = 32767
        Me.Repassword_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Repassword_txtbox.Modified = False
        Me.Repassword_txtbox.Multiline = False
        Me.Repassword_txtbox.Name = "Repassword_txtbox"
        StateProperties53.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties53.FillColor = System.Drawing.Color.Empty
        StateProperties53.ForeColor = System.Drawing.Color.Empty
        StateProperties53.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Repassword_txtbox.OnActiveState = StateProperties53
        StateProperties54.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties54.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties54.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties54.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Repassword_txtbox.OnDisabledState = StateProperties54
        StateProperties55.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties55.FillColor = System.Drawing.Color.Empty
        StateProperties55.ForeColor = System.Drawing.Color.Empty
        StateProperties55.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Repassword_txtbox.OnHoverState = StateProperties55
        StateProperties56.BorderColor = System.Drawing.Color.Silver
        StateProperties56.FillColor = System.Drawing.Color.White
        StateProperties56.ForeColor = System.Drawing.Color.Empty
        StateProperties56.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Repassword_txtbox.OnIdleState = StateProperties56
        Me.Repassword_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Repassword_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Repassword_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Repassword_txtbox.PlaceholderText = ""
        Me.Repassword_txtbox.ReadOnly = False
        Me.Repassword_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Repassword_txtbox.SelectedText = ""
        Me.Repassword_txtbox.SelectionLength = 0
        Me.Repassword_txtbox.SelectionStart = 0
        Me.Repassword_txtbox.ShortcutsEnabled = True
        Me.Repassword_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Repassword_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Repassword_txtbox.TabIndex = 39
        Me.Repassword_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Repassword_txtbox.TextMarginBottom = 0
        Me.Repassword_txtbox.TextMarginLeft = 3
        Me.Repassword_txtbox.TextMarginTop = 0
        Me.Repassword_txtbox.TextPlaceholder = ""
        Me.Repassword_txtbox.UseSystemPasswordChar = False
        Me.Repassword_txtbox.WordWrap = True
        '
        'AcountType_drpdown
        '
        Me.AcountType_drpdown.BackColor = System.Drawing.Color.Transparent
        Me.AcountType_drpdown.BackgroundColor = System.Drawing.Color.White
        Me.AcountType_drpdown.BorderColor = System.Drawing.Color.Silver
        Me.AcountType_drpdown.BorderRadius = 10
        Me.AcountType_drpdown.Color = System.Drawing.Color.Silver
        Me.AcountType_drpdown.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down
        Me.AcountType_drpdown.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.AcountType_drpdown.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.AcountType_drpdown.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.AcountType_drpdown.DisabledForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.AcountType_drpdown.DisabledIndicatorColor = System.Drawing.Color.DarkGray
        Me.AcountType_drpdown.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.AcountType_drpdown.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thin
        Me.AcountType_drpdown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AcountType_drpdown.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.AcountType_drpdown.FillDropDown = True
        Me.AcountType_drpdown.FillIndicator = False
        Me.AcountType_drpdown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AcountType_drpdown.Font = New System.Drawing.Font("Segoe UI", 11.25!)
        Me.AcountType_drpdown.ForeColor = System.Drawing.Color.Black
        Me.AcountType_drpdown.FormattingEnabled = True
        Me.AcountType_drpdown.Icon = Nothing
        Me.AcountType_drpdown.IndicatorAlignment = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.AcountType_drpdown.IndicatorColor = System.Drawing.Color.Gray
        Me.AcountType_drpdown.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.AcountType_drpdown.ItemBackColor = System.Drawing.Color.White
        Me.AcountType_drpdown.ItemBorderColor = System.Drawing.Color.White
        Me.AcountType_drpdown.ItemForeColor = System.Drawing.Color.Black
        Me.AcountType_drpdown.ItemHeight = 26
        Me.AcountType_drpdown.ItemHighLightColor = System.Drawing.Color.DodgerBlue
        Me.AcountType_drpdown.ItemHighLightForeColor = System.Drawing.Color.White
        Me.AcountType_drpdown.Items.AddRange(New Object() {"Admin", "User"})
        Me.AcountType_drpdown.ItemTopMargin = 3
        Me.AcountType_drpdown.Location = New System.Drawing.Point(30, 292)
        Me.AcountType_drpdown.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.AcountType_drpdown.Name = "AcountType_drpdown"
        Me.AcountType_drpdown.Size = New System.Drawing.Size(105, 32)
        Me.AcountType_drpdown.TabIndex = 49
        Me.AcountType_drpdown.Text = Nothing
        Me.AcountType_drpdown.TextAlignment = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.AcountType_drpdown.TextLeftMargin = 5
        '
        'BunifuLabel8
        '
        Me.BunifuLabel8.AllowParentOverrides = False
        Me.BunifuLabel8.AutoEllipsis = False
        Me.BunifuLabel8.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel8.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel8.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel8.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel8.Location = New System.Drawing.Point(10, 261)
        Me.BunifuLabel8.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel8.Name = "BunifuLabel8"
        Me.BunifuLabel8.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel8.Size = New System.Drawing.Size(98, 31)
        Me.BunifuLabel8.TabIndex = 50
        Me.BunifuLabel8.Text = "Account type"
        Me.BunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuLabel7
        '
        Me.BunifuLabel7.AllowParentOverrides = False
        Me.BunifuLabel7.AutoEllipsis = False
        Me.BunifuLabel7.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel7.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel7.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel7.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel7.Location = New System.Drawing.Point(10, 122)
        Me.BunifuLabel7.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel7.Name = "BunifuLabel7"
        Me.BunifuLabel7.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel7.Size = New System.Drawing.Size(129, 31)
        Me.BunifuLabel7.TabIndex = 48
        Me.BunifuLabel7.Text = "Retype Password:"
        Me.BunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuLabel6
        '
        Me.BunifuLabel6.AllowParentOverrides = False
        Me.BunifuLabel6.AutoEllipsis = False
        Me.BunifuLabel6.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel6.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel6.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel6.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel6.Location = New System.Drawing.Point(10, 26)
        Me.BunifuLabel6.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel6.Name = "BunifuLabel6"
        Me.BunifuLabel6.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel6.Size = New System.Drawing.Size(73, 31)
        Me.BunifuLabel6.TabIndex = 47
        Me.BunifuLabel6.Text = "Password:"
        Me.BunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.BunifuLabel2, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Fname_txtbox, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.BunifuLabel3, 0, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.Mname_txtbox, 0, 5)
        Me.TableLayoutPanel4.Controls.Add(Me.BunifuLabel4, 0, 7)
        Me.TableLayoutPanel4.Controls.Add(Me.Lname_txtbox, 0, 8)
        Me.TableLayoutPanel4.Controls.Add(Me.BunifuLabel5, 0, 10)
        Me.TableLayoutPanel4.Controls.Add(Me.Email_txtbox, 0, 11)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 13
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(318, 410)
        Me.TableLayoutPanel4.TabIndex = 0
        '
        'BunifuLabel2
        '
        Me.BunifuLabel2.AllowParentOverrides = False
        Me.BunifuLabel2.AutoEllipsis = False
        Me.BunifuLabel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel2.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel2.Location = New System.Drawing.Point(10, 25)
        Me.BunifuLabel2.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel2.Name = "BunifuLabel2"
        Me.BunifuLabel2.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel2.Size = New System.Drawing.Size(82, 31)
        Me.BunifuLabel2.TabIndex = 36
        Me.BunifuLabel2.Text = "First Name:"
        Me.BunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Fname_txtbox
        '
        Me.Fname_txtbox.AcceptsReturn = False
        Me.Fname_txtbox.AcceptsTab = False
        Me.Fname_txtbox.AnimationSpeed = 200
        Me.Fname_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Fname_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Fname_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Fname_txtbox.BackgroundImage = CType(resources.GetObject("Fname_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Fname_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Fname_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Fname_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Fname_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Fname_txtbox.BorderRadius = 10
        Me.Fname_txtbox.BorderThickness = 1
        Me.Fname_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Fname_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Fname_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fname_txtbox.DefaultText = ""
        Me.Fname_txtbox.FillColor = System.Drawing.Color.White
        Me.Fname_txtbox.HideSelection = True
        Me.Fname_txtbox.IconLeft = Nothing
        Me.Fname_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Fname_txtbox.IconPadding = 10
        Me.Fname_txtbox.IconRight = Nothing
        Me.Fname_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Fname_txtbox.Lines = New String(-1) {}
        Me.Fname_txtbox.Location = New System.Drawing.Point(30, 56)
        Me.Fname_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Fname_txtbox.MaxLength = 32767
        Me.Fname_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Fname_txtbox.Modified = False
        Me.Fname_txtbox.Multiline = False
        Me.Fname_txtbox.Name = "Fname_txtbox"
        StateProperties57.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties57.FillColor = System.Drawing.Color.Empty
        StateProperties57.ForeColor = System.Drawing.Color.Empty
        StateProperties57.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Fname_txtbox.OnActiveState = StateProperties57
        StateProperties58.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties58.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties58.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties58.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Fname_txtbox.OnDisabledState = StateProperties58
        StateProperties59.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties59.FillColor = System.Drawing.Color.Empty
        StateProperties59.ForeColor = System.Drawing.Color.Empty
        StateProperties59.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Fname_txtbox.OnHoverState = StateProperties59
        StateProperties60.BorderColor = System.Drawing.Color.Silver
        StateProperties60.FillColor = System.Drawing.Color.White
        StateProperties60.ForeColor = System.Drawing.Color.Empty
        StateProperties60.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Fname_txtbox.OnIdleState = StateProperties60
        Me.Fname_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Fname_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Fname_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Fname_txtbox.PlaceholderText = ""
        Me.Fname_txtbox.ReadOnly = False
        Me.Fname_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Fname_txtbox.SelectedText = ""
        Me.Fname_txtbox.SelectionLength = 0
        Me.Fname_txtbox.SelectionStart = 0
        Me.Fname_txtbox.ShortcutsEnabled = True
        Me.Fname_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Fname_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Fname_txtbox.TabIndex = 37
        Me.Fname_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Fname_txtbox.TextMarginBottom = 0
        Me.Fname_txtbox.TextMarginLeft = 3
        Me.Fname_txtbox.TextMarginTop = 0
        Me.Fname_txtbox.TextPlaceholder = ""
        Me.Fname_txtbox.UseSystemPasswordChar = False
        Me.Fname_txtbox.WordWrap = True
        '
        'BunifuLabel3
        '
        Me.BunifuLabel3.AllowParentOverrides = False
        Me.BunifuLabel3.AutoEllipsis = False
        Me.BunifuLabel3.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel3.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel3.Location = New System.Drawing.Point(10, 121)
        Me.BunifuLabel3.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel3.Name = "BunifuLabel3"
        Me.BunifuLabel3.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel3.Size = New System.Drawing.Size(103, 31)
        Me.BunifuLabel3.TabIndex = 38
        Me.BunifuLabel3.Text = "Middle Name:"
        Me.BunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Mname_txtbox
        '
        Me.Mname_txtbox.AcceptsReturn = False
        Me.Mname_txtbox.AcceptsTab = False
        Me.Mname_txtbox.AnimationSpeed = 200
        Me.Mname_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Mname_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Mname_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Mname_txtbox.BackgroundImage = CType(resources.GetObject("Mname_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Mname_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Mname_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Mname_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Mname_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Mname_txtbox.BorderRadius = 10
        Me.Mname_txtbox.BorderThickness = 1
        Me.Mname_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Mname_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Mname_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Mname_txtbox.DefaultText = ""
        Me.Mname_txtbox.FillColor = System.Drawing.Color.White
        Me.Mname_txtbox.HideSelection = True
        Me.Mname_txtbox.IconLeft = Nothing
        Me.Mname_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Mname_txtbox.IconPadding = 10
        Me.Mname_txtbox.IconRight = Nothing
        Me.Mname_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Mname_txtbox.Lines = New String(-1) {}
        Me.Mname_txtbox.Location = New System.Drawing.Point(30, 152)
        Me.Mname_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Mname_txtbox.MaxLength = 32767
        Me.Mname_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Mname_txtbox.Modified = False
        Me.Mname_txtbox.Multiline = False
        Me.Mname_txtbox.Name = "Mname_txtbox"
        StateProperties61.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties61.FillColor = System.Drawing.Color.Empty
        StateProperties61.ForeColor = System.Drawing.Color.Empty
        StateProperties61.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Mname_txtbox.OnActiveState = StateProperties61
        StateProperties62.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties62.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties62.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties62.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Mname_txtbox.OnDisabledState = StateProperties62
        StateProperties63.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties63.FillColor = System.Drawing.Color.Empty
        StateProperties63.ForeColor = System.Drawing.Color.Empty
        StateProperties63.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Mname_txtbox.OnHoverState = StateProperties63
        StateProperties64.BorderColor = System.Drawing.Color.Silver
        StateProperties64.FillColor = System.Drawing.Color.White
        StateProperties64.ForeColor = System.Drawing.Color.Empty
        StateProperties64.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Mname_txtbox.OnIdleState = StateProperties64
        Me.Mname_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Mname_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Mname_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Mname_txtbox.PlaceholderText = ""
        Me.Mname_txtbox.ReadOnly = False
        Me.Mname_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Mname_txtbox.SelectedText = ""
        Me.Mname_txtbox.SelectionLength = 0
        Me.Mname_txtbox.SelectionStart = 0
        Me.Mname_txtbox.ShortcutsEnabled = True
        Me.Mname_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Mname_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Mname_txtbox.TabIndex = 39
        Me.Mname_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Mname_txtbox.TextMarginBottom = 0
        Me.Mname_txtbox.TextMarginLeft = 3
        Me.Mname_txtbox.TextMarginTop = 0
        Me.Mname_txtbox.TextPlaceholder = ""
        Me.Mname_txtbox.UseSystemPasswordChar = False
        Me.Mname_txtbox.WordWrap = True
        '
        'BunifuLabel4
        '
        Me.BunifuLabel4.AllowParentOverrides = False
        Me.BunifuLabel4.AutoEllipsis = False
        Me.BunifuLabel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel4.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel4.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel4.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel4.Location = New System.Drawing.Point(10, 217)
        Me.BunifuLabel4.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel4.Name = "BunifuLabel4"
        Me.BunifuLabel4.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel4.Size = New System.Drawing.Size(80, 31)
        Me.BunifuLabel4.TabIndex = 40
        Me.BunifuLabel4.Text = "Last Name:"
        Me.BunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Lname_txtbox
        '
        Me.Lname_txtbox.AcceptsReturn = False
        Me.Lname_txtbox.AcceptsTab = False
        Me.Lname_txtbox.AnimationSpeed = 200
        Me.Lname_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Lname_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Lname_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Lname_txtbox.BackgroundImage = CType(resources.GetObject("Lname_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Lname_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Lname_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Lname_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Lname_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Lname_txtbox.BorderRadius = 10
        Me.Lname_txtbox.BorderThickness = 1
        Me.Lname_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Lname_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Lname_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lname_txtbox.DefaultText = ""
        Me.Lname_txtbox.FillColor = System.Drawing.Color.White
        Me.Lname_txtbox.HideSelection = True
        Me.Lname_txtbox.IconLeft = Nothing
        Me.Lname_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Lname_txtbox.IconPadding = 10
        Me.Lname_txtbox.IconRight = Nothing
        Me.Lname_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Lname_txtbox.Lines = New String(-1) {}
        Me.Lname_txtbox.Location = New System.Drawing.Point(30, 248)
        Me.Lname_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Lname_txtbox.MaxLength = 32767
        Me.Lname_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Lname_txtbox.Modified = False
        Me.Lname_txtbox.Multiline = False
        Me.Lname_txtbox.Name = "Lname_txtbox"
        StateProperties65.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties65.FillColor = System.Drawing.Color.Empty
        StateProperties65.ForeColor = System.Drawing.Color.Empty
        StateProperties65.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Lname_txtbox.OnActiveState = StateProperties65
        StateProperties66.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties66.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties66.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties66.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Lname_txtbox.OnDisabledState = StateProperties66
        StateProperties67.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties67.FillColor = System.Drawing.Color.Empty
        StateProperties67.ForeColor = System.Drawing.Color.Empty
        StateProperties67.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Lname_txtbox.OnHoverState = StateProperties67
        StateProperties68.BorderColor = System.Drawing.Color.Silver
        StateProperties68.FillColor = System.Drawing.Color.White
        StateProperties68.ForeColor = System.Drawing.Color.Empty
        StateProperties68.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Lname_txtbox.OnIdleState = StateProperties68
        Me.Lname_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Lname_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Lname_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Lname_txtbox.PlaceholderText = ""
        Me.Lname_txtbox.ReadOnly = False
        Me.Lname_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Lname_txtbox.SelectedText = ""
        Me.Lname_txtbox.SelectionLength = 0
        Me.Lname_txtbox.SelectionStart = 0
        Me.Lname_txtbox.ShortcutsEnabled = True
        Me.Lname_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Lname_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Lname_txtbox.TabIndex = 41
        Me.Lname_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Lname_txtbox.TextMarginBottom = 0
        Me.Lname_txtbox.TextMarginLeft = 3
        Me.Lname_txtbox.TextMarginTop = 0
        Me.Lname_txtbox.TextPlaceholder = ""
        Me.Lname_txtbox.UseSystemPasswordChar = False
        Me.Lname_txtbox.WordWrap = True
        '
        'BunifuLabel5
        '
        Me.BunifuLabel5.AllowParentOverrides = False
        Me.BunifuLabel5.AutoEllipsis = False
        Me.BunifuLabel5.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel5.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel5.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel5.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel5.Location = New System.Drawing.Point(10, 313)
        Me.BunifuLabel5.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel5.Name = "BunifuLabel5"
        Me.BunifuLabel5.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel5.Size = New System.Drawing.Size(42, 31)
        Me.BunifuLabel5.TabIndex = 42
        Me.BunifuLabel5.Text = "Email:"
        Me.BunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Email_txtbox
        '
        Me.Email_txtbox.AcceptsReturn = False
        Me.Email_txtbox.AcceptsTab = False
        Me.Email_txtbox.AnimationSpeed = 200
        Me.Email_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Email_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Email_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Email_txtbox.BackgroundImage = CType(resources.GetObject("Email_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Email_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Email_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Email_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Email_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Email_txtbox.BorderRadius = 10
        Me.Email_txtbox.BorderThickness = 1
        Me.Email_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Email_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Email_txtbox.DefaultText = ""
        Me.Email_txtbox.FillColor = System.Drawing.Color.White
        Me.Email_txtbox.HideSelection = True
        Me.Email_txtbox.IconLeft = Nothing
        Me.Email_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.IconPadding = 10
        Me.Email_txtbox.IconRight = Nothing
        Me.Email_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.Lines = New String(-1) {}
        Me.Email_txtbox.Location = New System.Drawing.Point(30, 344)
        Me.Email_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Email_txtbox.MaxLength = 32767
        Me.Email_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Email_txtbox.Modified = False
        Me.Email_txtbox.Multiline = False
        Me.Email_txtbox.Name = "Email_txtbox"
        StateProperties69.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties69.FillColor = System.Drawing.Color.Empty
        StateProperties69.ForeColor = System.Drawing.Color.Empty
        StateProperties69.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnActiveState = StateProperties69
        StateProperties70.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties70.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties70.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties70.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Email_txtbox.OnDisabledState = StateProperties70
        StateProperties71.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties71.FillColor = System.Drawing.Color.Empty
        StateProperties71.ForeColor = System.Drawing.Color.Empty
        StateProperties71.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnHoverState = StateProperties71
        StateProperties72.BorderColor = System.Drawing.Color.Silver
        StateProperties72.FillColor = System.Drawing.Color.White
        StateProperties72.ForeColor = System.Drawing.Color.Empty
        StateProperties72.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnIdleState = StateProperties72
        Me.Email_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Email_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Email_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Email_txtbox.PlaceholderText = ""
        Me.Email_txtbox.ReadOnly = False
        Me.Email_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Email_txtbox.SelectedText = ""
        Me.Email_txtbox.SelectionLength = 0
        Me.Email_txtbox.SelectionStart = 0
        Me.Email_txtbox.ShortcutsEnabled = True
        Me.Email_txtbox.Size = New System.Drawing.Size(242, 40)
        Me.Email_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Email_txtbox.TabIndex = 43
        Me.Email_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Email_txtbox.TextMarginBottom = 0
        Me.Email_txtbox.TextMarginLeft = 3
        Me.Email_txtbox.TextMarginTop = 0
        Me.Email_txtbox.TextPlaceholder = ""
        Me.Email_txtbox.UseSystemPasswordChar = False
        Me.Email_txtbox.WordWrap = True
        '
        'Account_Dialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(646, 549)
        Me.Controls.Add(Me.Program_panel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Account_Dialog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Account  Dialog"
        Me.Program_panel.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Program_panel As Bunifu.UI.WinForms.BunifuShadowPanel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents BunifuLabel10 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
    Friend WithEvents Password_show_chckbox As Bunifu.UI.WinForms.BunifuCheckBox
    Friend WithEvents BunifuLabel9 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Password_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents Repassword_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents AcountType_drpdown As Bunifu.UI.WinForms.BunifuDropdown
    Friend WithEvents BunifuLabel8 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuLabel7 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuLabel6 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents BunifuLabel2 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Fname_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents BunifuLabel3 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Mname_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents BunifuLabel4 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Lname_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents BunifuLabel5 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Email_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents TableLayoutPanel9 As TableLayoutPanel
    Friend WithEvents Account_add_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
End Class
