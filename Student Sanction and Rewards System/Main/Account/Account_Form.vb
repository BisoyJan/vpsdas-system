﻿Imports MySql.Data.MySqlClient
Public Class Account_Form
    Dim adapter As New MySqlDataAdapter
    Dim pagingDS As DataSet
    Dim scrollVal As Integer
    Public row As DataGridViewRow

    Private Sub Account_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Login.Close()
        DisplayUsers()
    End Sub

    Private Sub Account_add_btn_Click(sender As Object, e As EventArgs) Handles Account_add_btn.Click
        Account_Dialog.ShowDialog()
    End Sub

    Private Sub Account_edit_btn_Click(sender As Object, e As EventArgs) Handles Account_edit_btn.Click
        Account_Dialog.Account_add_btn.Text = "Edit"
        Account_Dialog.ShowDialog()
    End Sub

    Private Sub DisplayUsers()
        If opendb() Then
            Dim query As String = "SELECT `id`, `email`, `type`, `first_name`, `middle_name`, `last_name` FROM `users` ORDER BY id DESC"

            adapter = New MySqlDataAdapter(query, conn)
            pagingDS = New DataSet()

            adapter.Fill(pagingDS, scrollVal, 10, "Account_table")

            Try

                Account_datagridview.DataSource = pagingDS
                Account_datagridview.DataMember = "Account_table"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Account_datagridview_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles Account_datagridview.CellClick
        If e.RowIndex >= 0 Then
            row = Me.Account_datagridview.Rows(e.RowIndex)
            Account_Dialog.UserPrimaryKey = row.Cells("id").Value.ToString
            Account_delete_btn.Enabled = True
            Account_edit_btn.Enabled = True
        End If
    End Sub

    Private Sub Account_refresh_btn_Click(sender As Object, e As EventArgs) Handles Account_refresh_btn.Click
        DisplayUsers()
    End Sub

    Private Sub backup_database_btn_Click(sender As Object, e As EventArgs) Handles backup_database_btn.Click
        Dim dbname As String = "vpsdasdata"
        If opendb() Then
            SaveFileDialog1.FileName = DateAndTime.DateString + "_" + dbname
            SaveFileDialog1.Filter = "SQL Server database backup files|*.sql"
            SaveFileDialog1.ShowDialog()
            Dim query As String = "BACKUP DATABASE vpsdasdata TO DISK = '" & SaveFileDialog1.FileName & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Try
                Dim mb As MySqlBackup = New MySqlBackup(cmd)
                mb.ExportToFile(SaveFileDialog1.FileName)

                MessageBox.Show("Succesfuly Back Up Database.",
                "Important Message")

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()

            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub


End Class