﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Account_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Account_Form))
        Dim BorderEdges1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges2 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges3 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim StateProperties1 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties2 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties3 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties4 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim BorderEdges4 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges5 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges6 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim BorderEdges7 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Account_panel = New Bunifu.UI.WinForms.BunifuPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel13 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuLabel2 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel()
        Me.Properties_next_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.Properties_prev_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.TableLayoutPanel12 = New System.Windows.Forms.TableLayoutPanel()
        Me.Account_edit_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.Properties_search_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.Account_add_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.Account_delete_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.Account_refresh_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.backup_database_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel()
        Me.Account_datagridview = New Bunifu.UI.WinForms.BunifuDataGridView()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Account_panel.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel13.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        Me.TableLayoutPanel12.SuspendLayout()
        Me.TableLayoutPanel11.SuspendLayout()
        CType(Me.Account_datagridview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Account_panel
        '
        Me.Account_panel.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_panel.BackgroundImage = CType(resources.GetObject("Account_panel.BackgroundImage"), System.Drawing.Image)
        Me.Account_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Account_panel.BorderColor = System.Drawing.Color.Transparent
        Me.Account_panel.BorderRadius = 3
        Me.Account_panel.BorderThickness = 1
        Me.Account_panel.Controls.Add(Me.TableLayoutPanel1)
        Me.Account_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Account_panel.Location = New System.Drawing.Point(0, 0)
        Me.Account_panel.Name = "Account_panel"
        Me.Account_panel.ShowBorders = True
        Me.Account_panel.Size = New System.Drawing.Size(1252, 774)
        Me.Account_panel.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel13, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel10, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel12, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel11, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1252, 774)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'TableLayoutPanel13
        '
        Me.TableLayoutPanel13.ColumnCount = 2
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel13.Controls.Add(Me.BunifuLabel2, 0, 0)
        Me.TableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel13.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel13.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel13.Name = "TableLayoutPanel13"
        Me.TableLayoutPanel13.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel13.RowCount = 1
        Me.TableLayoutPanel13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel13.Size = New System.Drawing.Size(1252, 48)
        Me.TableLayoutPanel13.TabIndex = 0
        '
        'BunifuLabel2
        '
        Me.BunifuLabel2.AllowParentOverrides = False
        Me.BunifuLabel2.AutoEllipsis = False
        Me.BunifuLabel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.BunifuLabel2.Location = New System.Drawing.Point(8, 8)
        Me.BunifuLabel2.Name = "BunifuLabel2"
        Me.BunifuLabel2.Padding = New System.Windows.Forms.Padding(10)
        Me.BunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel2.Size = New System.Drawing.Size(431, 32)
        Me.BunifuLabel2.TabIndex = 6
        Me.BunifuLabel2.Text = "Only Authorized Personel will only use this page" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.BunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.ColumnCount = 3
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel10.Controls.Add(Me.Properties_next_btn, 1, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.Properties_prev_btn, 0, 0)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(0, 704)
        Me.TableLayoutPanel10.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 1
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(1252, 70)
        Me.TableLayoutPanel10.TabIndex = 3
        '
        'Properties_next_btn
        '
        Me.Properties_next_btn.AllowAnimations = True
        Me.Properties_next_btn.AllowMouseEffects = True
        Me.Properties_next_btn.AllowToggling = False
        Me.Properties_next_btn.AnimationSpeed = 200
        Me.Properties_next_btn.AutoGenerateColors = False
        Me.Properties_next_btn.AutoRoundBorders = False
        Me.Properties_next_btn.AutoSizeLeftIcon = True
        Me.Properties_next_btn.AutoSizeRightIcon = True
        Me.Properties_next_btn.BackColor = System.Drawing.Color.Transparent
        Me.Properties_next_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.BackgroundImage = CType(resources.GetObject("Properties_next_btn.BackgroundImage"), System.Drawing.Image)
        Me.Properties_next_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_next_btn.ButtonText = "Next"
        Me.Properties_next_btn.ButtonTextMarginLeft = 0
        Me.Properties_next_btn.ColorContrastOnClick = 45
        Me.Properties_next_btn.ColorContrastOnHover = 45
        Me.Properties_next_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges1.BottomLeft = True
        BorderEdges1.BottomRight = True
        BorderEdges1.TopLeft = True
        BorderEdges1.TopRight = True
        Me.Properties_next_btn.CustomizableEdges = BorderEdges1
        Me.Properties_next_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Properties_next_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Properties_next_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Properties_next_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Properties_next_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Properties_next_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Properties_next_btn.ForeColor = System.Drawing.Color.White
        Me.Properties_next_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Properties_next_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Properties_next_btn.IconLeftPadding = New System.Windows.Forms.Padding(35, 3, 3, 3)
        Me.Properties_next_btn.IconMarginLeft = 11
        Me.Properties_next_btn.IconPadding = 10
        Me.Properties_next_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Properties_next_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Properties_next_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Properties_next_btn.IconSize = 25
        Me.Properties_next_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.IdleBorderRadius = 30
        Me.Properties_next_btn.IdleBorderThickness = 1
        Me.Properties_next_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.IdleIconLeftImage = CType(resources.GetObject("Properties_next_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Properties_next_btn.IdleIconRightImage = Nothing
        Me.Properties_next_btn.IndicateFocus = True
        Me.Properties_next_btn.Location = New System.Drawing.Point(206, 3)
        Me.Properties_next_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Properties_next_btn.Name = "Properties_next_btn"
        Me.Properties_next_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Properties_next_btn.OnDisabledState.BorderRadius = 30
        Me.Properties_next_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_next_btn.OnDisabledState.BorderThickness = 1
        Me.Properties_next_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Properties_next_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Properties_next_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Properties_next_btn.OnDisabledState.IconRightImage = Nothing
        Me.Properties_next_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.onHoverState.BorderRadius = 30
        Me.Properties_next_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_next_btn.onHoverState.BorderThickness = 1
        Me.Properties_next_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Properties_next_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Properties_next_btn.onHoverState.IconLeftImage = Nothing
        Me.Properties_next_btn.onHoverState.IconRightImage = Nothing
        Me.Properties_next_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.OnIdleState.BorderRadius = 30
        Me.Properties_next_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_next_btn.OnIdleState.BorderThickness = 1
        Me.Properties_next_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Properties_next_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Properties_next_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Properties_next_btn.OnIdleState.IconRightImage = Nothing
        Me.Properties_next_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_next_btn.OnPressedState.BorderRadius = 30
        Me.Properties_next_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_next_btn.OnPressedState.BorderThickness = 1
        Me.Properties_next_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Properties_next_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Properties_next_btn.OnPressedState.IconLeftImage = Nothing
        Me.Properties_next_btn.OnPressedState.IconRightImage = Nothing
        Me.Properties_next_btn.Size = New System.Drawing.Size(151, 49)
        Me.Properties_next_btn.TabIndex = 12
        Me.Properties_next_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Properties_next_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Properties_next_btn.TextMarginLeft = 0
        Me.Properties_next_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Properties_next_btn.UseDefaultRadiusAndThickness = True
        '
        'Properties_prev_btn
        '
        Me.Properties_prev_btn.AllowAnimations = True
        Me.Properties_prev_btn.AllowMouseEffects = True
        Me.Properties_prev_btn.AllowToggling = False
        Me.Properties_prev_btn.AnimationSpeed = 200
        Me.Properties_prev_btn.AutoGenerateColors = False
        Me.Properties_prev_btn.AutoRoundBorders = False
        Me.Properties_prev_btn.AutoSizeLeftIcon = True
        Me.Properties_prev_btn.AutoSizeRightIcon = True
        Me.Properties_prev_btn.BackColor = System.Drawing.Color.Transparent
        Me.Properties_prev_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.BackgroundImage = CType(resources.GetObject("Properties_prev_btn.BackgroundImage"), System.Drawing.Image)
        Me.Properties_prev_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_prev_btn.ButtonText = "Prev"
        Me.Properties_prev_btn.ButtonTextMarginLeft = 0
        Me.Properties_prev_btn.ColorContrastOnClick = 45
        Me.Properties_prev_btn.ColorContrastOnHover = 45
        Me.Properties_prev_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges2.BottomLeft = True
        BorderEdges2.BottomRight = True
        BorderEdges2.TopLeft = True
        BorderEdges2.TopRight = True
        Me.Properties_prev_btn.CustomizableEdges = BorderEdges2
        Me.Properties_prev_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Properties_prev_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Properties_prev_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Properties_prev_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Properties_prev_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Properties_prev_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Properties_prev_btn.ForeColor = System.Drawing.Color.White
        Me.Properties_prev_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Properties_prev_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Properties_prev_btn.IconLeftPadding = New System.Windows.Forms.Padding(35, 3, 3, 3)
        Me.Properties_prev_btn.IconMarginLeft = 11
        Me.Properties_prev_btn.IconPadding = 10
        Me.Properties_prev_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Properties_prev_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Properties_prev_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Properties_prev_btn.IconSize = 25
        Me.Properties_prev_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.IdleBorderRadius = 30
        Me.Properties_prev_btn.IdleBorderThickness = 1
        Me.Properties_prev_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.IdleIconLeftImage = CType(resources.GetObject("Properties_prev_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Properties_prev_btn.IdleIconRightImage = Nothing
        Me.Properties_prev_btn.IndicateFocus = True
        Me.Properties_prev_btn.Location = New System.Drawing.Point(35, 3)
        Me.Properties_prev_btn.Margin = New System.Windows.Forms.Padding(35, 3, 10, 3)
        Me.Properties_prev_btn.Name = "Properties_prev_btn"
        Me.Properties_prev_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Properties_prev_btn.OnDisabledState.BorderRadius = 30
        Me.Properties_prev_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_prev_btn.OnDisabledState.BorderThickness = 1
        Me.Properties_prev_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Properties_prev_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Properties_prev_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Properties_prev_btn.OnDisabledState.IconRightImage = Nothing
        Me.Properties_prev_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.onHoverState.BorderRadius = 30
        Me.Properties_prev_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_prev_btn.onHoverState.BorderThickness = 1
        Me.Properties_prev_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Properties_prev_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Properties_prev_btn.onHoverState.IconLeftImage = Nothing
        Me.Properties_prev_btn.onHoverState.IconRightImage = Nothing
        Me.Properties_prev_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.OnIdleState.BorderRadius = 30
        Me.Properties_prev_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_prev_btn.OnIdleState.BorderThickness = 1
        Me.Properties_prev_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Properties_prev_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Properties_prev_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Properties_prev_btn.OnIdleState.IconRightImage = Nothing
        Me.Properties_prev_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Properties_prev_btn.OnPressedState.BorderRadius = 30
        Me.Properties_prev_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Properties_prev_btn.OnPressedState.BorderThickness = 1
        Me.Properties_prev_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Properties_prev_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Properties_prev_btn.OnPressedState.IconLeftImage = Nothing
        Me.Properties_prev_btn.OnPressedState.IconRightImage = Nothing
        Me.Properties_prev_btn.Size = New System.Drawing.Size(151, 49)
        Me.Properties_prev_btn.TabIndex = 11
        Me.Properties_prev_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Properties_prev_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Properties_prev_btn.TextMarginLeft = 0
        Me.Properties_prev_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Properties_prev_btn.UseDefaultRadiusAndThickness = True
        '
        'TableLayoutPanel12
        '
        Me.TableLayoutPanel12.ColumnCount = 7
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.594594!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.4054!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel12.Controls.Add(Me.Account_edit_btn, 5, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Properties_search_txtbox, 0, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Account_add_btn, 4, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Account_delete_btn, 6, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Account_refresh_btn, 2, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.backup_database_btn, 3, 0)
        Me.TableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel12.Location = New System.Drawing.Point(0, 48)
        Me.TableLayoutPanel12.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel12.Name = "TableLayoutPanel12"
        Me.TableLayoutPanel12.RowCount = 1
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel12.Size = New System.Drawing.Size(1252, 58)
        Me.TableLayoutPanel12.TabIndex = 1
        '
        'Account_edit_btn
        '
        Me.Account_edit_btn.AllowAnimations = True
        Me.Account_edit_btn.AllowMouseEffects = True
        Me.Account_edit_btn.AllowToggling = False
        Me.Account_edit_btn.AnimationSpeed = 200
        Me.Account_edit_btn.AutoGenerateColors = False
        Me.Account_edit_btn.AutoRoundBorders = False
        Me.Account_edit_btn.AutoSizeLeftIcon = True
        Me.Account_edit_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Account_edit_btn.AutoSizeRightIcon = True
        Me.Account_edit_btn.BackColor = System.Drawing.Color.Transparent
        Me.Account_edit_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.BackgroundImage = CType(resources.GetObject("Account_edit_btn.BackgroundImage"), System.Drawing.Image)
        Me.Account_edit_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_edit_btn.ButtonText = "Edit"
        Me.Account_edit_btn.ButtonTextMarginLeft = 0
        Me.Account_edit_btn.ColorContrastOnClick = 45
        Me.Account_edit_btn.ColorContrastOnHover = 45
        Me.Account_edit_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges3.BottomLeft = True
        BorderEdges3.BottomRight = True
        BorderEdges3.TopLeft = True
        BorderEdges3.TopRight = True
        Me.Account_edit_btn.CustomizableEdges = BorderEdges3
        Me.Account_edit_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Account_edit_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_edit_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_edit_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_edit_btn.Enabled = False
        Me.Account_edit_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Account_edit_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_edit_btn.ForeColor = System.Drawing.Color.White
        Me.Account_edit_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Account_edit_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Account_edit_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.Account_edit_btn.IconMarginLeft = 11
        Me.Account_edit_btn.IconPadding = 10
        Me.Account_edit_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Account_edit_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Account_edit_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Account_edit_btn.IconSize = 25
        Me.Account_edit_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.IdleBorderRadius = 30
        Me.Account_edit_btn.IdleBorderThickness = 1
        Me.Account_edit_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.IdleIconLeftImage = CType(resources.GetObject("Account_edit_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Account_edit_btn.IdleIconRightImage = Nothing
        Me.Account_edit_btn.IndicateFocus = True
        Me.Account_edit_btn.Location = New System.Drawing.Point(942, 3)
        Me.Account_edit_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Account_edit_btn.Name = "Account_edit_btn"
        Me.Account_edit_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_edit_btn.OnDisabledState.BorderRadius = 30
        Me.Account_edit_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_edit_btn.OnDisabledState.BorderThickness = 1
        Me.Account_edit_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_edit_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_edit_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Account_edit_btn.OnDisabledState.IconRightImage = Nothing
        Me.Account_edit_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.onHoverState.BorderRadius = 30
        Me.Account_edit_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_edit_btn.onHoverState.BorderThickness = 1
        Me.Account_edit_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_edit_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Account_edit_btn.onHoverState.IconLeftImage = Nothing
        Me.Account_edit_btn.onHoverState.IconRightImage = Nothing
        Me.Account_edit_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.OnIdleState.BorderRadius = 30
        Me.Account_edit_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_edit_btn.OnIdleState.BorderThickness = 1
        Me.Account_edit_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Account_edit_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Account_edit_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Account_edit_btn.OnIdleState.IconRightImage = Nothing
        Me.Account_edit_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_edit_btn.OnPressedState.BorderRadius = 30
        Me.Account_edit_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_edit_btn.OnPressedState.BorderThickness = 1
        Me.Account_edit_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_edit_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Account_edit_btn.OnPressedState.IconLeftImage = Nothing
        Me.Account_edit_btn.OnPressedState.IconRightImage = Nothing
        Me.Account_edit_btn.Size = New System.Drawing.Size(127, 52)
        Me.Account_edit_btn.TabIndex = 7
        Me.Account_edit_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Account_edit_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Account_edit_btn.TextMarginLeft = 0
        Me.Account_edit_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Account_edit_btn.UseDefaultRadiusAndThickness = True
        '
        'Properties_search_txtbox
        '
        Me.Properties_search_txtbox.AcceptsReturn = False
        Me.Properties_search_txtbox.AcceptsTab = False
        Me.Properties_search_txtbox.AnimationSpeed = 200
        Me.Properties_search_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Properties_search_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Properties_search_txtbox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Properties_search_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Properties_search_txtbox.BackgroundImage = CType(resources.GetObject("Properties_search_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Properties_search_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Properties_search_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Properties_search_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Properties_search_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Properties_search_txtbox.BorderRadius = 10
        Me.Properties_search_txtbox.BorderThickness = 1
        Me.Properties_search_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Properties_search_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Properties_search_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Properties_search_txtbox.DefaultText = ""
        Me.Properties_search_txtbox.FillColor = System.Drawing.Color.White
        Me.Properties_search_txtbox.HideSelection = True
        Me.Properties_search_txtbox.IconLeft = Nothing
        Me.Properties_search_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Properties_search_txtbox.IconPadding = 10
        Me.Properties_search_txtbox.IconRight = Nothing
        Me.Properties_search_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Properties_search_txtbox.Lines = New String(-1) {}
        Me.Properties_search_txtbox.Location = New System.Drawing.Point(35, 3)
        Me.Properties_search_txtbox.Margin = New System.Windows.Forms.Padding(35, 3, 10, 3)
        Me.Properties_search_txtbox.MaxLength = 32767
        Me.Properties_search_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Properties_search_txtbox.Modified = False
        Me.Properties_search_txtbox.Multiline = False
        Me.Properties_search_txtbox.Name = "Properties_search_txtbox"
        StateProperties1.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties1.FillColor = System.Drawing.Color.Empty
        StateProperties1.ForeColor = System.Drawing.Color.Empty
        StateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Properties_search_txtbox.OnActiveState = StateProperties1
        StateProperties2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties2.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties2.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Properties_search_txtbox.OnDisabledState = StateProperties2
        StateProperties3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties3.FillColor = System.Drawing.Color.Empty
        StateProperties3.ForeColor = System.Drawing.Color.Empty
        StateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Properties_search_txtbox.OnHoverState = StateProperties3
        StateProperties4.BorderColor = System.Drawing.Color.Silver
        StateProperties4.FillColor = System.Drawing.Color.White
        StateProperties4.ForeColor = System.Drawing.Color.Empty
        StateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Properties_search_txtbox.OnIdleState = StateProperties4
        Me.Properties_search_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Properties_search_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Properties_search_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Properties_search_txtbox.PlaceholderText = "Search"
        Me.Properties_search_txtbox.ReadOnly = False
        Me.Properties_search_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Properties_search_txtbox.SelectedText = ""
        Me.Properties_search_txtbox.SelectionLength = 0
        Me.Properties_search_txtbox.SelectionStart = 0
        Me.Properties_search_txtbox.ShortcutsEnabled = True
        Me.Properties_search_txtbox.Size = New System.Drawing.Size(320, 52)
        Me.Properties_search_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Properties_search_txtbox.TabIndex = 2
        Me.Properties_search_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Properties_search_txtbox.TextMarginBottom = 0
        Me.Properties_search_txtbox.TextMarginLeft = 3
        Me.Properties_search_txtbox.TextMarginTop = 0
        Me.Properties_search_txtbox.TextPlaceholder = "Search"
        Me.Properties_search_txtbox.UseSystemPasswordChar = False
        Me.Properties_search_txtbox.WordWrap = True
        '
        'Account_add_btn
        '
        Me.Account_add_btn.AllowAnimations = True
        Me.Account_add_btn.AllowMouseEffects = True
        Me.Account_add_btn.AllowToggling = False
        Me.Account_add_btn.AnimationSpeed = 200
        Me.Account_add_btn.AutoGenerateColors = False
        Me.Account_add_btn.AutoRoundBorders = False
        Me.Account_add_btn.AutoSizeLeftIcon = True
        Me.Account_add_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Account_add_btn.AutoSizeRightIcon = True
        Me.Account_add_btn.BackColor = System.Drawing.Color.Transparent
        Me.Account_add_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.BackgroundImage = CType(resources.GetObject("Account_add_btn.BackgroundImage"), System.Drawing.Image)
        Me.Account_add_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.ButtonText = "Add"
        Me.Account_add_btn.ButtonTextMarginLeft = 0
        Me.Account_add_btn.ColorContrastOnClick = 45
        Me.Account_add_btn.ColorContrastOnHover = 45
        Me.Account_add_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges4.BottomLeft = True
        BorderEdges4.BottomRight = True
        BorderEdges4.TopLeft = True
        BorderEdges4.TopRight = True
        Me.Account_add_btn.CustomizableEdges = BorderEdges4
        Me.Account_add_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Account_add_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_add_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_add_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_add_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Account_add_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_add_btn.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Account_add_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Account_add_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.Account_add_btn.IconMarginLeft = 11
        Me.Account_add_btn.IconPadding = 10
        Me.Account_add_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Account_add_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Account_add_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Account_add_btn.IconSize = 25
        Me.Account_add_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.IdleBorderRadius = 30
        Me.Account_add_btn.IdleBorderThickness = 1
        Me.Account_add_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.IdleIconLeftImage = CType(resources.GetObject("Account_add_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Account_add_btn.IdleIconRightImage = Nothing
        Me.Account_add_btn.IndicateFocus = True
        Me.Account_add_btn.Location = New System.Drawing.Point(795, 3)
        Me.Account_add_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Account_add_btn.Name = "Account_add_btn"
        Me.Account_add_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.BorderRadius = 30
        Me.Account_add_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnDisabledState.BorderThickness = 1
        Me.Account_add_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_add_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Account_add_btn.OnDisabledState.IconRightImage = Nothing
        Me.Account_add_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.onHoverState.BorderRadius = 30
        Me.Account_add_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.onHoverState.BorderThickness = 1
        Me.Account_add_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.onHoverState.IconLeftImage = Nothing
        Me.Account_add_btn.onHoverState.IconRightImage = Nothing
        Me.Account_add_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.OnIdleState.BorderRadius = 30
        Me.Account_add_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnIdleState.BorderThickness = 1
        Me.Account_add_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Account_add_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Account_add_btn.OnIdleState.IconRightImage = Nothing
        Me.Account_add_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_add_btn.OnPressedState.BorderRadius = 30
        Me.Account_add_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_add_btn.OnPressedState.BorderThickness = 1
        Me.Account_add_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_add_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Account_add_btn.OnPressedState.IconLeftImage = Nothing
        Me.Account_add_btn.OnPressedState.IconRightImage = Nothing
        Me.Account_add_btn.Size = New System.Drawing.Size(127, 52)
        Me.Account_add_btn.TabIndex = 14
        Me.Account_add_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Account_add_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Account_add_btn.TextMarginLeft = 0
        Me.Account_add_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Account_add_btn.UseDefaultRadiusAndThickness = True
        '
        'Account_delete_btn
        '
        Me.Account_delete_btn.AllowAnimations = True
        Me.Account_delete_btn.AllowMouseEffects = True
        Me.Account_delete_btn.AllowToggling = False
        Me.Account_delete_btn.AnimationSpeed = 200
        Me.Account_delete_btn.AutoGenerateColors = False
        Me.Account_delete_btn.AutoRoundBorders = False
        Me.Account_delete_btn.AutoSizeLeftIcon = True
        Me.Account_delete_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Account_delete_btn.AutoSizeRightIcon = True
        Me.Account_delete_btn.BackColor = System.Drawing.Color.Transparent
        Me.Account_delete_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.BackgroundImage = CType(resources.GetObject("Account_delete_btn.BackgroundImage"), System.Drawing.Image)
        Me.Account_delete_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_delete_btn.ButtonText = "Delete"
        Me.Account_delete_btn.ButtonTextMarginLeft = 0
        Me.Account_delete_btn.ColorContrastOnClick = 45
        Me.Account_delete_btn.ColorContrastOnHover = 45
        Me.Account_delete_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges5.BottomLeft = True
        BorderEdges5.BottomRight = True
        BorderEdges5.TopLeft = True
        BorderEdges5.TopRight = True
        Me.Account_delete_btn.CustomizableEdges = BorderEdges5
        Me.Account_delete_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Account_delete_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_delete_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_delete_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_delete_btn.Enabled = False
        Me.Account_delete_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Account_delete_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_delete_btn.ForeColor = System.Drawing.Color.White
        Me.Account_delete_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Account_delete_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Account_delete_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.Account_delete_btn.IconMarginLeft = 11
        Me.Account_delete_btn.IconPadding = 10
        Me.Account_delete_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Account_delete_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Account_delete_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Account_delete_btn.IconSize = 25
        Me.Account_delete_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.IdleBorderRadius = 30
        Me.Account_delete_btn.IdleBorderThickness = 1
        Me.Account_delete_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.IdleIconLeftImage = CType(resources.GetObject("Account_delete_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Account_delete_btn.IdleIconRightImage = Nothing
        Me.Account_delete_btn.IndicateFocus = True
        Me.Account_delete_btn.Location = New System.Drawing.Point(1089, 3)
        Me.Account_delete_btn.Margin = New System.Windows.Forms.Padding(10, 3, 35, 3)
        Me.Account_delete_btn.Name = "Account_delete_btn"
        Me.Account_delete_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_delete_btn.OnDisabledState.BorderRadius = 30
        Me.Account_delete_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_delete_btn.OnDisabledState.BorderThickness = 1
        Me.Account_delete_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_delete_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_delete_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Account_delete_btn.OnDisabledState.IconRightImage = Nothing
        Me.Account_delete_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.onHoverState.BorderRadius = 30
        Me.Account_delete_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_delete_btn.onHoverState.BorderThickness = 1
        Me.Account_delete_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_delete_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Account_delete_btn.onHoverState.IconLeftImage = Nothing
        Me.Account_delete_btn.onHoverState.IconRightImage = Nothing
        Me.Account_delete_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.OnIdleState.BorderRadius = 30
        Me.Account_delete_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_delete_btn.OnIdleState.BorderThickness = 1
        Me.Account_delete_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Account_delete_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Account_delete_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Account_delete_btn.OnIdleState.IconRightImage = Nothing
        Me.Account_delete_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_delete_btn.OnPressedState.BorderRadius = 30
        Me.Account_delete_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_delete_btn.OnPressedState.BorderThickness = 1
        Me.Account_delete_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_delete_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Account_delete_btn.OnPressedState.IconLeftImage = Nothing
        Me.Account_delete_btn.OnPressedState.IconRightImage = Nothing
        Me.Account_delete_btn.Size = New System.Drawing.Size(127, 52)
        Me.Account_delete_btn.TabIndex = 15
        Me.Account_delete_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Account_delete_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Account_delete_btn.TextMarginLeft = 0
        Me.Account_delete_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Account_delete_btn.UseDefaultRadiusAndThickness = True
        '
        'Account_refresh_btn
        '
        Me.Account_refresh_btn.AllowAnimations = True
        Me.Account_refresh_btn.AllowMouseEffects = True
        Me.Account_refresh_btn.AllowToggling = False
        Me.Account_refresh_btn.AnimationSpeed = 200
        Me.Account_refresh_btn.AutoGenerateColors = False
        Me.Account_refresh_btn.AutoRoundBorders = False
        Me.Account_refresh_btn.AutoSizeLeftIcon = True
        Me.Account_refresh_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Account_refresh_btn.AutoSizeRightIcon = True
        Me.Account_refresh_btn.BackColor = System.Drawing.Color.Transparent
        Me.Account_refresh_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.BackgroundImage = CType(resources.GetObject("Account_refresh_btn.BackgroundImage"), System.Drawing.Image)
        Me.Account_refresh_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_refresh_btn.ButtonText = "Refresh"
        Me.Account_refresh_btn.ButtonTextMarginLeft = 0
        Me.Account_refresh_btn.ColorContrastOnClick = 45
        Me.Account_refresh_btn.ColorContrastOnHover = 45
        Me.Account_refresh_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges6.BottomLeft = True
        BorderEdges6.BottomRight = True
        BorderEdges6.TopLeft = True
        BorderEdges6.TopRight = True
        Me.Account_refresh_btn.CustomizableEdges = BorderEdges6
        Me.Account_refresh_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Account_refresh_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_refresh_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_refresh_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_refresh_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Account_refresh_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_refresh_btn.ForeColor = System.Drawing.Color.White
        Me.Account_refresh_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Account_refresh_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Account_refresh_btn.IconLeftPadding = New System.Windows.Forms.Padding(30, 3, 3, 3)
        Me.Account_refresh_btn.IconMarginLeft = 11
        Me.Account_refresh_btn.IconPadding = 10
        Me.Account_refresh_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Account_refresh_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Account_refresh_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Account_refresh_btn.IconSize = 25
        Me.Account_refresh_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.IdleBorderRadius = 30
        Me.Account_refresh_btn.IdleBorderThickness = 1
        Me.Account_refresh_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.IdleIconLeftImage = CType(resources.GetObject("Account_refresh_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Account_refresh_btn.IdleIconRightImage = Nothing
        Me.Account_refresh_btn.IndicateFocus = True
        Me.Account_refresh_btn.Location = New System.Drawing.Point(386, 3)
        Me.Account_refresh_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.Account_refresh_btn.Name = "Account_refresh_btn"
        Me.Account_refresh_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Account_refresh_btn.OnDisabledState.BorderRadius = 30
        Me.Account_refresh_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_refresh_btn.OnDisabledState.BorderThickness = 1
        Me.Account_refresh_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_refresh_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Account_refresh_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Account_refresh_btn.OnDisabledState.IconRightImage = Nothing
        Me.Account_refresh_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.onHoverState.BorderRadius = 30
        Me.Account_refresh_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_refresh_btn.onHoverState.BorderThickness = 1
        Me.Account_refresh_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_refresh_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Account_refresh_btn.onHoverState.IconLeftImage = Nothing
        Me.Account_refresh_btn.onHoverState.IconRightImage = Nothing
        Me.Account_refresh_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.OnIdleState.BorderRadius = 30
        Me.Account_refresh_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_refresh_btn.OnIdleState.BorderThickness = 1
        Me.Account_refresh_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Account_refresh_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Account_refresh_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Account_refresh_btn.OnIdleState.IconRightImage = Nothing
        Me.Account_refresh_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Account_refresh_btn.OnPressedState.BorderRadius = 30
        Me.Account_refresh_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Account_refresh_btn.OnPressedState.BorderThickness = 1
        Me.Account_refresh_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Account_refresh_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Account_refresh_btn.OnPressedState.IconLeftImage = Nothing
        Me.Account_refresh_btn.OnPressedState.IconRightImage = Nothing
        Me.Account_refresh_btn.Size = New System.Drawing.Size(167, 52)
        Me.Account_refresh_btn.TabIndex = 9
        Me.Account_refresh_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Account_refresh_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Account_refresh_btn.TextMarginLeft = 0
        Me.Account_refresh_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Account_refresh_btn.UseDefaultRadiusAndThickness = True
        '
        'backup_database_btn
        '
        Me.backup_database_btn.AllowAnimations = True
        Me.backup_database_btn.AllowMouseEffects = True
        Me.backup_database_btn.AllowToggling = False
        Me.backup_database_btn.AnimationSpeed = 200
        Me.backup_database_btn.AutoGenerateColors = False
        Me.backup_database_btn.AutoRoundBorders = False
        Me.backup_database_btn.AutoSizeLeftIcon = True
        Me.backup_database_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.backup_database_btn.AutoSizeRightIcon = True
        Me.backup_database_btn.BackColor = System.Drawing.Color.Transparent
        Me.backup_database_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.BackgroundImage = CType(resources.GetObject("backup_database_btn.BackgroundImage"), System.Drawing.Image)
        Me.backup_database_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.backup_database_btn.ButtonText = "Backup"
        Me.backup_database_btn.ButtonTextMarginLeft = 0
        Me.backup_database_btn.ColorContrastOnClick = 45
        Me.backup_database_btn.ColorContrastOnHover = 45
        Me.backup_database_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges7.BottomLeft = True
        BorderEdges7.BottomRight = True
        BorderEdges7.TopLeft = True
        BorderEdges7.TopRight = True
        Me.backup_database_btn.CustomizableEdges = BorderEdges7
        Me.backup_database_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.backup_database_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.backup_database_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.backup_database_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.backup_database_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.backup_database_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.backup_database_btn.ForeColor = System.Drawing.Color.White
        Me.backup_database_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.backup_database_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.backup_database_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.backup_database_btn.IconMarginLeft = 11
        Me.backup_database_btn.IconPadding = 10
        Me.backup_database_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.backup_database_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.backup_database_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.backup_database_btn.IconSize = 25
        Me.backup_database_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.IdleBorderRadius = 30
        Me.backup_database_btn.IdleBorderThickness = 1
        Me.backup_database_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.IdleIconLeftImage = CType(resources.GetObject("backup_database_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.backup_database_btn.IdleIconRightImage = Nothing
        Me.backup_database_btn.IndicateFocus = True
        Me.backup_database_btn.Location = New System.Drawing.Point(635, 3)
        Me.backup_database_btn.Margin = New System.Windows.Forms.Padding(10, 3, 10, 3)
        Me.backup_database_btn.Name = "backup_database_btn"
        Me.backup_database_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.backup_database_btn.OnDisabledState.BorderRadius = 30
        Me.backup_database_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.backup_database_btn.OnDisabledState.BorderThickness = 1
        Me.backup_database_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.backup_database_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.backup_database_btn.OnDisabledState.IconLeftImage = Nothing
        Me.backup_database_btn.OnDisabledState.IconRightImage = Nothing
        Me.backup_database_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.onHoverState.BorderRadius = 30
        Me.backup_database_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.backup_database_btn.onHoverState.BorderThickness = 1
        Me.backup_database_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.backup_database_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.backup_database_btn.onHoverState.IconLeftImage = Nothing
        Me.backup_database_btn.onHoverState.IconRightImage = Nothing
        Me.backup_database_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.OnIdleState.BorderRadius = 30
        Me.backup_database_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.backup_database_btn.OnIdleState.BorderThickness = 1
        Me.backup_database_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.backup_database_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("backup_database_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.backup_database_btn.OnIdleState.IconRightImage = Nothing
        Me.backup_database_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.backup_database_btn.OnPressedState.BorderRadius = 30
        Me.backup_database_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.backup_database_btn.OnPressedState.BorderThickness = 1
        Me.backup_database_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.backup_database_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.backup_database_btn.OnPressedState.IconLeftImage = Nothing
        Me.backup_database_btn.OnPressedState.IconRightImage = Nothing
        Me.backup_database_btn.Size = New System.Drawing.Size(140, 52)
        Me.backup_database_btn.TabIndex = 16
        Me.backup_database_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.backup_database_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.backup_database_btn.TextMarginLeft = 0
        Me.backup_database_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.backup_database_btn.UseDefaultRadiusAndThickness = True
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.ColumnCount = 1
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel11.Controls.Add(Me.Account_datagridview, 0, 0)
        Me.TableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(3, 109)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 1
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(1246, 592)
        Me.TableLayoutPanel11.TabIndex = 2
        '
        'Account_datagridview
        '
        Me.Account_datagridview.AllowCustomTheming = True
        Me.Account_datagridview.AllowUserToAddRows = False
        Me.Account_datagridview.AllowUserToDeleteRows = False
        Me.Account_datagridview.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        Me.Account_datagridview.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Account_datagridview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Account_datagridview.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Account_datagridview.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Account_datagridview.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.Account_datagridview.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(204, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Account_datagridview.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Account_datagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Account_datagridview.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Account_datagridview.CurrentTheme.AlternatingRowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Account_datagridview.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black
        Me.Account_datagridview.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Account_datagridview.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black
        Me.Account_datagridview.CurrentTheme.BackColor = System.Drawing.Color.White
        Me.Account_datagridview.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Account_datagridview.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue
        Me.Account_datagridview.CurrentTheme.HeaderStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 11.75!, System.Drawing.FontStyle.Bold)
        Me.Account_datagridview.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.Account_datagridview.CurrentTheme.HeaderStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Account_datagridview.CurrentTheme.HeaderStyle.SelectionForeColor = System.Drawing.Color.White
        Me.Account_datagridview.CurrentTheme.Name = Nothing
        Me.Account_datagridview.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White
        Me.Account_datagridview.CurrentTheme.RowsStyle.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Account_datagridview.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black
        Me.Account_datagridview.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Account_datagridview.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Account_datagridview.DefaultCellStyle = DataGridViewCellStyle3
        Me.Account_datagridview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Account_datagridview.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.Account_datagridview.EnableHeadersVisualStyles = False
        Me.Account_datagridview.GridColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Account_datagridview.HeaderBackColor = System.Drawing.Color.DodgerBlue
        Me.Account_datagridview.HeaderBgColor = System.Drawing.Color.Empty
        Me.Account_datagridview.HeaderForeColor = System.Drawing.Color.White
        Me.Account_datagridview.Location = New System.Drawing.Point(10, 20)
        Me.Account_datagridview.Margin = New System.Windows.Forms.Padding(10, 20, 10, 20)
        Me.Account_datagridview.Name = "Account_datagridview"
        Me.Account_datagridview.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Account_datagridview.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.Account_datagridview.RowHeadersVisible = False
        Me.Account_datagridview.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Account_datagridview.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.Account_datagridview.RowTemplate.Height = 40
        Me.Account_datagridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Account_datagridview.Size = New System.Drawing.Size(1226, 552)
        Me.Account_datagridview.TabIndex = 1
        Me.Account_datagridview.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.DodgerBlue
        '
        'Account_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1252, 774)
        Me.Controls.Add(Me.Account_panel)
        Me.Name = "Account_Form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Account_form"
        Me.Account_panel.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel13.ResumeLayout(False)
        Me.TableLayoutPanel13.PerformLayout()
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TableLayoutPanel12.ResumeLayout(False)
        Me.TableLayoutPanel11.ResumeLayout(False)
        CType(Me.Account_datagridview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Account_panel As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel13 As TableLayoutPanel
    Friend WithEvents BunifuLabel2 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents TableLayoutPanel10 As TableLayoutPanel
    Friend WithEvents Properties_next_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Properties_prev_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents TableLayoutPanel12 As TableLayoutPanel
    Friend WithEvents Account_edit_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Properties_search_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents Account_add_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Account_delete_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Account_refresh_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents TableLayoutPanel11 As TableLayoutPanel
    Friend WithEvents Account_datagridview As Bunifu.UI.WinForms.BunifuDataGridView
    Friend WithEvents backup_database_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
