﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions

Public Class Account_Dialog
    Dim encrypted As String
    Dim decrypted As String
    Public UserPrimaryKey As String

    Private Sub Account_Dialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UserPrimaryKey = "0" Then
            Account_add_btn.Text = "Save"
        Else
            Edit_users()
        End If
    End Sub

    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

    Protected Sub Decrypt(sender As Object, e As EventArgs)
        'decrypted = Me.decrypt(txtEncryptedText.Text.Trim())
    End Sub

    Private Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D,
         &H65, &H64, &H76, &H65, &H64, &H65,
         &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function

    Private Sub Account_add_btn_Click(sender As Object, e As EventArgs) Handles Account_add_btn.Click
        Dim regex As Regex = New Regex("^[^@\s]+@[^@\s]+\.[^@\s]+$")
        Dim isvalid As Boolean = regex.IsMatch(Email_txtbox.Text.Trim)
        encrypted = Me.Encrypt(Repassword_txtbox.Text.Trim())

        If String.IsNullOrEmpty(Repassword_txtbox.Text) Or String.IsNullOrEmpty(Email_txtbox.Text) Or String.IsNullOrEmpty(AcountType_drpdown.Text) Or String.IsNullOrEmpty(Fname_txtbox.Text) Or String.IsNullOrEmpty(Mname_txtbox.Text) Or String.IsNullOrEmpty(Lname_txtbox.Text) Then
            MsgBox("All inputs must be filled!",
               MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
               "Input Fields")
        ElseIf Not isvalid Then

            MsgBox("Please enter a Valid Email address!",
               MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
               "Email Format")
        Else
            If Password_txtbox.Text = Repassword_txtbox.Text Then
                If Account_add_btn.Text = "Save" Then
                    If opendb() Then
                        Dim query As String = "INSERT INTO `users`(`email`, `password`, `type`, `first_name`, `middle_name`, `last_name`) VALUES ('" & Email_txtbox.Text & "','" & encrypted & "', '" & AcountType_drpdown.Text & "','" & Fname_txtbox.Text & "','" & Mname_txtbox.Text & "', '" & Lname_txtbox.Text & "')"
                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader
                            MessageBox.Show("Succesfuly Added.",
                            "Important Message")
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                            Me.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                ElseIf Account_add_btn.Text = "Edit" Then
                    If opendb() Then
                        Dim query As String = "UPDATE `users` SET `email`='" & Email_txtbox.Text & "',`password`='" & encrypted & "',`type`='" & AcountType_drpdown.Text & "',`first_name`='" & Fname_txtbox.Text & "',`middle_name`='" & Mname_txtbox.Text & "',`last_name`='" & Lname_txtbox.Text & "' WHERE `id` = '" & UserPrimaryKey & "'"
                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader
                            MessageBox.Show("Succesfuly Edited.",
                            "Important Message")
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                            Me.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If
            Else
                MsgBox("Password is not Match",
                   MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
                   "Password")
            End If
        End If
    End Sub

    Private Sub Edit_users()
        If opendb() Then
            Dim query As String = "SELECT `id`, `email`, `password`, `type`, `first_name`, `middle_name`, `last_name` FROM `users` WHERE id = '" & UserPrimaryKey & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader
            Dim table As New DataTable()

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    Fname_txtbox.Text = dtreader.GetString("first_name")
                    Mname_txtbox.Text = dtreader.GetString("middle_name")
                    Lname_txtbox.Text = dtreader.GetString("last_name")
                    Email_txtbox.Text = dtreader.GetString("email")
                    Password_txtbox.Text = Me.Decrypt(dtreader.GetString("password"))
                    AcountType_drpdown.Text = dtreader.GetString("type")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Password_show_chckbox_CheckedChanged(sender As Object, e As Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs) Handles Password_show_chckbox.CheckedChanged
        If Password_show_chckbox.Checked Then
            Password_txtbox.PasswordChar = ""
            Repassword_txtbox.PasswordChar = ""
        Else
            Password_txtbox.PasswordChar = "*"
            Repassword_txtbox.PasswordChar = "*"
        End If
    End Sub

    Private Sub Password_txtbox_Leave(sender As Object, e As EventArgs) Handles Password_txtbox.Leave
        If Password_txtbox.Text.Length >= 7 Then

        Else
            MsgBox("The password must be more than 7 Characters!",
               MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
               "Password")
        End If
    End Sub

    Private Sub Repassword_txtbox_Leave(sender As Object, e As EventArgs) Handles Repassword_txtbox.Leave
        If Repassword_txtbox.Text.Length >= 7 Then

        Else
            MsgBox("The password must be more than 7 Characters!",
               MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation,
               "Password")
        End If
    End Sub

    Private Sub Account_Dialog_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Dispose()
    End Sub
End Class