﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word

Public Class kindly_act_form

    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Public PrimaryKey_kindly As String

    Dim id_student As String
    Dim dateissued As String

    Dim kindly As String
    Dim kindlyPath As String
    Dim kindlySave As String
    Dim fullpath As String
    Dim DatabasePath As String

    Dim ReturnedItem As String
    Dim DatePresented As String
    Dim StudentName As String

    Private Sub formclose()
        Reward_Dashboard.disableKindlybtn()
        Reward_Dashboard.DisplayKindlyAct()
        Me.Dispose()
    End Sub

    Private Sub kindly_act_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If kindlySave = "" And kindlyPath = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        ElseIf Kindly_save_btn.Text = "Edit" Then
            displayspecificKindly()
        End If
    End Sub

    Private Sub MVP_save_btn_Click(sender As Object, e As EventArgs) Handles Kindly_save_btn.Click
        dateissued = issued_date_picker.Value.ToString("d MMMM, yyyy")
        If Kindly_save_btn.Text = "Save" Then
            If opendb() Then
                Dim query As String = "INSERT INTO `kindly_acts`(`student_id`, `date_issued`, `kindly_act`) VALUES ('" & id_student & "','" & dateissued & "', '" & kindly_description_txtbox.Text & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    PrimaryKey_kindly = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message" + PrimaryKey_kindly)

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CreateDocument()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        ElseIf Kindly_save_btn.Text = "Edit" Then
            If opendb() Then

                Dim query As String = "UPDATE `kindly_acts` SET `student_id`='" & id_student & "', `date_issued`='" & dateissued & "', `kindly_act`='" & kindly_description_txtbox.Text & "' WHERE `id` = '" & PrimaryKey_kindly & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub MVP_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MVP_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If MVP_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id, students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & MVP_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub displayspecificKindly()
        If opendb() Then
            Dim query As String = "SELECT
                kindly_acts.id AS ID,
                students.id AS IDstudent,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                kindly_acts.kindly_act
            FROM
                kindly_acts
            JOIN students ON kindly_acts.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                kindly_acts.id = '" & PrimaryKey_kindly & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("IDstudent")
                    MVP_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Full_name_txtbox.Text = dtreader.GetString("Firstname") + " " + dtreader.GetString("Middlename") + " " + dtreader.GetString("Lastname") + ""
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("program_name")
                    kindly_description_txtbox.Text = dtreader.GetString("kindly_act")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub DocumentPath()
        kindly = "Kindly Act"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Kindly + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    kindlyPath = dtreader.GetString("path")
                    kindlySave = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                kindly_acts.id AS ID,
                students.id AS IDstudent,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                kindly_acts.kindly_act,
                kindly_acts.date_issued
            FROM
                kindly_acts
            JOIN students ON kindly_acts.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                kindly_acts.id = '" & PrimaryKey_kindly & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("IDstudent")
                    PrimaryKey_kindly = dtreader.GetString("ID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")


                    ReturnedItem = dtreader.GetString("kindly_act")
                    DatePresented = dtreader.GetString("date_issued")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + PrimaryKey_kindly
            fullpath = kindlySave + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(kindlyPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("DateIssued").Range.Text = DatePresented
            wdBooks("ReturnedItem").Range.Text = ReturnedItem


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If Kindly = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & id_student & "','" & Kindly & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Reward_Dashboard.disableKindlybtn()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub kindly_act_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        formclose()
    End Sub
End Class