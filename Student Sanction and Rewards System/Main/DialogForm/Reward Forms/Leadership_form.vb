﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Public Class Leadership_form

    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Dim id_student As String

    Dim dateissued As String
    Public PrimaryKey_Leader As String

    Dim Leadership As String
    Dim LeadershipPath As String
    Dim LeadershipSave As String
    Dim fullpath As String
    Dim DatabasePath As String

    Dim StudentName As String
    Dim DateGiven As String
    Dim VPSDASName As String
    Dim EventTitle As String

    Private Sub formclose()
        Reward_Dashboard.DisableLeadershipbtn()
        Me.Dispose()
    End Sub
    Private Sub Leadership_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If LeadershipPath = "" And LeadershipSave = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        ElseIf leader_save_btn.Text = "Edit" Then
            DisplaySpecificLeadership()
        End If
    End Sub

    Private Sub DisplaySpecificLeadership()
        If opendb() Then
            Dim query As String = "SELECT
                leaderships.id AS ID,
                students.id AS studid,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                leaderships.date_issued,
                leaderships.event_title,
                leaderships.vpsdas_name
            FROM
                leaderships
            JOIN students ON leaderships.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                leaderships.id = '" & PrimaryKey_Leader & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("studid")
                    leader_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Full_name_txtbox.Text = dtreader.GetString("Firstname") + " " + dtreader.GetString("Middlename") + " " + dtreader.GetString("Lastname") + ""
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("program_name")

                    event_title_txtbox.Text = dtreader.GetString("event_title")

                    person_incharge_name_txtbox.Text = dtreader.GetString("vpsdas_name")


                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub


    Private Sub leader_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles leader_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If leader_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & leader_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub leader_save_btn_Click(sender As Object, e As EventArgs) Handles leader_save_btn.Click
        dateissued = issued_date_picker.Value.ToString("MMMM d, yyyy")
        If leader_save_btn.Text = "Save" Then
            If opendb() Then

                Dim query As String = "INSERT INTO `leaderships`(`student_id`, `date_issued`, `event_title`, `vpsdas_name`) VALUES ('" & id_student & "','" & dateissued & "', '" & event_title_txtbox.Text & "','" & person_incharge_name_txtbox.Text & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    PrimaryKey_Leader = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)

                Finally
                    conn.Close()
                    CreateDocument()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        ElseIf leader_save_btn.Text = "Edit" Then

            If opendb() Then

                Dim query As String = "UPDATE `leaderships` SET `student_id`='" & id_student & "',`date_issued`='" & dateissued & "',`event_title`='" & event_title_txtbox.Text & "',`vpsdas_name`='" & person_incharge_name_txtbox.Text & "' WHERE `id` = '" & PrimaryKey_Leader & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub DocumentPath()
        Leadership = "Leadership"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Leadership + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    LeadershipPath = dtreader.GetString("path")
                    LeadershipSave = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                leaderships.id AS ID,
                students.id AS studid,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                leaderships.date_issued,
                leaderships.event_title,
                leaderships.vpsdas_name
            FROM
                leaderships
            JOIN students ON leaderships.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                leaderships.id = '" & PrimaryKey_Leader & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("studid")
                    PrimaryKey_Leader = dtreader.GetString("ID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    DateGiven = dtreader.GetString("date_issued")
                    EventTitle = dtreader.GetString("event_title")
                    VPSDASName = dtreader.GetString("vpsdas_name")

                End While
            Catch ex As Exception
                MsgBox(ex.Message + "Create Document")
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + PrimaryKey_Leader
            fullpath = LeadershipSave + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(LeadershipPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("DateIssued").Range.Text = DateGiven
            wdBooks("EventTitle").Range.Text = EventTitle
            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("VPSDASName").Range.Text = VPSDASName


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If LeadershipPath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & id_student & "','" & Leadership & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Leadership_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        formclose()
    End Sub
End Class