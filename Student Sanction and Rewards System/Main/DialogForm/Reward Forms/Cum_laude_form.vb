﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word

Public Class Cum_laude_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Dim id_student As String

    Dim dateissued As String
    Public PrimaryKey_Cumlaude As String

    Dim CumLaude As String
    Dim CumLaudePath As String
    Dim CumLaudeSave As String
    Dim fullpath As String
    Dim DatabasePath As String

    Dim studentid As String
    Dim DateGiven As String
    Dim Program As String
    Dim yeaSchool As String
    Dim StudentName As String

    Private Sub formclose()
        Reward_Dashboard.disableCumLaudebtn()
        Me.Dispose()
    End Sub


    Private Sub Cumlaude_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Cumlaude_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Cumlaude_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Cumlaude_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub cumlaude_save_btn_Click(sender As Object, e As EventArgs) Handles cumlaude_save_btn.Click
        dateissued = issued_date_picker.Value.ToString("MMMM d, yyyy")
        If cumlaude_save_btn.Text = "Save" Then
            If opendb() Then

                Dim query As String = "INSERT INTO `cum_laudes`(`student_id`, `date_issued`, `school_year`) VALUES ('" & id_student & "','" & dateissued & "', '" & school_year_txtbox.Text & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    PrimaryKey_Cumlaude = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)

                Finally
                    conn.Close()
                    CreateDocument()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        ElseIf cumlaude_save_btn.Text = "Edit" Then

            If opendb() Then

                Dim query As String = "UPDATE `cum_laudes` SET `student_id`='" & id_student & "', `date_issued`='" & dateissued & "', `school_year`='" & school_year_txtbox.Text & "' WHERE `id` = '" & PrimaryKey_Cumlaude & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        End If
    End Sub

    Private Sub DisplaySpecificCumlaude()
        If opendb() Then
            Dim query As String = "SELECT
                cum_laudes.id AS ID,
                students.id AS studid,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                cum_laudes.date_issued,
                cum_laudes.school_year
            FROM
                cum_laudes
            JOIN students ON cum_laudes.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE
                cum_laudes.id ='" & PrimaryKey_Cumlaude & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("studid")
                    Cumlaude_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Full_name_txtbox.Text = dtreader.GetString("Firstname") + " " + dtreader.GetString("Middlename") + " " + dtreader.GetString("Lastname") + ""
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("program_name")

                    school_year_txtbox.Text = dtreader.GetString("school_year")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Cum_laude_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If CumLaudePath = "" And CumLaudeSave = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        ElseIf cumlaude_save_btn.Text = "Edit" Then
            DisplaySpecificCumlaude()
        End If
    End Sub

    Private Sub DocumentPath()
        CumLaude = "Cum Laude"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + CumLaude + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    CumLaudePath = dtreader.GetString("path")
                    CumLaudeSave = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                            cum_laudes.id AS ID,
                            students.id AS studid,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            programs.program_name,
                            cum_laudes.date_issued,
                            cum_laudes.school_year
                        FROM
                            cum_laudes
                        JOIN students ON cum_laudes.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                    WHERE
                     cum_laudes.id = '" & PrimaryKey_Cumlaude & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    studentid = dtreader.GetString("studid")
                    PrimaryKey_Cumlaude = dtreader.GetString("ID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    DateGiven = dtreader.GetString("date_issued")
                    Program = dtreader.GetString("Abbreviation")
                    yeaSchool = dtreader.GetString("school_year")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + PrimaryKey_Cumlaude
            fullpath = CumLaudeSave + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(CumLaudePath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("Program").Range.Text = Program
            wdBooks("DateIssued").Range.Text = DateGiven
            wdBooks("SchoolYear").Range.Text = yeaSchool
            wdBooks("StudentName").Range.Text = StudentName


            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If CumLaudePath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & studentid & "','" & CumLaude & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Cum_laude_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        formclose()
    End Sub
End Class