﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word

Public Class Outstanding_athlete_form

    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Dim Outstanding As String
    Dim OutstandingPath As String
    Dim OutstandingSave As String
    Dim fullpath As String
    Dim DatabasePath As String

    Dim id_student As String

    Dim dateissued As String
    Public PrimaryKey_Outstanding_athlete As String

    Dim CoachName As String
    Dim DatePresented As String
    Dim OrganizerName As String
    Dim Sports As String
    Dim StudentName As String

    Private Sub formclose()
        Reward_Dashboard.DisableOutstandingbtn()
        Reward_Dashboard.DisplayOutstandingRewards()
        Me.Dispose()
    End Sub

    Private Sub Outstanding_athlete_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If OutstandingPath = "" And OutstandingSave = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        ElseIf Outstanding_athlete_save_btn.Text = "Edit" Then
            DisplaySpecifecOutstanding()
        End If
    End Sub

    Private Sub Outstanding_athlete_save_btn_Click(sender As Object, e As EventArgs) Handles Outstanding_athlete_save_btn.Click
        dateissued = issued_date_picker.Value.ToString("d MMMM, yyyy")
        If Outstanding_athlete_save_btn.Text = "Save" Then
            If opendb() Then

                Dim query As String = "INSERT INTO `outstanding_athlete` (`student_id`, `coach_name`, `organizer_name`, `sports`, `date_issued`) VALUES ('" & id_student & "','" & Choach_txtbox.Text & "', '" & Organizer_txtbox.Text & "','" & Sports_title_txtbox.Text & "','" & dateissued & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    PrimaryKey_Outstanding_athlete = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CreateDocument()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        ElseIf Outstanding_athlete_save_btn.Text = "Edit" Then
            If opendb() Then

                Dim query As String = "UPDATE `outstanding_athlete` SET `student_id`='" & id_student & "',`coach_name`='" & Choach_txtbox.Text & "',`organizer_name`='" & Organizer_txtbox.Text & "',`sports`='" & Sports_title_txtbox.Text & "',`date_issued`='" & dateissued & "' WHERE `id` = '" & PrimaryKey_Outstanding_athlete & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        End If
    End Sub

    Private Sub Honor_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Outstanding_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Outstanding_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Outstanding_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetString("id")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub DisplaySpecifecOutstanding()
        If opendb() Then
            Dim query As String = "SELECT
                outstanding_athlete.id AS ID,
                students.id as IDstudent,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
                outstanding_athlete.sports,
                outstanding_athlete.date_issued,
                outstanding_athlete.coach_name,
                outstanding_athlete.organizer_name
            FROM
                outstanding_athlete
            JOIN students ON outstanding_athlete.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE outstanding_athlete.id = '" & PrimaryKey_Outstanding_athlete & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("IDstudent")
                    Outstanding_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Full_name_txtbox.Text = dtreader.GetString("Firstname") + " " + dtreader.GetString("Middlename") + " " + dtreader.GetString("Lastname") + ""
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("program_name")
                    Sports_title_txtbox.Text = dtreader.GetString("sports")

                    Choach_txtbox.Text = dtreader.GetString("coach_name")
                    Organizer_txtbox.Text = dtreader.GetString("organizer_name")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub DocumentPath()
        Outstanding = "Outstanding Athlete"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Outstanding + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    OutstandingPath = dtreader.GetString("path")
                    OutstandingSave = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisplayCase()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                outstanding_athlete.id AS ID,
                students.id AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                outstanding_athlete.sports,
                outstanding_athlete.date_issued,
                outstanding_athlete.coach_name,
                outstanding_athlete.organizer_name
            FROM
                outstanding_athlete
            JOIN students ON outstanding_athlete.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE outstanding_athlete.id = '" & PrimaryKey_Outstanding_athlete & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("StudentID")
                    PrimaryKey_Outstanding_athlete = dtreader.GetString("ID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")


                    Sports = dtreader.GetString("sports")
                    CoachName = dtreader.GetString("coach_name")
                    OrganizerName = dtreader.GetString("organizer_name")
                    DatePresented = dtreader.GetString("date_issued")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + PrimaryKey_Outstanding_athlete
            fullpath = OutstandingSave + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(OutstandingPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("CoachName").Range.Text = CoachName
            wdBooks("DatePresented").Range.Text = DatePresented
            wdBooks("Organizername").Range.Text = OrganizerName
            wdBooks("Sports").Range.Text = Sports
            wdBooks("Sports2").Range.Text = Sports
            wdBooks("StudentName").Range.Text = StudentName

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If OutstandingPath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & id_student & "','" & Outstanding & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Outstanding_athlete_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        formclose()
    End Sub
End Class