﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Public Class Honor_form

    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String

    Dim id_student As String

    Dim dateissued As String
    Public PrimaryKey_Honor As String

    Dim Honor As String
    Dim HonorPath As String
    Dim HonorSave As String
    Dim fullpath As String
    Dim DatabasePath As String

    Dim studentid As String
    Dim DateGiven As String
    Dim GWA As String
    Dim yeaSchool As String
    Dim StudentName As String
    Dim UnitHead As String
    Dim UnitHeadTitle As String


    Private Sub formclose()
        Reward_Dashboard.DisableHonorbtn()
        Reward_Dashboard.DisplayHonors()
        Me.Dispose()
    End Sub

    Private Sub Honor_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If HonorPath = "" And HonorSave = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        ElseIf Honor_save_btn.Text = "Edit" Then
            DisplaySpecificHonor()
        End If
    End Sub

    Private Sub DisplaySpecificHonor()
        If opendb() Then
            Dim query As String = "SELECT
	            honors.id as ID,
                students.id as studid,
                students.student_no AS StudentID,
                students.first_name AS Firstname,
                students.middle_name AS Middlename,
                students.last_name AS Lastname,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Abbreviation,
                programs.program_name,
   	            honors.gwa as GWA,
                honors.school_year as SchoolYear,
                honors.date as Date,
                honors.unit_head,
                honors.unit_head_title
            FROM
                honors
            JOIN students on honors.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            WHERE honors.id = '" & PrimaryKey_Honor & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    id_student = dtreader.GetString("studid")
                    Honor_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Full_name_txtbox.Text = dtreader.GetString("Firstname") + " " + dtreader.GetString("Middlename") + " " + dtreader.GetString("Lastname") + ""
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("program_name")
                    StudentGWA.Text = dtreader.GetString("GWA")

                    Unit_head.Text = dtreader.GetString("unit_head")
                    Unit_head_title.Text = dtreader.GetString("unit_head_title")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub


    Private Sub Honor_save_btn_Click(sender As Object, e As EventArgs) Handles Honor_save_btn.Click
        dateissued = issued_date_picker.Value.ToString("MMMM d, yyyy")
        If Honor_save_btn.Text = "Save" Then
            If opendb() Then

                Dim query As String = "INSERT INTO `honors`(`student_id`, `gwa`, `school_year`, `date`, `unit_head`, `unit_head_title`) VALUES ('" & id_student & "','" & StudentGWA.Text & "', '" & SchoolYear.Text & "','" & dateissued & "','" & Unit_head.Text & "' ,'" & Unit_head_title.Text & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    PrimaryKey_Honor = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CreateDocument()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        ElseIf Honor_save_btn.Text = "Edit" Then
            If opendb() Then

                Dim query As String = "UPDATE `honors` SET `student_id`='" & id_student & "',`gwa`='" & StudentGWA.Text & "',`school_year`='" & SchoolYear.Text & "',`date`='" & dateissued & "',`unit_head`='" & Unit_head.Text & "',`unit_head_title`='" & Unit_head.Text & "' WHERE `id` = '" & PrimaryKey_Honor & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    formclose()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        End If
    End Sub

    Private Sub Honor_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Honor_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Honor_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Honor_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub DocumentPath()
        Honor = "Honors"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Honor + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    HonorPath = dtreader.GetString("path")
                    HonorSave = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CreateDocument()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                            honors.id AS ID,
                            students.id as IDstudent,
                            students.student_no AS StudentID,
                            students.first_name AS Firstname,
                            students.middle_name AS Middlename,
                            students.last_name AS Lastname,
                            students.age AS Age,
                            students.gender AS Gender,
                            students.section AS Section,
                            programs.abbreviation AS Abbreviation,
                            honors.gwa AS GWA,
                            honors.school_year AS SchoolYear,
                            honors.date,
                            honors.unit_head,
                            honors.unit_head_title
                        FROM
                            honors
                        JOIN students ON honors.student_id = students.id
                        JOIN programs ON students.program_id = programs.id
                    WHERE
                     honors.id = '" & PrimaryKey_Honor & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    studentid = dtreader.GetString("IDstudent")
                    PrimaryKey_Honor = dtreader.GetString("ID")
                    StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    GWA = dtreader.GetString("GWA")
                    DateGiven = dtreader.GetString("date")
                    UnitHead = dtreader.GetString("unit_head")
                    yeaSchool = dtreader.GetString("SchoolYear")
                    UnitHeadTitle = dtreader.GetString("unit_head_title")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        Try 'Merging Data from database to Document Themeplate
            sFileName = StudentName + "_" + PrimaryKey_Honor
            fullpath = HonorSave + "\" + sFileName
            wdApp = New Word.Application
            wdDocs = wdApp.Documents

            Dim wdDoc As Word.Document = wdDocs.Add(HonorPath)
            Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

            wdBooks("GWA").Range.Text = GWA
            wdBooks("DateGiven").Range.Text = DateGiven
            wdBooks("SchoolYear").Range.Text = yeaSchool
            wdBooks("StudentName").Range.Text = StudentName
            wdBooks("UnitHead").Range.Text = UnitHead
            wdBooks("UnitHeadTitle").Range.Text = UnitHeadTitle

            wdDoc.SaveAs(fullpath)
            ReleaseObject(wdBooks)
            wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
            ReleaseObject(wdDoc)
            wdApp.Quit()


            DatabasePath = fullpath.Replace("\", "\\")
        Catch ex As Exception
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
        End Try

        If HonorPath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & studentid & "','" & Honor & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Honor_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        formclose()
    End Sub
End Class