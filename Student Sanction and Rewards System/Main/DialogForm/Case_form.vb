﻿Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports MySql.Data.MySqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class Case_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String
    Dim CounsellingCrystal As New CrystalReportCounselling

    Dim datesched As String
    Dim id_student As String
    Dim id_violation As String
    Dim recommendation As String

    Dim Case_StudentID As String
    Dim Case_Program_Section As String
    Dim Case_Violation As String
    Dim Case_Incident_Report As String
    Dim Case_Resolution As String
    Dim Case_Recommendations As String
    Dim Case_Date As String
    Dim Case_Chairmain As String
    Dim Case_members As String
    Dim Case_StudentName As String
    Dim Case_Primarykey As String
    Dim student_id As String

    Dim DatabasePath As String
    Dim CasePath As String
    Dim SavePath As String
    Dim fullpath As String
    Dim Cases As String
    Public primaryid_case As String

    Private Sub Case_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Documentpath()

        If CasePath = "" And SavePath = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        Else
            If Main_Dashboard.Case_Primary_id > 0 And Case_save_btn.Text = "Edit" Then
                Case_edit()
            End If
            Display_action_information()
        End If
    End Sub

    Private Sub Display_action_information()
        If opendb() Then
            Dim query As String = "SELECT
                disciplinary_action.id AS ID,
                students.id as IDstudent,
                students.student_no AS StudenID,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.age,
                students.gender,
                programs.program_name,
                offenses.offense,
                violations.code,
                violations.violation
            FROM
                disciplinary_action
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id

            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                disciplinary_action.id = '" + Main_Dashboard.Disciplinary_action_primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                If dtreader.Read Then
                    student_id = dtreader.GetString("IDstudent")
                    Case_studentid_txtbox.Text = dtreader.GetString("StudenID")
                    Case_violationcode_txtbox.Text = dtreader.GetString("code")
                    Case_age_txtbox.Text = dtreader.GetInt32("age")
                    Case_gender_txtbox.Text = dtreader.GetString("gender")
                    Case_section_txtbox.Text = dtreader.GetString("section")
                    Case_course_txtbox.Text = dtreader.GetString("program_name")
                    Case_fullname_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","

                    Case_offensive_textbox.Text = dtreader.GetString("offense")
                    Case_violationcode_txtbox.Text = dtreader.GetString("code")
                    Case_violation_description_textbox.Text = dtreader.GetString("violation")
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Documentpath()
        Cases = "Case"
        'create
        If opendb() Then
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Cases + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    CasePath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CrystalReportCounselling()
        If opendb() Then
            Dim query As String = "SELECT
                cases.id AS ID,
                disciplinary_action.id AS disciplinaryID,
                students.id as studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.age,
                students.gender,
                violations.code,
                violations.violation,
                programs.abbreviation,
                programs.program_name,
                offenses.offense,
                cases.recommend,
                cases.resolution,
                cases.date,
                cases.chairman,
                cases.members,
                cases.report
            FROM
                `cases`
            JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
            cases.id = '" + Main_Dashboard.Case_Primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Case_Primarykey = dtreader.GetString("ID")
                    student_id = dtreader.GetString("studentID")
                    Case_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Case_Program_Section = dtreader.GetString("abbreviation") + " " + dtreader.GetString("section")
                    Case_StudentID = dtreader.GetString("student_no")

                    Case_Violation = dtreader.GetString("violation")

                    Case_Resolution = dtreader.GetString("resolution")
                    Case_Incident_Report = dtreader.GetString("report")

                    Case_members = dtreader.GetString("members")
                    Case_Chairmain = dtreader.GetString("chairman")

                    Case_Resolution = dtreader.GetString("recommend")

                    Case_Date = dtreader.GetDateTime("date")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        CounsellingCrystal.SetParameterValue("StudentName", Case_StudentName)
        CounsellingCrystal.SetParameterValue("ProgramYearSection", Case_Program_Section)
        CounsellingCrystal.SetParameterValue("StudentID", Case_StudentID)
        CounsellingCrystal.SetParameterValue("Violation", Case_Violation)
        CounsellingCrystal.SetParameterValue("IncidentReport", Case_Incident_Report)

        If Case_Recommendations = "Closed/Resolve" Then

            CounsellingCrystal.SetParameterValue("Close/Resolve", "X")
            CounsellingCrystal.SetParameterValue("ContinueHearing", " ")
            CounsellingCrystal.SetParameterValue("FormalInvestigation", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", " ")

        ElseIf Case_Recommendations = "For Investagation" Then

            CounsellingCrystal.SetParameterValue("FormalInvestigation", "X")
            CounsellingCrystal.SetParameterValue("ContinueHearing", " ")
            CounsellingCrystal.SetParameterValue("Close/Resolve", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", " ")

        ElseIf Case_Recommendations = "Hearing Continuation" Then

            CounsellingCrystal.SetParameterValue("ContinueHearing", "X")
            CounsellingCrystal.SetParameterValue("FormalInvestigation", " ")
            CounsellingCrystal.SetParameterValue("Close/Resolve", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", Case_Date)

        End If

        CounsellingCrystal.SetParameterValue("Resolution", Case_Resolution)
        CounsellingCrystal.SetParameterValue("Chairman", Case_Chairmain)
        CounsellingCrystal.SetParameterValue("Members", Case_members)

        CrystalReportViewer.CrystalReportViewer1.ReportSource = CounsellingCrystal

        sFileName = Case_StudentName + "_" + primaryid_case
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = CounsellingCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            CounsellingCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        DatabasePath = fullpath.Replace("\", "\\")

        If SavePath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & student_id & "','" & Cases & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Public Sub ViewCounsellingCrystal()
        Documentpath()

        If opendb() Then
            Dim query As String = "SELECT
                cases.id AS ID,
                disciplinary_action.id AS disciplinaryID,
                students.id as studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.age,
                students.gender,
                violations.code,
                violations.violation,
                programs.abbreviation,
                programs.program_name,
                offenses.offense,
                cases.recommend,
                cases.resolution,
                cases.date,
                cases.chairman,
                cases.members,
                cases.report
            FROM
                `cases`
            JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
            cases.id = '" + Main_Dashboard.Case_Primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Case_Primarykey = dtreader.GetString("ID")
                    student_id = dtreader.GetString("studentID")
                    Case_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Case_Program_Section = dtreader.GetString("abbreviation") + " " + dtreader.GetString("section")
                    Case_StudentID = dtreader.GetString("student_no")

                    Case_Violation = dtreader.GetString("violation")

                    Case_Resolution = dtreader.GetString("resolution")
                    Case_Incident_Report = dtreader.GetString("report")

                    Case_members = dtreader.GetString("members")
                    Case_Chairmain = dtreader.GetString("chairman")

                    Case_Recommendations = dtreader.GetString("recommend")

                    Case_Date = dtreader.GetDateTime("date")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        CounsellingCrystal.SetParameterValue("StudentName", Case_StudentName)
        CounsellingCrystal.SetParameterValue("ProgramYearSection", Case_Program_Section)
        CounsellingCrystal.SetParameterValue("StudentID", Case_StudentID)
        CounsellingCrystal.SetParameterValue("Violation", Case_Violation)
        CounsellingCrystal.SetParameterValue("IncidentReport", Case_Incident_Report)

        If Case_Recommendations = "Closed/Resolve" Then

            CounsellingCrystal.SetParameterValue("Close/Resolve", "X")
            CounsellingCrystal.SetParameterValue("ContinueHearing", " ")
            CounsellingCrystal.SetParameterValue("FormalInvestigation", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", " ")

        ElseIf Case_Recommendations = "For Investagation" Then

            CounsellingCrystal.SetParameterValue("FormalInvestigation", "X")
            CounsellingCrystal.SetParameterValue("ContinueHearing", " ")
            CounsellingCrystal.SetParameterValue("Close/Resolve", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", " ")

        ElseIf Case_Recommendations = "Hearing Continuation" Then

            CounsellingCrystal.SetParameterValue("ContinueHearing", "X")
            CounsellingCrystal.SetParameterValue("FormalInvestigation", " ")
            CounsellingCrystal.SetParameterValue("Close/Resolve", " ")

            CounsellingCrystal.SetParameterValue("HearingDate", Case_Date)

        End If

        CounsellingCrystal.SetParameterValue("Resolution", Case_Resolution)
        CounsellingCrystal.SetParameterValue("Chairman", Case_Chairmain)
        CounsellingCrystal.SetParameterValue("Members", Case_members)

        CrystalReportViewer.CrystalReportViewer1.ReportSource = CounsellingCrystal

        sFileName = Case_StudentName + "_" + primaryid_case
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = CounsellingCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            CounsellingCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        CrystalReportViewer.ShowDialog()

    End Sub

    'Private Sub CreateDocument()

    '    'MsgBox(primaryid_case)
    '    If opendb() Then
    '        Dim query As String = "SELECT
    '            cases.id AS ID,
    '            disciplinary_action.id AS disciplinaryID,
    '            students.id as studentID,
    '            students.student_no,
    '            students.first_name,
    '            students.middle_name,
    '            students.last_name,
    '            students.section,
    '            students.age,
    '            students.gender,
    '            violations.code,
    '            violations.violation,
    '            programs.abbreviation,
    '            programs.program_name,
    '            offenses.offense,
    '            cases.recommend,
    '            cases.resolution,
    '            cases.date,
    '            cases.chairman,
    '            cases.members,
    '            cases.report
    '        FROM
    '            `cases`
    '        JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
    '        JOIN referrals ON disciplinary_action.referral_id = referrals.id
    '        JOIN violations ON referrals.violation_id = violations.id
    '        JOIN students ON referrals.student_id = students.id
    '        JOIN programs ON students.program_id = programs.id
    '        JOIN offenses ON violations.offenses_id = offenses.id
    '        WHERE
    '        cases.id = '" + primaryid_case + "'"

    '        Dim cmd As New MySqlCommand(query, conn)
    '        Dim dtreader As MySqlDataReader

    '        Try
    '            dtreader = cmd.ExecuteReader

    '            While dtreader.Read

    '                Case_Primarykey = dtreader.GetString("ID")
    '                student_id = dtreader.GetString("studentID")
    '                Case_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
    '                Case_Program_Section = dtreader.GetString("section")
    '                Case_StudentID = dtreader.GetString("student_no")

    '                Case_Violation = dtreader.GetString("violation")

    '                Case_Resolution = dtreader.GetString("resolution")
    '                Case_Incident_Report = dtreader.GetString("report")

    '                Case_members = dtreader.GetString("members")
    '                Case_Chairmain = dtreader.GetString("chairman")

    '                Case_Resolution = dtreader.GetString("recommend")

    '                Case_Date = dtreader.GetDateTime("date")

    '            End While

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        Finally
    '            conn.Close()
    '        End Try
    '    Else
    '        MsgBox("NO database connections")
    '    End If

    '    Try
    '        sFileName = Case_StudentName + "_" + primaryid_case
    '        fullpath = SavePath + "\" + sFileName 'save path with filename
    '        wdApp = New Word.Application
    '        wdDocs = wdApp.Documents

    '        Dim wdDoc As Word.Document = wdDocs.Add(CasePath) 'Locate Themeplate
    '        Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

    '        wdBooks("StudentName").Range.Text = Case_StudentName
    '        wdBooks("StudentID").Range.Text = Case_StudentID
    '        wdBooks("Section").Range.Text = Case_Program_Section
    '        wdBooks("Violation").Range.Text = Case_Violation
    '        wdBooks("Report").Range.Text = Case_Incident_Report
    '        wdBooks("Resolution").Range.Text = Case_Resolution

    '        If Case_Resolution = "Closed/Resolve" Then
    '            wdBooks("Resolve").Range.Text = "X"
    '            wdBooks("Hearing").Range.Text = ""
    '            wdBooks("Investagation").Range.Text = ""
    '        ElseIf Case_Resolution = "For Investagation" Then
    '            wdBooks("Investagation").Range.Text = "X"
    '            wdBooks("Hearing").Range.Text = ""
    '            wdBooks("Resolve").Range.Text = ""
    '        ElseIf Case_Resolution = "Hearing Continuation" Then
    '            wdBooks("Hearing").Range.Text = "X"
    '            wdBooks("Investagation").Range.Text = ""
    '            wdBooks("Resolve").Range.Text = ""
    '            wdBooks("Date").Range.Text = Case_Date
    '        End If

    '        wdBooks("Chairman").Range.Text = Case_Chairmain
    '        wdBooks("Members").Range.Text = Case_members

    '        wdDoc.SaveAs(fullpath)
    '        ReleaseObject(wdBooks)
    '        wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
    '        ReleaseObject(wdDoc)
    '        wdApp.Quit()

    '        DatabasePath = fullpath.Replace("\", "\\")

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    If opendb() Then
    '        Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & student_id & "','" & Cases & "','" & DatabasePath & "')"

    '        Dim cmd As New MySqlCommand(query, conn)
    '        Dim dtreader As MySqlDataReader

    '        Try
    '            dtreader = cmd.ExecuteReader
    '            MessageBox.Show("Succesfuly Created.",
    '    "Important Message")

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        Finally
    '            conn.Close()
    '            Main_Dashboard.DisableCaseBTN()
    '            Me.Close()
    '        End Try
    '    Else
    '        MsgBox("NO database connections")
    '    End If
    'End Sub

    'Private Sub ReleaseObject(ByVal obj As Object)
    '    Try
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
    '        obj = Nothing
    '    Catch ex As Exception
    '        obj = Nothing
    '    Finally
    '        GC.Collect()
    '    End Try
    'End Sub

    Private Sub Case_hearing_radiobutton_CheckedChanged(sender As Object, e As EventArgs) Handles Case_hearing_radiobutton.CheckedChanged
        If Case_hearing_radiobutton.Checked = True Then
            Case_hearing_datepicker.Enabled = True
        Else
            Case_hearing_datepicker.Enabled = False
        End If
    End Sub
    Private Sub Case_edit()
        If opendb() Then

            Dim query As String = "SELECT
                cases.id AS ID,
                disciplinary_action.id AS disciplinaryID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.age,
                students.gender,
                violations.code,
                violations.violation,
                programs.abbreviation,
                programs.program_name,
                offenses.offense,
                cases.recommend,
                cases.resolution,
                cases.date,
                cases.chairman,
                cases.members,
                cases.report
            FROM
                `cases`
            JOIN disciplinary_action ON cases.disciplinary_action_id = disciplinary_action.id
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
            cases.id = '" + Main_Dashboard.Case_Primary_id + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read
                    Case_age_txtbox.Text = dtreader.GetInt32("age")
                    Case_gender_txtbox.Text = dtreader.GetString("gender")
                    Case_section_txtbox.Text = dtreader.GetString("section")
                    Case_studentid_txtbox.Text = dtreader.GetString("student_no")
                    Case_course_txtbox.Text = dtreader.GetString("program_name")
                    Case_fullname_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","

                    Case_offensive_textbox.Text = dtreader.GetString("offense")
                    Case_violationcode_txtbox.Text = dtreader.GetString("code")
                    Case_violation_description_textbox.Text = dtreader.GetString("violation")

                    Case_resolution_txtbox.Text = dtreader.GetString("resolution")
                    Case_incident_description_txtbox.Text = dtreader.GetString("report")

                    Case_members_txtbox.Text = dtreader.GetString("members")
                    Case_chairman_txtbox.Text = dtreader.GetString("chairman")

                    If dtreader.GetString("recommend") = "Closed/Resolve" Then
                        Case_Resolve_radiobutton.Checked = True
                    ElseIf dtreader.GetString("recommend") = "For Investagation" Then
                        Case_investigation_radiobutton.Checked = True
                    ElseIf dtreader.GetString("recommend") = "Hearing Continuation" Then
                        Case_hearing_radiobutton.Checked = True
                        Case_hearing_datepicker.Value = dtreader.GetDateTime("date")
                    End If

                End While


            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Case_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Case_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Case_studentid_txtbox.Text.Trim(" ") = "" Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Case_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            If dtreader.Read Then
                                Case_fullname_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                                Case_gender_txtbox.Text = dtreader.GetString("gender")
                                Case_age_txtbox.Text = dtreader.GetInt32("age")
                                Case_section_txtbox.Text = dtreader.GetString("section")
                                Case_course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")
                            End If

                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub Case_violationcode_txtbox_KeyDown(sender As Object, e As KeyEventArgs) Handles Case_violationcode_txtbox.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True

            If opendb() Then
                Dim query As String = "SELECT violations.id as ID, offenses.offense as Category, violations.code as Code, violations.violation as Violation FROM violations JOIN offenses ON violations.offenses_id=offenses.id WHERE violations.code LIKE '%" & Case_violationcode_txtbox.Text & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                    While dtreader.Read

                        Case_offensive_textbox.Text = dtreader.GetString("Category")
                        Case_violation_description_textbox.Text = dtreader.GetString("Violation")
                        id_violation = dtreader.GetInt32("ID")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Case_save_btn_Click(sender As Object, e As EventArgs) Handles Case_save_btn.Click
        If Case_Resolve_radiobutton.Checked = True Then
            recommendation = "Closed/Resolve"
        ElseIf Case_investigation_radiobutton.Checked = True Then
            recommendation = "For Investagation"
        ElseIf Case_hearing_radiobutton.Checked = True Then
            Case_hearing_datepicker.Enabled = True
            recommendation = "Hearing Continuation"
            datesched = Case_hearing_datepicker.Value.ToString("yyyy-M-d")
        End If

        If Case_save_btn.Text = "Save" Then

            If opendb() Then

                Dim query As String = "INSERT INTO `cases`(`disciplinary_action_id`, `report`, `resolution`, `recommend`, `chairman`, `members`, `date`) VALUES ('" & Main_Dashboard.Disciplinary_action_primary_id & "', '" & Case_incident_description_txtbox.Text & "','" & Case_resolution_txtbox.Text & "','" & recommendation & "','" & Case_chairman_txtbox.Text & "','" & Case_members_txtbox.Text & "', '" & datesched & "'); SELECT LAST_INSERT_ID()"
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    primaryid_case = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CrystalReportCounselling()
                    Main_Dashboard.DisplayCase()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        ElseIf Case_save_btn.Text = "Edit" Then
            If opendb() Then

                Dim query As String = "UPDATE `cases` SET `report`='" & Case_incident_description_txtbox.Text & "',`resolution`='" & Case_resolution_txtbox.Text & "',`recommend`='" & recommendation & "',`date`='" & datesched & "',`chairman`='" & Case_chairman_txtbox.Text & "',`members`='" & Case_members_txtbox.Text & "' WHERE id = '" + Main_Dashboard.Case_Primary_id + "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edited.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CrystalReportCounselling()
                    Main_Dashboard.DisplayCase()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        End If
    End Sub


    Private Sub Case_form_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
        Main_Dashboard.DisableDisciplinarActionBTN()
    End Sub
End Class