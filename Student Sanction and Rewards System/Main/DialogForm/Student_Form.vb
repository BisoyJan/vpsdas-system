﻿Imports MySql.Data.MySqlClient
Public Class Student_Form
    Dim studentIDcheck As String

    Private Sub Student_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Display_program_drpbox()

        If Student_add_btn.Text = "Edit" Then
            Edit_student()
        End If
    End Sub

    Private Sub ClearTexbox()
        For Each Control As Control In Me.Controls
            If TypeOf Control Is TextBox Then
                Control.Text = String.Empty
            End If
        Next
    End Sub

    Private Sub Display_program_drpbox()
        If opendb() Then
            Dim query As String = "SELECT * FROM `programs`"

            Dim adapter As New MySqlDataAdapter(query, conn)
            Dim table As New DataTable()

            Try
                adapter.Fill(table)

                Program_drpdwn.DataSource = table
                Program_drpdwn.DisplayMember = "program_name"
                Program_drpdwn.ValueMember = "id"

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Edit_student()
        If opendb() Then
            Dim query As String = "SELECT * FROM `students` WHERE id = '" & Main_Dashboard.Student_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader
            Dim table As New DataTable()

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read
                    Student_id_txtbox.Text = dtreader.GetInt32("student_no")
                    Fname_txtbox.Text = dtreader.GetString("first_name")
                    Mname_txtbox.Text = dtreader.GetString("middle_name")
                    Lname_txtbox.Text = dtreader.GetString("last_name")
                    Age_txtbox.Text = dtreader.GetInt32("age")
                    Section_txtbox.Text = dtreader.GetString("section")
                    Gender_drpdown.Text = dtreader.GetString("gender")
                    Email_txtbox.Text = dtreader.GetString("email")
                    Program_drpdwn.SelectedValue = dtreader.GetInt32("program_id")
                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub



    Private Sub Student_Form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main_Dashboard.DisableStudentBTN()
        Main_Dashboard.Student_primary_id = "0"
        Fname_txtbox.Text = ""
        Student_id_txtbox.Text = ""
        Mname_txtbox.Text = ""
        Lname_txtbox.Text = ""
        Age_txtbox.Text = ""
        Section_txtbox.Text = ""
        Gender_drpdown.Text = ""
    End Sub

    Private Sub checkStudentID()
        If opendb() Then
            Dim query As String = "SELECT
                        `student_no`
                    FROM
                        `students`
                    WHERE
                        student_no LIKE '%" & Student_id_txtbox.Text & "%'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    studentIDcheck = dtreader.GetString("student_no")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub


    Private Sub Student_add_btn_Click(sender As Object, e As EventArgs) Handles Student_add_btn.Click
        checkStudentID()
        If Student_add_btn.Text = "Save" Then
            If (String.IsNullOrEmpty(studentIDcheck)) Then
                If opendb() Then
                    Dim query As String = "INSERT INTO `students`( `student_no`, `first_name`, `middle_name`, `last_name`, `age`, `gender`, `email`,`section`, `program_id`) VALUES 
                                   ('" & Student_id_txtbox.Text & "', '" & Fname_txtbox.Text & "', '" & Mname_txtbox.Text & "', '" & Lname_txtbox.Text & "', '" & Age_txtbox.Text & "', '" & Gender_drpdown.Text & "', '" & Email_txtbox.Text & "', '" & Section_txtbox.Text & "', '" & Program_drpdwn.SelectedValue & "')"
                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader

                    Try
                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Added.",
                        "Important Message")
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Me.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            Else
                MsgBox("Student ID is already Existed")
                studentIDcheck = vbNullString
            End If

        ElseIf Student_add_btn.Text = "Edit" Then

            If opendb() Then
                    Dim query As String = "UPDATE `students` SET `student_no`= '" & Student_id_txtbox.Text & "', `first_name`= '" & Fname_txtbox.Text & "', `middle_name`= '" & Mname_txtbox.Text & "', `last_name`= '" & Lname_txtbox.Text & "', `age`= '" & Age_txtbox.Text & "', `gender` = '" & Gender_drpdown.Text & "', `section`= '" & Section_txtbox.Text & "', `email`='" & Email_txtbox.Text & "' ,`program_id`= '" & Program_drpdwn.SelectedValue & "' WHERE id = '" & Main_Dashboard.Student_primary_id & "'"


                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader
                    Try

                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Edited.",
                        "Important Message")


                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Me.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If
            studentIDcheck = vbNullString


    End Sub

    Private Sub Student_id_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Student_id_txtbox.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Age_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Age_txtbox.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
End Class