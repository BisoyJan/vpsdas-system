﻿Imports MySql.Data.MySqlClient
Imports System.Net.Mail
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Public Class Disciplinary_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String
    Dim DisciplinaryCrystal As New CrystalReportAction

    Public primaryid_discplinary_action As String

    Public Student_Email As String

    Dim id_student As String
    Dim referral_id As String
    Dim FormatDate As Date
    Dim FormatTime As DateTime
    Dim Action As String
    Dim DatabasePath As String
    Dim ActionPath As String
    Dim SavePath As String
    Dim fullpath As String

    Dim Disciplinary_IDStudent As String
    Dim Disciplinary_StudentID As String
    Dim Disciplinary_StudentName As String
    Dim Disciplinary_Section As String
    Dim Disciplinary_Offense As String
    Dim Disciplinary_Committed_Date As String
    Dim Disciplinary_Committed_Time As DateTime
    Dim Disciplinary_Employee_name As String
    Dim Disciplinary_Counseling_Date As String
    Dim Disciplinary_Counseling_Time As DateTime
    Dim Disciplinary_Issued_Date As String
    Dim Disciplinary_VPSDAS_Name As String

    Dim DateChecking As String

    Dim Incident_time As String
    Dim Incident_date As String
    Dim Counselling_from As String
    Dim Counselling_time As String

    Private Sub Disciplinary_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If ActionPath = "" And SavePath = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        Else
            Datetime()
            Display_referral()
            If primaryid_discplinary_action > "0" And Disciplinary_save_btn.Text = "Edit" Then
                Edit_disciplinary()
            End If
        End If
    End Sub

    Public Sub Display_referral()
        Try
            If opendb() Then
                Dim query As String = "SELECT
                        referrals.id as ID,
                        students.student_no as StudentID,
                        students.first_name,
                        students.middle_name,
                        students.last_name,
                        students.age,
                        students.gender,
                        students.section,
                        students.email,
                        programs.program_name,
                        offenses.offense,
                        violations.code,
                        violations.violation,
  	                    employee_name,
                        referred
                    FROM
                        referrals
                    JOIN students ON referrals.student_id = students.id
                    JOIN programs ON students.program_id = programs.id
                    JOIN violations ON referrals.violation_id = violations.id
                    JOIN offenses ON violations.offenses_id = offenses.id
                    WHERE referrals.id = '" + Main_Dashboard.Referral_primary_id + "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                    While dtreader.Read

                        Disciplinary_studentid_txtbox.Text = dtreader.GetString("StudentID")
                        Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                        Gender_txtbox.Text = dtreader.GetString("gender")
                        Age_txtbox.Text = dtreader.GetInt32("age")
                        Section_txtbox.Text = dtreader.GetString("section")
                        Course_txtbox.Text = dtreader.GetString("program_name")
                        Student_Email = dtreader.GetString("email")
                        id_student = dtreader.GetInt32("id")

                        Staff_fullname_txtbox.Text = dtreader.GetString("employee_name")
                        VP_name_txtbox.Text = dtreader.GetString("referred")

                        Violation_code_txtbox.Text = dtreader.GetString("code")
                        Type_of_offense_txtbox.Text = dtreader.GetString("offense")
                        Violation_description_txtbox.Text = dtreader.GetString("violation")
                        referral_id = dtreader.GetInt32("ID")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub Edit_disciplinary()
        Try
            If opendb() Then
                Dim query As String = "SELECT
                        referrals.id AS ID,
                        students.student_no AS StudentID,
                        students.first_name,
                        students.middle_name,
                        students.last_name,
                        students.age,
                        students.gender,
                        students.section,
                        disciplinary_action.committed_date,
                        disciplinary_action.committed_time,
                        disciplinary_action.counselling_date,
                        disciplinary_action.counselling_time,
                        disciplinary_action.issual_date,
                        disciplinary_action.remarks,
                        programs.program_name,
                        offenses.offense,
                        violations.code,
                        violations.violation,
                        referrals.employee_name,
                        referrals.referred,
                        offenses.offense
                    FROM
                        disciplinary_action
                    JOIN referrals On disciplinary_action.referral_id = referrals.id
                    JOIN students ON referrals.student_id = students.id
                    JOIN programs ON students.program_id = programs.id
                    JOIN violations ON referrals.violation_id = violations.id
                    JOIN offenses ON violations.offenses_id = offenses.id
                    WHERE
                        disciplinary_action.id =  '" & primaryid_discplinary_action & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                    While dtreader.Read
                        Disciplinary_studentid_txtbox.Text = dtreader.GetInt32("StudentID")
                        Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                        Gender_txtbox.Text = dtreader.GetString("gender")
                        Age_txtbox.Text = dtreader.GetInt32("age")
                        Section_txtbox.Text = dtreader.GetString("section")
                        Course_txtbox.Text = dtreader.GetString("program_name")
                        Type_of_offense_txtbox.Text = dtreader.GetString("offense")
                        Violation_code_txtbox.Text = dtreader.GetString("code")
                        Violation_description_txtbox.Text = dtreader.GetString("violation")
                        Incident_date_picker.Value = dtreader.GetDateTime("committed_date")
                        Incident_time_picker.Value = Date.Today + DirectCast(dtreader("committed_time"), TimeSpan)
                        Counselling_from_datepicker.Value = dtreader.GetDateTime("counselling_date")
                        Counselling_time_picker.Value = Date.Today + DirectCast(dtreader("counselling_time"), TimeSpan)

                        Counselling_status_drpbox.Text = dtreader.GetString("remarks")
                        issued_date_picker.Value = dtreader.GetDateTime("issual_date")
                        Staff_fullname_txtbox.Text = dtreader.GetString("employee_name")
                        VP_name_txtbox.Text = dtreader.GetString("referred")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub DocumentPath()
        Action = "Action"

        If opendb() Then
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Action + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    ActionPath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CrystalReportDisciplinary()
        If opendb() Then
            Dim query As String = "SELECT
                disciplinary_action.id,
                students.id AS studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                students.email,
                offenses.offense,
                programs.abbreviation,
                referrals.referred,
                referrals.employee_name,
                disciplinary_action.committed_date,
                disciplinary_action.committed_time,
                disciplinary_action.counselling_date,
                disciplinary_action.counselling_time,
                disciplinary_action.issual_date
            FROM
                `disciplinary_action`
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                disciplinary_action.id = '" + primaryid_discplinary_action + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Disciplinary_IDStudent = dtreader.GetString("studentID")
                    Disciplinary_StudentID = dtreader.GetString("studentID")
                    Disciplinary_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Disciplinary_Section = dtreader.GetString("abbreviation") + " " + dtreader.GetString("section")
                    Disciplinary_Committed_Date = dtreader.GetDateTime("committed_date")
                    Disciplinary_Committed_Time = dtreader.GetString("committed_time")
                    Disciplinary_Employee_name = dtreader.GetString("employee_name")
                    Disciplinary_Counseling_Time = dtreader.GetString("counselling_time")
                    Disciplinary_Issued_Date = dtreader.GetDateTime("issual_date")

                    Student_Email = dtreader.GetString("email")

                    Disciplinary_Counseling_Date = dtreader.GetDateTime("counselling_date")
                    Disciplinary_VPSDAS_Name = dtreader.GetString("referred")
                    Disciplinary_Offense = dtreader.GetString("offense")

                    Main_Dashboard.StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        DisciplinaryCrystal.SetParameterValue("NameOfStudent", Disciplinary_StudentName)
        DisciplinaryCrystal.SetParameterValue("ProgramYearSection", Disciplinary_Section)
        DisciplinaryCrystal.SetParameterValue("TypeOfOffense", Disciplinary_Offense)
        DisciplinaryCrystal.SetParameterValue("DateCommitted", Disciplinary_Committed_Date)
        DisciplinaryCrystal.SetParameterValue("FacultyName", Disciplinary_Employee_name)
        DisciplinaryCrystal.SetParameterValue("DateCounselling", Disciplinary_Counseling_Date)
        DisciplinaryCrystal.SetParameterValue("TimeCounselling", Disciplinary_Committed_Time)
        DisciplinaryCrystal.SetParameterValue("VPSDASName", Disciplinary_VPSDAS_Name)
        DisciplinaryCrystal.SetParameterValue("DateIssued", Disciplinary_Issued_Date)

        CrystalReportViewer.CrystalReportViewer1.ReportSource = DisciplinaryCrystal

        sFileName = Main_Dashboard.StudentName + "_" + primaryid_discplinary_action
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = DisciplinaryCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            DisciplinaryCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        DatabasePath = fullpath.Replace("\", "\\")

        If opendb() Then
            Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Disciplinary_IDStudent & "','" & Action & "','" & DatabasePath & "')"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

    End Sub

    Public Sub ViewActionCrystal()
        DocumentPath()

        If opendb() Then
            Dim query As String = "SELECT
                disciplinary_action.id,
                students.id AS studentID,
                students.student_no,
                students.first_name,
                students.middle_name,
                students.last_name,
                students.section,
                offenses.offense,
                programs.abbreviation,
                referrals.referred,
                referrals.employee_name,
                disciplinary_action.committed_date,
                disciplinary_action.committed_time,
                disciplinary_action.counselling_date,
                disciplinary_action.counselling_time,
                disciplinary_action.issual_date
            FROM
                `disciplinary_action`
            JOIN referrals ON disciplinary_action.referral_id = referrals.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                disciplinary_action.id = '" + primaryid_discplinary_action + "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Disciplinary_IDStudent = dtreader.GetString("studentID")
                    Disciplinary_StudentID = dtreader.GetString("studentID")
                    Disciplinary_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
                    Disciplinary_Section = dtreader.GetString("abbreviation") + " " + dtreader.GetString("section")
                    Disciplinary_Committed_Date = dtreader.GetDateTime("committed_date")
                    Disciplinary_Committed_Time = dtreader.GetString("committed_time")
                    Disciplinary_Employee_name = dtreader.GetString("employee_name")
                    Disciplinary_Counseling_Time = dtreader.GetString("counselling_time")
                    Disciplinary_Issued_Date = dtreader.GetDateTime("issual_date")

                    Disciplinary_Counseling_Date = dtreader.GetDateTime("counselling_date")
                    Disciplinary_VPSDAS_Name = dtreader.GetString("referred")
                    Disciplinary_Offense = dtreader.GetString("offense")

                    Main_Dashboard.StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        DisciplinaryCrystal.SetParameterValue("NameOfStudent", Disciplinary_StudentName)
        DisciplinaryCrystal.SetParameterValue("ProgramYearSection", Disciplinary_Section)
        DisciplinaryCrystal.SetParameterValue("TypeOfOffense", Disciplinary_Offense)
        DisciplinaryCrystal.SetParameterValue("DateCommitted", Disciplinary_Committed_Date)
        DisciplinaryCrystal.SetParameterValue("FacultyName", Disciplinary_Employee_name)
        DisciplinaryCrystal.SetParameterValue("DateCounselling", Disciplinary_Counseling_Date)
        DisciplinaryCrystal.SetParameterValue("TimeCounselling", Disciplinary_Committed_Time)
        DisciplinaryCrystal.SetParameterValue("VPSDASName", Disciplinary_VPSDAS_Name)
        DisciplinaryCrystal.SetParameterValue("DateIssued", Disciplinary_Issued_Date)
        CrystalReportViewer.CrystalReportViewer1.ReportSource = DisciplinaryCrystal

        sFileName = Main_Dashboard.StudentName + "_" + primaryid_discplinary_action
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = DisciplinaryCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            DisciplinaryCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        CrystalReportViewer.ShowDialog()


    End Sub


    'Private Sub CreateDocument()
    '    If opendb() Then
    '        Dim query As String = "SELECT
    '            disciplinary_action.id,
    '            students.id AS studentID,
    '            students.student_no,
    '            students.first_name,
    '            students.middle_name,
    '            students.last_name,
    '            students.section,
    '            offenses.offense,
    '            referrals.referred,
    '            referrals.employee_name,
    '            disciplinary_action.committed_date,
    '            disciplinary_action.committed_time,
    '            disciplinary_action.counselling_date,
    '            disciplinary_action.counselling_time,
    '            disciplinary_action.issual_date
    '        FROM
    '            `disciplinary_action`
    '        JOIN referrals ON disciplinary_action.referral_id = referrals.id
    '        JOIN violations ON referrals.violation_id = violations.id
    '        JOIN students ON referrals.student_id = students.id
    '        JOIN programs ON students.program_id = programs.id
    '        JOIN offenses ON violations.offenses_id = offenses.id
    '        WHERE
    '            disciplinary_action.id = '" + primaryid_discplinary_action + "'"

    '        Dim cmd As New MySqlCommand(query, conn)
    '        Dim dtreader As MySqlDataReader

    '        Try
    '            dtreader = cmd.ExecuteReader

    '            While dtreader.Read

    '                Disciplinary_IDStudent = dtreader.GetString("studentID")
    '                Disciplinary_StudentID = dtreader.GetString("studentID")
    '                Disciplinary_StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")
    '                Disciplinary_Section = dtreader.GetString("section")
    '                Disciplinary_Committed_Date = dtreader.GetDateTime("committed_date")
    '                Disciplinary_Committed_Time = dtreader.GetString("committed_time")
    '                Disciplinary_Employee_name = dtreader.GetString("employee_name")
    '                Disciplinary_Counseling_Time = dtreader.GetString("counselling_time")
    '                Disciplinary_Issued_Date = dtreader.GetDateTime("issual_date")

    '                Disciplinary_Counseling_Date = dtreader.GetDateTime("counselling_date")
    '                Disciplinary_VPSDAS_Name = dtreader.GetString("referred")
    '                Disciplinary_Offense = dtreader.GetString("offense")

    '                Main_Dashboard.StudentName = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name")

    '            End While

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        Finally
    '            conn.Close()
    '        End Try
    '    Else
    '        MsgBox("NO database connections")
    '    End If

    '    sFileName = Main_Dashboard.StudentName + "_" + primaryid_discplinary_action
    '    fullpath = SavePath + "\" + sFileName
    '    wdApp = New Word.Application
    '    wdDocs = wdApp.Documents

    '    Dim wdDoc As Word.Document = wdDocs.Add(ActionPath)
    '    Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

    '    wdBooks("DateTimeOffense").Range.Text = Disciplinary_Committed_Date + " " + Disciplinary_Committed_Time.ToShortTimeString
    '    wdBooks("Offense").Range.Text = Disciplinary_Offense

    '    wdBooks("ClearanceDate").Range.Text = Disciplinary_Counseling_Date
    '    wdBooks("ClearanceTime").Range.Text = Disciplinary_Counseling_Time.ToShortTimeString
    '    wdBooks("IssuedDate").Range.Text = Disciplinary_Issued_Date

    '    wdBooks("StudentName").Range.Text = Disciplinary_StudentName
    '    wdBooks("Section").Range.Text = Disciplinary_Section

    '    wdBooks("Employee").Range.Text = Disciplinary_Employee_name
    '    wdBooks("VPSDAS").Range.Text = Disciplinary_VPSDAS_Name

    '    wdDoc.SaveAs(fullpath)
    '    ReleaseObject(wdBooks)
    '    wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
    '    ReleaseObject(wdDoc)
    '    wdApp.Quit()

    '    DatabasePath = fullpath.Replace("\", "\\")

    '    If opendb() Then
    '        Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Disciplinary_IDStudent & "','" & Action & "','" & DatabasePath & "')"

    '        Dim cmd As New MySqlCommand(query, conn)
    '        Dim dtreader As MySqlDataReader

    '        Try
    '            dtreader = cmd.ExecuteReader

    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        Finally
    '            conn.Close()
    '        End Try
    '    Else
    '        MsgBox("NO database connections")
    '    End If

    'End Sub

    'Private Sub ReleaseObject(ByVal obj As Object)
    '    Try
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
    '        obj = Nothing
    '    Catch ex As Exception
    '        obj = Nothing
    '    Finally
    '        GC.Collect()
    '    End Try
    'End Sub


    Private Sub EmailStudent()
        FormatDate = Counselling_from_datepicker.Value
        FormatTime = Counselling_time_picker.Value
        'CreateDocument()
        CrystalReportDisciplinary()

        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Dim attachment As System.Net.Mail.Attachment
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential("lnu.ovpsdas@gmail.com", "OVPSDAS-LNU")
            Smtp_Server.Port = 587
            Smtp_Server.EnableSsl = True
            Smtp_Server.Host = "smtp.gmail.com"

            attachment = New System.Net.Mail.Attachment($"{fullpath}")

            e_mail = New MailMessage()
            e_mail.From = New MailAddress("lnu.ovpsdas@gmail.com")
            e_mail.To.Add(Student_Email)
            e_mail.Subject = "Schedule for Counselling"
            e_mail.IsBodyHtml = False
            e_mail.Body = "Good Day " + Full_name_txtbox.Text + "" & vbCrLf & "This is auto Generated Email From OVPSDAS, your schedule for Counselling this """ + FormatDate.ToLongDateString() + " " + FormatTime.ToShortTimeString() + """ Please come to this day"
            e_mail.Attachments.Add(attachment)
            Smtp_Server.Send(e_mail)

            MessageBox.Show("Succesfully Email Send",
                    "Auto Email")
            Me.Dispose()
        Catch error_t As Exception
            MsgBox(error_t.ToString)
        End Try
    End Sub

    Private Sub SaveEdit()
        Dim issued_date As String = issued_date_picker.Value.ToString("yyyy-M-d")

        If Disciplinary_save_btn.Text = "Save" Then
            If opendb() Then

                Dim query As String = "INSERT INTO `disciplinary_action`(`referral_id`, `committed_date`, `committed_time`, `counselling_date`, `counselling_time`, `issual_date`, `remarks`) VALUES ('" & referral_id & "','" & Incident_date & "','" & Incident_time & "','" & Counselling_from & "','" & Counselling_time & "','" & issued_date & "','" & Counselling_status_drpbox.Text & "'); SELECT LAST_INSERT_ID()" 'I added double query in order to get the last primary key"

                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try
                    primaryid_discplinary_action = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    EmailStudent()
                    conn.Close()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                    Main_Dashboard.DisableReferralBTN()
                    Main_Dashboard.ShowDisciplinaryAction()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        ElseIf Disciplinary_save_btn.Text = "Edit" Then

            If opendb() Then

                Dim query As String = "UPDATE `disciplinary_action` SET `committed_date`='" & Incident_date & "',`committed_time`='" & Incident_time & "',`counselling_date`='" & Counselling_from & "',`counselling_time`='" & Incident_time & "',`issual_date`='" & issued_date & "',`remarks`='" & Counselling_status_drpbox.Text & "' WHERE id = '" & primaryid_discplinary_action & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edited.",
                    "Important Message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisableDisciplinarActionBTN()
                    EmailStudent()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Disciplinary_save_btn_Click(sender As Object, e As EventArgs) Handles Disciplinary_save_btn.Click
        Incident_time = Incident_time_picker.Value.ToString("hh:mm:ss")
        Incident_date = Incident_date_picker.Value.ToString("yyyy-M-d")
        Counselling_from = Counselling_from_datepicker.Value.ToString("yyyy-M-d")
        Counselling_time = Counselling_time_picker.Value.ToString("hh:mm:ss")
        If Disciplinary_save_btn.Text = "Save" Then

            Try
                If opendb() Then
                    Dim query As String = "SELECT
                        `counselling_date`
                    FROM
                        `disciplinary_action`
                    WHERE
                        counselling_date =  '" & Counselling_from_datepicker.Value.ToString("yyyy-M-d") & "'"

                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader

                    Try
                        dtreader = cmd.ExecuteReader

                        While dtreader.Read

                            DateChecking = dtreader.GetDateTime("counselling_date")

                        End While
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try

            If Counselling_from < Date.Now.ToString("yyyy-M-d") Then
                MsgBox("Date is Already past")
            ElseIf (String.IsNullOrEmpty(DateChecking)) Then
                SaveEdit()
            Else
                MsgBox("Date is Already Taken Please Select another Date")
                DateChecking = vbNullString
            End If

        ElseIf Disciplinary_save_btn.Text = "Edit" Then
            SaveEdit()
        End If

    End Sub

    Private Sub Datetime()
        Incident_time_picker.Format = DateTimePickerFormat.Time
        Incident_time_picker.ShowUpDown = True
        Counselling_time_picker.Format = DateTimePickerFormat.Time
        Counselling_time_picker.ShowUpDown = True
    End Sub

    Private Sub Disciplinary_form_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub


End Class