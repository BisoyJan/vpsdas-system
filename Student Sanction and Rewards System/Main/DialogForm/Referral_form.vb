﻿Imports MySql.Data.MySqlClient
Imports System.Net.Mail
Imports System.IO
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraRichEdit.API.Native
Imports Word = Microsoft.Office.Interop.Word
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class Referral_form
    Private wdApp As Word.Application
    Private wdDocs As Word.Documents
    Private sFileName As String
    Dim ReferralCrystal As New CrystalReportReferral

    Dim id_student As String
    Dim id_violation As String
    Dim dateofreferral As String

    Public referral_primary_id As String


    Dim Referral As String
    Dim DatabasePath As String
    Dim ReferralPath As String
    Dim SavePath As String
    Dim fullpath As String


    Dim Referral_ID As String
    Dim Referral_StudenID As String
    Dim Referral_WhatDate As String
    Dim Referral_Description As String
    Dim Referral_EmployeeName As String
    Dim Referral_Referred As String
    Dim Referral_StudentName As String

    Dim Referal_check As String
    Dim Student_number As String

    Private Sub Referral_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DocumentPath()
        If ReferralPath = "" And SavePath = "" Then
            MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set Please set it first", MessageBoxIcon.Error)
            Me.Close()
        Else
            If referral_primary_id > 0 Then
                Edit_referral()
            End If
        End If
    End Sub

    Private Sub Referral_limit_check()
        If opendb() Then
            Dim query As String = "SELECT
                COUNT(students.id)
            FROM
                referrals
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                students.student_no = '" + Student_number + "'"

            Dim cmd As New MySqlCommand(query, conn)

            Try

                Referal_check = cmd.ExecuteScalar().ToString()

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Referral_barrier()
        If (Referal_check = 3) Then
            Dim result As DialogResult = MessageBox.Show("This student has already reached sanctioned limit" + Environment.NewLine + "Do you want to add again?", "Confirmation Dialog", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If result = DialogResult.No Then
                MessageBox.Show("Cancelled", "Confirmation Dialog")
                Me.Dispose()
            ElseIf result = DialogResult.Yes Then

            End If
        Else

        End If
    End Sub

    Private Sub Referral_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Referral_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Referral_studentid_txtbox.Text.Trim(" ") = " " Then

                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Referral_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ""
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("id")
                                Student_number = dtreader.GetString("student_no")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                            Referral_limit_check()
                            Referral_barrier()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub Violationcode_txtbox_KeyDown(sender As Object, e As KeyEventArgs) Handles Violationcode_txtbox.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If opendb() Then
                Dim query As String = "SELECT violations.id as ID, offenses.offense as Category, violations.code as Code, violations.violation as Violation FROM violations JOIN offenses ON violations.offenses_id=offenses.id WHERE violations.code LIKE '%" & Violationcode_txtbox.Text & "' ORDER BY violations.ID DESC"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                    While dtreader.Read


                        Offensive_textbox.Text = dtreader.GetString("Category")
                        Description_txtbox.Text = dtreader.GetString("Violation")
                        id_violation = dtreader.GetInt32("ID")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Private Sub Edit_referral()
        If opendb() Then

            Dim query As String = "SELECT
                referrals.id AS ID,
                students.id as studID,
                students.student_no AS StudentID,
                students.first_name AS FirstName,
                students.middle_name AS MiddleName,
                students.last_name AS LastName,
                students.age AS Age,
                students.gender AS Gender,
                students.section AS Section,
                programs.abbreviation AS Program,
                violations.id AS ViolationID,
                violations.code AS ViolationCode,
                violations.violation AS Violations,
                offenses.offense,
                employee_name AS EmployeeName,
                referred AS Referred,
                DATE AS DATE
            FROM
                referrals
            JOIN students ON referrals.student_id = students.id
            JOIN programs ON students.program_id = programs.id
            JOIN violations ON referrals.violation_id = violations.id
            JOIN offenses ON violations.offenses_id = offenses.id
            WHERE
                referrals.id = '" & referral_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Full_name_txtbox.Text = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName") + ","
                    Gender_txtbox.Text = dtreader.GetString("Gender")
                    Age_txtbox.Text = dtreader.GetInt32("Age")
                    Section_txtbox.Text = dtreader.GetString("Section")
                    Course_txtbox.Text = dtreader.GetString("Program")
                    Referral_studentid_txtbox.Text = dtreader.GetString("StudentID")
                    Referred_txtbox.Text = dtreader.GetString("Referred")
                    Description_txtbox.Text = dtreader.GetString("Violations")
                    Violationcode_txtbox.Text = dtreader.GetString("ViolationCode")
                    Offensive_textbox.Text = dtreader.GetString("offense")
                    Referred_datepicker.Value = dtreader.GetDateTime("Date")
                    Employee_name_txtbox.Text = dtreader.GetString("EmployeeName")
                    Referred_txtbox.Text = dtreader.GetString("Referred")
                    referral_primary_id = dtreader.GetString("ID")

                    id_student = dtreader.GetString("studID")
                    id_violation = dtreader.GetString("ViolationID")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub DocumentPath()
        Referral = "Referral"

        If opendb() Then 'Locating Themeplate file path
            Dim query As String = "SELECT * FROM `themeplate_filepath` WHERE `name` = '" + Referral + "'"
            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader
                While dtreader.Read

                    ReferralPath = dtreader.GetString("path")
                    SavePath = dtreader.GetString("save_path")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
                Main_Dashboard.DisplayCase()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub CrystalReportReferral()
        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                    referrals.id AS ID,
                    students.id AS StudentID,
                    students.first_name AS FirstName,
                    students.middle_name AS MiddleName,
                    students.last_name AS LastName,
                    students.age AS Age,
                    students.gender AS Gender,
                    students.section AS Section,
                    programs.abbreviation AS Program,
                    violations.violation AS Violations,
                    employee_name AS EmployeeName,
                    referred AS Referred,
                    DATE AS DATE
                FROM
                    referrals
                JOIN students ON referrals.student_id = students.id
                JOIN programs ON students.program_id = programs.id
                JOIN violations ON referrals.violation_id = violations.id
                WHERE
	                referrals.id = '" & referral_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Referral_ID = dtreader.GetString("ID")
                    Referral_StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    Referral_Referred = dtreader.GetString("Referred")
                    Referral_EmployeeName = dtreader.GetString("EmployeeName")
                    Referral_Description = dtreader.GetString("Violations")
                    Referral_WhatDate = dtreader.GetDateTime("Date").ToLongDateString()
                    Referral_StudenID = dtreader.GetString("StudentID")
                    Main_Dashboard.StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If

        ReferralCrystal.SetParameterValue("NameOfStudent", Referral_StudentName)
        ReferralCrystal.SetParameterValue("ReferredTo", Referral_Referred)
        ReferralCrystal.SetParameterValue("Description", Referral_Description)
        ReferralCrystal.SetParameterValue("EmployeeName", Referral_EmployeeName)
        ReferralCrystal.SetParameterValue("Date", Referral_WhatDate)
        CrystalReportViewer.CrystalReportViewer1.ReportSource = ReferralCrystal
        'CrystalReportViewer.ShowDialog()

        sFileName = Main_Dashboard.StudentName + "_" + referral_primary_id
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = ReferralCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            ReferralCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        DatabasePath = fullpath.Replace("\", "\\")

        If SavePath = "" Then

        Else
            If opendb() Then
                Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Referral_StudenID & "','" & Referral & "','" & DatabasePath & "')"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If
        End If

    End Sub

    Public Sub ViewReferralCrystal()
        DocumentPath()

        If opendb() Then 'Getting all data for Themeplate
            Dim query As String = "SELECT
                    referrals.id AS ID,
                    students.id AS StudentID,
                    students.first_name AS FirstName,
                    students.middle_name AS MiddleName,
                    students.last_name AS LastName,
                    students.age AS Age,
                    students.gender AS Gender,
                    students.section AS Section,
                    programs.abbreviation AS Program,
                    violations.violation AS Violations,
                    employee_name AS EmployeeName,
                    referred AS Referred,
                    DATE AS DATE
                FROM
                    referrals
                JOIN students ON referrals.student_id = students.id
                JOIN programs ON students.program_id = programs.id
                JOIN violations ON referrals.violation_id = violations.id
                WHERE
	                referrals.id = '" & referral_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    Referral_ID = dtreader.GetString("ID")
                    Referral_StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
                    Referral_Referred = dtreader.GetString("Referred")
                    Referral_EmployeeName = dtreader.GetString("EmployeeName")
                    Referral_Description = dtreader.GetString("Violations")
                    Referral_WhatDate = dtreader.GetDateTime("Date").ToLongDateString()
                    Referral_StudenID = dtreader.GetString("StudentID")
                    Main_Dashboard.StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

                End While
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If


        ReferralCrystal.SetParameterValue("NameOfStudent", Referral_StudentName)
        ReferralCrystal.SetParameterValue("ReferredTo", Referral_Referred)
        ReferralCrystal.SetParameterValue("Description", Referral_Description)
        ReferralCrystal.SetParameterValue("EmployeeName", Referral_EmployeeName)
        ReferralCrystal.SetParameterValue("Date", Referral_WhatDate)
        CrystalReportViewer.CrystalReportViewer1.ReportSource = ReferralCrystal

        sFileName = Main_Dashboard.StudentName + "_" + referral_primary_id
        fullpath = SavePath + "\" + sFileName + ".pdf"

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
            CrDiskFileDestinationOptions.DiskFileName = fullpath
            CrExportOptions = ReferralCrystal.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            ReferralCrystal.Export()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        CrystalReportViewer.ShowDialog()

    End Sub

    'Private Sub CreateDocument()

    '    If opendb() Then 'Getting all data for Themeplate
    '        Dim query As String = "SELECT
    '                referrals.id AS ID,
    '                students.id AS StudentID,
    '                students.first_name AS FirstName,
    '                students.middle_name AS MiddleName,
    '                students.last_name AS LastName,
    '                students.age AS Age,
    '                students.gender AS Gender,
    '                students.section AS Section,
    '                programs.abbreviation AS Program,
    '                violations.violation AS Violations,
    '                employee_name AS EmployeeName,
    '                referred AS Referred,
    '                DATE AS DATE
    '            FROM
    '                referrals
    '            JOIN students ON referrals.student_id = students.id
    '            JOIN programs ON students.program_id = programs.id
    '            JOIN violations ON referrals.violation_id = violations.id
    '            WHERE
    '             referrals.id = '" & referral_primary_id & "'"

    '        Dim cmd As New MySqlCommand(query, conn)
    '        Dim dtreader As MySqlDataReader

    '        Try
    '            dtreader = cmd.ExecuteReader

    '            While dtreader.Read

    '                Referral_ID = dtreader.GetString("ID")
    '                Referral_StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")
    '                Referral_Referred = dtreader.GetString("Referred")
    '                Referral_EmployeeName = dtreader.GetString("EmployeeName")
    '                Referral_Description = dtreader.GetString("Violations")
    '                Referral_WhatDate = dtreader.GetDateTime("Date").ToLongDateString()
    '                Referral_StudenID = dtreader.GetString("StudentID")
    '                Main_Dashboard.StudentName = dtreader.GetString("FirstName") + " " + dtreader.GetString("MiddleName") + " " + dtreader.GetString("LastName")

    '            End While
    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        Finally
    '            conn.Close()
    '        End Try
    '    Else
    '        MsgBox("NO database connections")
    '    End If

    '    Try 'Merging Data from database to Document Themeplate
    '        sFileName = Main_Dashboard.StudentName + "_" + referral_primary_id
    '        fullpath = SavePath + "\" + sFileName
    '        wdApp = New Word.Application
    '        wdDocs = wdApp.Documents

    '        Dim wdDoc As Word.Document = wdDocs.Add(ReferralPath)
    '        Dim wdBooks As Word.Bookmarks = wdDoc.Bookmarks

    '        wdBooks("Date").Range.Text = Referral_WhatDate
    '        wdBooks("Description").Range.Text = Referral_Description
    '        wdBooks("EmployeeName").Range.Text = Referral_EmployeeName
    '        wdBooks("Referred").Range.Text = Referral_Referred
    '        wdBooks("StudentName").Range.Text = Referral_StudentName

    '        wdDoc.SaveAs(fullpath)
    '        ReleaseObject(wdBooks)
    '        wdDoc.Close(True) ' gin edit ko adi FALSE inin hiya dapat
    '        ReleaseObject(wdDoc)
    '        wdApp.Quit()


    '        DatabasePath = fullpath.Replace("\", "\\")
    '    Catch ex As Exception
    '        MsgBox("Themeplate File Path and Save Path" & vbCrLf & "Not Yet Set", MessageBoxIcon.Error)
    '    End Try

    '    If ReferralPath = "" Then

    '    Else
    '        If opendb() Then
    '            Dim query As String = "INSERT INTO `document_filepath`(`student_id`, `name`, `path`) VALUES ('" & Referral_StudenID & "','" & Referral & "','" & DatabasePath & "')"

    '            Dim cmd As New MySqlCommand(query, conn)
    '            Dim dtreader As MySqlDataReader

    '            Try
    '                dtreader = cmd.ExecuteReader

    '            Catch ex As Exception
    '                MsgBox(ex.Message)
    '            Finally
    '                conn.Close()
    '            End Try
    '        Else
    '            MsgBox("NO database connections")
    '        End If
    '    End If

    'End Sub

    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Referral_save_btn_Click(sender As Object, e As EventArgs) Handles Referral_save_btn.Click
        dateofreferral = Referred_datepicker.Value.ToString("yyyy-M-d")
        If Referral_save_btn.Text = "Save" Then
            Dim datepicker As String = Referred_datepicker.Value.ToString("yyyy-M-d")
            If opendb() Then

                Dim query As String = "INSERT INTO `referrals`(`student_id`, `violation_id`, `employee_name`, `referred`, `date`) VALUES ('" & id_student & "','" & id_violation & "', '" & Employee_name_txtbox.Text & "','" & Referred_txtbox.Text & "','" & dateofreferral & "'); SELECT LAST_INSERT_ID()" 'I added double query in order to get the last primary key
                Dim cmd As New MySqlCommand(query, conn)
                Dim cmd_result As Integer = CInt(cmd.ExecuteScalar()) 'I use this code in order to get the last primary key inserted from database

                Try

                    referral_primary_id = cmd_result
                    MessageBox.Show("Succesfuly Added.",
                    "Important Message")
                    Me.Close()

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    Main_Dashboard.DisableReferralBTN()
                    CrystalReportReferral()
                    'CreateDocument()
                End Try

            Else
                MsgBox("NO database connections")
            End If

        ElseIf Referral_save_btn.Text = "Edit" Then
            Dim datepicker As String = Referred_datepicker.Value.ToString("yyyy-M-d")
            If opendb() Then

                Dim query As String = "UPDATE `referrals` SET `student_id`='" & id_student & "',`violation_id`='" & id_violation & "',`employee_name`='" & Employee_name_txtbox.Text & "',`referred`='" & Referred_txtbox.Text & "', `date`='" & dateofreferral & "' WHERE `id` = '" & referral_primary_id & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("Succesfuly Edit.",
                    "Important Message")
                    Me.Close()
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                    CrystalReportReferral()
                    Main_Dashboard.DisableReferralBTN()
                End Try

            Else
                MsgBox("NO database connections")
            End If
        End If
    End Sub

    Private Sub Referral_form_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
        Main_Dashboard.DisableReferralBTN()
    End Sub
End Class