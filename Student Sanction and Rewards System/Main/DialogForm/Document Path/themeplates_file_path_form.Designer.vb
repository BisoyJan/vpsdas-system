﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class themeplates_file_path_form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(themeplates_file_path_form))
        Dim BorderEdges1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim StateProperties1 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties2 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties3 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties4 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim BorderEdges2 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Dim StateProperties5 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties6 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties7 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties8 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim BorderEdges3 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuShadowPanel1 = New Bunifu.UI.WinForms.BunifuShadowPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.save_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.path_title_drpbx = New Bunifu.UI.WinForms.BunifuDropdown()
        Me.Location_panel = New Bunifu.UI.WinForms.BunifuShadowPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuLabel3 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.save_path_locate_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.save_path_locate_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.themeplate_path_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.BunifuLabel2 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.themeplate_locate_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.BunifuShadowPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Location_panel.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.BunifuShadowPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Location_panel, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(691, 633)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'BunifuShadowPanel1
        '
        Me.BunifuShadowPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BunifuShadowPanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel1.BorderRadius = 10
        Me.BunifuShadowPanel1.BorderThickness = 0
        Me.BunifuShadowPanel1.Controls.Add(Me.TableLayoutPanel3)
        Me.BunifuShadowPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuShadowPanel1.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Gradient
        Me.BunifuShadowPanel1.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Horizontal
        Me.BunifuShadowPanel1.Location = New System.Drawing.Point(469, 10)
        Me.BunifuShadowPanel1.Margin = New System.Windows.Forms.Padding(10)
        Me.BunifuShadowPanel1.Name = "BunifuShadowPanel1"
        Me.BunifuShadowPanel1.PanelColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel1.PanelColor2 = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel1.ShadowColor = System.Drawing.Color.Transparent
        Me.BunifuShadowPanel1.ShadowDept = 2
        Me.BunifuShadowPanel1.ShadowDepth = 3
        Me.BunifuShadowPanel1.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded
        Me.BunifuShadowPanel1.ShadowTopLeftVisible = False
        Me.BunifuShadowPanel1.Size = New System.Drawing.Size(212, 613)
        Me.BunifuShadowPanel1.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat
        Me.BunifuShadowPanel1.TabIndex = 30
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.save_btn, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.path_title_drpbx, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 5
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.26382!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.37954!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.35665!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(212, 613)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'save_btn
        '
        Me.save_btn.AllowAnimations = True
        Me.save_btn.AllowMouseEffects = True
        Me.save_btn.AllowToggling = False
        Me.save_btn.AnimationSpeed = 200
        Me.save_btn.AutoGenerateColors = False
        Me.save_btn.AutoRoundBorders = False
        Me.save_btn.AutoSizeLeftIcon = True
        Me.save_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.save_btn.AutoSizeRightIcon = True
        Me.save_btn.BackColor = System.Drawing.Color.Transparent
        Me.save_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.BackgroundImage = CType(resources.GetObject("save_btn.BackgroundImage"), System.Drawing.Image)
        Me.save_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_btn.ButtonText = "Save"
        Me.save_btn.ButtonTextMarginLeft = 0
        Me.save_btn.ColorContrastOnClick = 45
        Me.save_btn.ColorContrastOnHover = 45
        Me.save_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges1.BottomLeft = True
        BorderEdges1.BottomRight = True
        BorderEdges1.TopLeft = True
        BorderEdges1.TopRight = True
        Me.save_btn.CustomizableEdges = BorderEdges1
        Me.save_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.save_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.save_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.save_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.save_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.save_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.save_btn.ForeColor = System.Drawing.Color.White
        Me.save_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.save_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.save_btn.IconLeftPadding = New System.Windows.Forms.Padding(30, 3, 3, 3)
        Me.save_btn.IconMarginLeft = 11
        Me.save_btn.IconPadding = 10
        Me.save_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.save_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.save_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.save_btn.IconSize = 25
        Me.save_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.IdleBorderRadius = 30
        Me.save_btn.IdleBorderThickness = 1
        Me.save_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.IdleIconLeftImage = CType(resources.GetObject("save_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.save_btn.IdleIconRightImage = Nothing
        Me.save_btn.IndicateFocus = True
        Me.save_btn.Location = New System.Drawing.Point(20, 282)
        Me.save_btn.Margin = New System.Windows.Forms.Padding(20)
        Me.save_btn.Name = "save_btn"
        Me.save_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.save_btn.OnDisabledState.BorderRadius = 30
        Me.save_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_btn.OnDisabledState.BorderThickness = 1
        Me.save_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.save_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.save_btn.OnDisabledState.IconLeftImage = Nothing
        Me.save_btn.OnDisabledState.IconRightImage = Nothing
        Me.save_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.onHoverState.BorderRadius = 30
        Me.save_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_btn.onHoverState.BorderThickness = 1
        Me.save_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.save_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.save_btn.onHoverState.IconLeftImage = Nothing
        Me.save_btn.onHoverState.IconRightImage = Nothing
        Me.save_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.OnIdleState.BorderRadius = 30
        Me.save_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_btn.OnIdleState.BorderThickness = 1
        Me.save_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.save_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("save_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.save_btn.OnIdleState.IconRightImage = Nothing
        Me.save_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_btn.OnPressedState.BorderRadius = 30
        Me.save_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_btn.OnPressedState.BorderThickness = 1
        Me.save_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.save_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.save_btn.OnPressedState.IconLeftImage = Nothing
        Me.save_btn.OnPressedState.IconRightImage = Nothing
        Me.save_btn.Size = New System.Drawing.Size(167, 52)
        Me.save_btn.TabIndex = 12
        Me.save_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.save_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.save_btn.TextMarginLeft = 0
        Me.save_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.save_btn.UseDefaultRadiusAndThickness = True
        '
        'path_title_drpbx
        '
        Me.path_title_drpbx.BackColor = System.Drawing.Color.Transparent
        Me.path_title_drpbx.BackgroundColor = System.Drawing.Color.White
        Me.path_title_drpbx.BorderColor = System.Drawing.Color.Silver
        Me.path_title_drpbx.BorderRadius = 1
        Me.path_title_drpbx.Color = System.Drawing.Color.Silver
        Me.path_title_drpbx.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down
        Me.path_title_drpbx.DisabledBackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.path_title_drpbx.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.path_title_drpbx.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.path_title_drpbx.DisabledForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.path_title_drpbx.DisabledIndicatorColor = System.Drawing.Color.DarkGray
        Me.path_title_drpbx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.path_title_drpbx.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.path_title_drpbx.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thin
        Me.path_title_drpbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.path_title_drpbx.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.path_title_drpbx.FillDropDown = True
        Me.path_title_drpbx.FillIndicator = False
        Me.path_title_drpbx.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.path_title_drpbx.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.path_title_drpbx.ForeColor = System.Drawing.Color.Black
        Me.path_title_drpbx.FormattingEnabled = True
        Me.path_title_drpbx.Icon = Nothing
        Me.path_title_drpbx.IndicatorAlignment = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.path_title_drpbx.IndicatorColor = System.Drawing.Color.Gray
        Me.path_title_drpbx.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right
        Me.path_title_drpbx.ItemBackColor = System.Drawing.Color.White
        Me.path_title_drpbx.ItemBorderColor = System.Drawing.Color.White
        Me.path_title_drpbx.ItemForeColor = System.Drawing.Color.Black
        Me.path_title_drpbx.ItemHeight = 26
        Me.path_title_drpbx.ItemHighLightColor = System.Drawing.Color.DodgerBlue
        Me.path_title_drpbx.ItemHighLightForeColor = System.Drawing.Color.White
        Me.path_title_drpbx.Items.AddRange(New Object() {"Action", "Referral", "Case", "Honors", "Cum Laude", "Outstanding Athlete", "MVP", "Kindly Act", "LeaderShip", "Cum Laude"})
        Me.path_title_drpbx.ItemTopMargin = 3
        Me.path_title_drpbx.Location = New System.Drawing.Point(3, 52)
        Me.path_title_drpbx.Name = "path_title_drpbx"
        Me.path_title_drpbx.Size = New System.Drawing.Size(206, 32)
        Me.path_title_drpbx.TabIndex = 40
        Me.path_title_drpbx.Text = Nothing
        Me.path_title_drpbx.TextAlignment = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left
        Me.path_title_drpbx.TextLeftMargin = 5
        '
        'Location_panel
        '
        Me.Location_panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Location_panel.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Location_panel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Location_panel.BorderRadius = 10
        Me.Location_panel.BorderThickness = 0
        Me.Location_panel.Controls.Add(Me.TableLayoutPanel2)
        Me.Location_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Location_panel.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Gradient
        Me.Location_panel.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Horizontal
        Me.Location_panel.Location = New System.Drawing.Point(10, 10)
        Me.Location_panel.Margin = New System.Windows.Forms.Padding(10)
        Me.Location_panel.Name = "Location_panel"
        Me.Location_panel.PanelColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Location_panel.PanelColor2 = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Location_panel.ShadowColor = System.Drawing.Color.Transparent
        Me.Location_panel.ShadowDept = 2
        Me.Location_panel.ShadowDepth = 3
        Me.Location_panel.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded
        Me.Location_panel.ShadowTopLeftVisible = False
        Me.Location_panel.Size = New System.Drawing.Size(439, 613)
        Me.Location_panel.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat
        Me.Location_panel.TabIndex = 29
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel8, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel6, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(439, 613)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 1
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.BunifuLabel3, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.save_path_locate_txtbox, 0, 1)
        Me.TableLayoutPanel8.Controls.Add(Me.TableLayoutPanel9, 0, 2)
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(0, 306)
        Me.TableLayoutPanel8.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 4
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.55949!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.44051!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(439, 307)
        Me.TableLayoutPanel8.TabIndex = 2
        '
        'BunifuLabel3
        '
        Me.BunifuLabel3.AllowParentOverrides = False
        Me.BunifuLabel3.AutoEllipsis = False
        Me.BunifuLabel3.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.Font = New System.Drawing.Font("Segoe UI Semibold", 12.25!, System.Drawing.FontStyle.Bold)
        Me.BunifuLabel3.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.BunifuLabel3.Location = New System.Drawing.Point(3, 3)
        Me.BunifuLabel3.Name = "BunifuLabel3"
        Me.BunifuLabel3.Padding = New System.Windows.Forms.Padding(10)
        Me.BunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel3.Size = New System.Drawing.Size(170, 41)
        Me.BunifuLabel3.TabIndex = 8
        Me.BunifuLabel3.Text = "Document Save Path"
        Me.BunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'save_path_locate_txtbox
        '
        Me.save_path_locate_txtbox.AcceptsReturn = False
        Me.save_path_locate_txtbox.AcceptsTab = False
        Me.save_path_locate_txtbox.AnimationSpeed = 200
        Me.save_path_locate_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.save_path_locate_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.save_path_locate_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.save_path_locate_txtbox.BackgroundImage = CType(resources.GetObject("save_path_locate_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.save_path_locate_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.save_path_locate_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.save_path_locate_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.save_path_locate_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.save_path_locate_txtbox.BorderRadius = 10
        Me.save_path_locate_txtbox.BorderThickness = 1
        Me.save_path_locate_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.save_path_locate_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.save_path_locate_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.save_path_locate_txtbox.DefaultText = ""
        Me.save_path_locate_txtbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.save_path_locate_txtbox.Enabled = False
        Me.save_path_locate_txtbox.FillColor = System.Drawing.Color.White
        Me.save_path_locate_txtbox.ForeColor = System.Drawing.Color.Black
        Me.save_path_locate_txtbox.HideSelection = True
        Me.save_path_locate_txtbox.IconLeft = Nothing
        Me.save_path_locate_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.save_path_locate_txtbox.IconPadding = 10
        Me.save_path_locate_txtbox.IconRight = Nothing
        Me.save_path_locate_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.save_path_locate_txtbox.Lines = New String(-1) {}
        Me.save_path_locate_txtbox.Location = New System.Drawing.Point(20, 67)
        Me.save_path_locate_txtbox.Margin = New System.Windows.Forms.Padding(20)
        Me.save_path_locate_txtbox.MaxLength = 32767
        Me.save_path_locate_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.save_path_locate_txtbox.Modified = False
        Me.save_path_locate_txtbox.Multiline = True
        Me.save_path_locate_txtbox.Name = "save_path_locate_txtbox"
        StateProperties1.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties1.FillColor = System.Drawing.Color.Empty
        StateProperties1.ForeColor = System.Drawing.Color.Empty
        StateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.save_path_locate_txtbox.OnActiveState = StateProperties1
        StateProperties2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties2.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties2.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.save_path_locate_txtbox.OnDisabledState = StateProperties2
        StateProperties3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties3.FillColor = System.Drawing.Color.Empty
        StateProperties3.ForeColor = System.Drawing.Color.Empty
        StateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.save_path_locate_txtbox.OnHoverState = StateProperties3
        StateProperties4.BorderColor = System.Drawing.Color.Silver
        StateProperties4.FillColor = System.Drawing.Color.White
        StateProperties4.ForeColor = System.Drawing.Color.Black
        StateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.save_path_locate_txtbox.OnIdleState = StateProperties4
        Me.save_path_locate_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.save_path_locate_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.save_path_locate_txtbox.PlaceholderForeColor = System.Drawing.Color.Gray
        Me.save_path_locate_txtbox.PlaceholderText = "File Path"
        Me.save_path_locate_txtbox.ReadOnly = False
        Me.save_path_locate_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.save_path_locate_txtbox.SelectedText = ""
        Me.save_path_locate_txtbox.SelectionLength = 0
        Me.save_path_locate_txtbox.SelectionStart = 0
        Me.save_path_locate_txtbox.ShortcutsEnabled = True
        Me.save_path_locate_txtbox.Size = New System.Drawing.Size(399, 119)
        Me.save_path_locate_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.save_path_locate_txtbox.TabIndex = 18
        Me.save_path_locate_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.save_path_locate_txtbox.TextMarginBottom = 0
        Me.save_path_locate_txtbox.TextMarginLeft = 3
        Me.save_path_locate_txtbox.TextMarginTop = 0
        Me.save_path_locate_txtbox.TextPlaceholder = "File Path"
        Me.save_path_locate_txtbox.UseSystemPasswordChar = False
        Me.save_path_locate_txtbox.WordWrap = True
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 3
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel9.Controls.Add(Me.save_path_locate_btn, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 206)
        Me.TableLayoutPanel9.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(439, 80)
        Me.TableLayoutPanel9.TabIndex = 19
        '
        'save_path_locate_btn
        '
        Me.save_path_locate_btn.AllowAnimations = True
        Me.save_path_locate_btn.AllowMouseEffects = True
        Me.save_path_locate_btn.AllowToggling = False
        Me.save_path_locate_btn.AnimationSpeed = 200
        Me.save_path_locate_btn.AutoGenerateColors = False
        Me.save_path_locate_btn.AutoRoundBorders = False
        Me.save_path_locate_btn.AutoSizeLeftIcon = True
        Me.save_path_locate_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.save_path_locate_btn.AutoSizeRightIcon = True
        Me.save_path_locate_btn.BackColor = System.Drawing.Color.Transparent
        Me.save_path_locate_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.BackgroundImage = CType(resources.GetObject("save_path_locate_btn.BackgroundImage"), System.Drawing.Image)
        Me.save_path_locate_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_path_locate_btn.ButtonText = "Locate"
        Me.save_path_locate_btn.ButtonTextMarginLeft = 0
        Me.save_path_locate_btn.ColorContrastOnClick = 45
        Me.save_path_locate_btn.ColorContrastOnHover = 45
        Me.save_path_locate_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges2.BottomLeft = True
        BorderEdges2.BottomRight = True
        BorderEdges2.TopLeft = True
        BorderEdges2.TopRight = True
        Me.save_path_locate_btn.CustomizableEdges = BorderEdges2
        Me.save_path_locate_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.save_path_locate_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.save_path_locate_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.save_path_locate_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.save_path_locate_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.save_path_locate_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.save_path_locate_btn.ForeColor = System.Drawing.Color.White
        Me.save_path_locate_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.save_path_locate_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.save_path_locate_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.save_path_locate_btn.IconMarginLeft = 11
        Me.save_path_locate_btn.IconPadding = 10
        Me.save_path_locate_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.save_path_locate_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.save_path_locate_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.save_path_locate_btn.IconSize = 25
        Me.save_path_locate_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.IdleBorderRadius = 30
        Me.save_path_locate_btn.IdleBorderThickness = 1
        Me.save_path_locate_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.IdleIconLeftImage = CType(resources.GetObject("save_path_locate_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.save_path_locate_btn.IdleIconRightImage = Nothing
        Me.save_path_locate_btn.IndicateFocus = True
        Me.save_path_locate_btn.Location = New System.Drawing.Point(156, 20)
        Me.save_path_locate_btn.Margin = New System.Windows.Forms.Padding(10, 20, 10, 3)
        Me.save_path_locate_btn.Name = "save_path_locate_btn"
        Me.save_path_locate_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.save_path_locate_btn.OnDisabledState.BorderRadius = 30
        Me.save_path_locate_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_path_locate_btn.OnDisabledState.BorderThickness = 1
        Me.save_path_locate_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.save_path_locate_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.save_path_locate_btn.OnDisabledState.IconLeftImage = Nothing
        Me.save_path_locate_btn.OnDisabledState.IconRightImage = Nothing
        Me.save_path_locate_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.onHoverState.BorderRadius = 30
        Me.save_path_locate_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_path_locate_btn.onHoverState.BorderThickness = 1
        Me.save_path_locate_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.save_path_locate_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.save_path_locate_btn.onHoverState.IconLeftImage = Nothing
        Me.save_path_locate_btn.onHoverState.IconRightImage = Nothing
        Me.save_path_locate_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.OnIdleState.BorderRadius = 30
        Me.save_path_locate_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_path_locate_btn.OnIdleState.BorderThickness = 1
        Me.save_path_locate_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.save_path_locate_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("save_path_locate_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.save_path_locate_btn.OnIdleState.IconRightImage = Nothing
        Me.save_path_locate_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.save_path_locate_btn.OnPressedState.BorderRadius = 30
        Me.save_path_locate_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.save_path_locate_btn.OnPressedState.BorderThickness = 1
        Me.save_path_locate_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.save_path_locate_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.save_path_locate_btn.OnPressedState.IconLeftImage = Nothing
        Me.save_path_locate_btn.OnPressedState.IconRightImage = Nothing
        Me.save_path_locate_btn.Size = New System.Drawing.Size(126, 52)
        Me.save_path_locate_btn.TabIndex = 10
        Me.save_path_locate_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.save_path_locate_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.save_path_locate_btn.TextMarginLeft = 0
        Me.save_path_locate_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.save_path_locate_btn.UseDefaultRadiusAndThickness = True
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.themeplate_path_txtbox, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.BunifuLabel2, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.TableLayoutPanel7, 0, 2)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel6.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 3
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.55949!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.44051!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(439, 306)
        Me.TableLayoutPanel6.TabIndex = 1
        '
        'themeplate_path_txtbox
        '
        Me.themeplate_path_txtbox.AcceptsReturn = False
        Me.themeplate_path_txtbox.AcceptsTab = False
        Me.themeplate_path_txtbox.AnimationSpeed = 200
        Me.themeplate_path_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.themeplate_path_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.themeplate_path_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.themeplate_path_txtbox.BackgroundImage = CType(resources.GetObject("themeplate_path_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.themeplate_path_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.themeplate_path_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.themeplate_path_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.themeplate_path_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.themeplate_path_txtbox.BorderRadius = 10
        Me.themeplate_path_txtbox.BorderThickness = 1
        Me.themeplate_path_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.themeplate_path_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.themeplate_path_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.themeplate_path_txtbox.DefaultText = ""
        Me.themeplate_path_txtbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.themeplate_path_txtbox.Enabled = False
        Me.themeplate_path_txtbox.FillColor = System.Drawing.Color.White
        Me.themeplate_path_txtbox.ForeColor = System.Drawing.Color.Black
        Me.themeplate_path_txtbox.HideSelection = True
        Me.themeplate_path_txtbox.IconLeft = Nothing
        Me.themeplate_path_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.themeplate_path_txtbox.IconPadding = 10
        Me.themeplate_path_txtbox.IconRight = Nothing
        Me.themeplate_path_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.themeplate_path_txtbox.Lines = New String(-1) {}
        Me.themeplate_path_txtbox.Location = New System.Drawing.Point(20, 67)
        Me.themeplate_path_txtbox.Margin = New System.Windows.Forms.Padding(20)
        Me.themeplate_path_txtbox.MaxLength = 32767
        Me.themeplate_path_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.themeplate_path_txtbox.Modified = False
        Me.themeplate_path_txtbox.Multiline = True
        Me.themeplate_path_txtbox.Name = "themeplate_path_txtbox"
        StateProperties5.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties5.FillColor = System.Drawing.Color.Empty
        StateProperties5.ForeColor = System.Drawing.Color.Empty
        StateProperties5.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.themeplate_path_txtbox.OnActiveState = StateProperties5
        StateProperties6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties6.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties6.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.themeplate_path_txtbox.OnDisabledState = StateProperties6
        StateProperties7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties7.FillColor = System.Drawing.Color.Empty
        StateProperties7.ForeColor = System.Drawing.Color.Empty
        StateProperties7.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.themeplate_path_txtbox.OnHoverState = StateProperties7
        StateProperties8.BorderColor = System.Drawing.Color.Silver
        StateProperties8.FillColor = System.Drawing.Color.White
        StateProperties8.ForeColor = System.Drawing.Color.Black
        StateProperties8.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.themeplate_path_txtbox.OnIdleState = StateProperties8
        Me.themeplate_path_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.themeplate_path_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.themeplate_path_txtbox.PlaceholderForeColor = System.Drawing.Color.Gray
        Me.themeplate_path_txtbox.PlaceholderText = "File Path"
        Me.themeplate_path_txtbox.ReadOnly = False
        Me.themeplate_path_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.themeplate_path_txtbox.SelectedText = ""
        Me.themeplate_path_txtbox.SelectionLength = 0
        Me.themeplate_path_txtbox.SelectionStart = 0
        Me.themeplate_path_txtbox.ShortcutsEnabled = True
        Me.themeplate_path_txtbox.Size = New System.Drawing.Size(399, 132)
        Me.themeplate_path_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.themeplate_path_txtbox.TabIndex = 20
        Me.themeplate_path_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.themeplate_path_txtbox.TextMarginBottom = 0
        Me.themeplate_path_txtbox.TextMarginLeft = 3
        Me.themeplate_path_txtbox.TextMarginTop = 0
        Me.themeplate_path_txtbox.TextPlaceholder = "File Path"
        Me.themeplate_path_txtbox.UseSystemPasswordChar = False
        Me.themeplate_path_txtbox.WordWrap = True
        '
        'BunifuLabel2
        '
        Me.BunifuLabel2.AllowParentOverrides = False
        Me.BunifuLabel2.AutoEllipsis = False
        Me.BunifuLabel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.Font = New System.Drawing.Font("Segoe UI Semibold", 12.25!, System.Drawing.FontStyle.Bold)
        Me.BunifuLabel2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.BunifuLabel2.Location = New System.Drawing.Point(3, 3)
        Me.BunifuLabel2.Name = "BunifuLabel2"
        Me.BunifuLabel2.Padding = New System.Windows.Forms.Padding(10)
        Me.BunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel2.Size = New System.Drawing.Size(172, 41)
        Me.BunifuLabel2.TabIndex = 8
        Me.BunifuLabel2.Text = "Themeplate File Path"
        Me.BunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 3
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel7.Controls.Add(Me.themeplate_locate_btn, 1, 0)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(0, 219)
        Me.TableLayoutPanel7.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(439, 87)
        Me.TableLayoutPanel7.TabIndex = 19
        '
        'themeplate_locate_btn
        '
        Me.themeplate_locate_btn.AllowAnimations = True
        Me.themeplate_locate_btn.AllowMouseEffects = True
        Me.themeplate_locate_btn.AllowToggling = False
        Me.themeplate_locate_btn.AnimationSpeed = 200
        Me.themeplate_locate_btn.AutoGenerateColors = False
        Me.themeplate_locate_btn.AutoRoundBorders = False
        Me.themeplate_locate_btn.AutoSizeLeftIcon = True
        Me.themeplate_locate_btn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.themeplate_locate_btn.AutoSizeRightIcon = True
        Me.themeplate_locate_btn.BackColor = System.Drawing.Color.Transparent
        Me.themeplate_locate_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.BackgroundImage = CType(resources.GetObject("themeplate_locate_btn.BackgroundImage"), System.Drawing.Image)
        Me.themeplate_locate_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.themeplate_locate_btn.ButtonText = "Locate"
        Me.themeplate_locate_btn.ButtonTextMarginLeft = 0
        Me.themeplate_locate_btn.ColorContrastOnClick = 45
        Me.themeplate_locate_btn.ColorContrastOnHover = 45
        Me.themeplate_locate_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges3.BottomLeft = True
        BorderEdges3.BottomRight = True
        BorderEdges3.TopLeft = True
        BorderEdges3.TopRight = True
        Me.themeplate_locate_btn.CustomizableEdges = BorderEdges3
        Me.themeplate_locate_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.themeplate_locate_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.themeplate_locate_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.themeplate_locate_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.themeplate_locate_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.themeplate_locate_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.themeplate_locate_btn.ForeColor = System.Drawing.Color.White
        Me.themeplate_locate_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.themeplate_locate_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.themeplate_locate_btn.IconLeftPadding = New System.Windows.Forms.Padding(20, 3, 3, 3)
        Me.themeplate_locate_btn.IconMarginLeft = 11
        Me.themeplate_locate_btn.IconPadding = 10
        Me.themeplate_locate_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.themeplate_locate_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.themeplate_locate_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.themeplate_locate_btn.IconSize = 25
        Me.themeplate_locate_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.IdleBorderRadius = 30
        Me.themeplate_locate_btn.IdleBorderThickness = 1
        Me.themeplate_locate_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.IdleIconLeftImage = CType(resources.GetObject("themeplate_locate_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.themeplate_locate_btn.IdleIconRightImage = Nothing
        Me.themeplate_locate_btn.IndicateFocus = True
        Me.themeplate_locate_btn.Location = New System.Drawing.Point(156, 20)
        Me.themeplate_locate_btn.Margin = New System.Windows.Forms.Padding(10, 20, 10, 3)
        Me.themeplate_locate_btn.Name = "themeplate_locate_btn"
        Me.themeplate_locate_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.themeplate_locate_btn.OnDisabledState.BorderRadius = 30
        Me.themeplate_locate_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.themeplate_locate_btn.OnDisabledState.BorderThickness = 1
        Me.themeplate_locate_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.themeplate_locate_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.themeplate_locate_btn.OnDisabledState.IconLeftImage = Nothing
        Me.themeplate_locate_btn.OnDisabledState.IconRightImage = Nothing
        Me.themeplate_locate_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.onHoverState.BorderRadius = 30
        Me.themeplate_locate_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.themeplate_locate_btn.onHoverState.BorderThickness = 1
        Me.themeplate_locate_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.themeplate_locate_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.themeplate_locate_btn.onHoverState.IconLeftImage = Nothing
        Me.themeplate_locate_btn.onHoverState.IconRightImage = Nothing
        Me.themeplate_locate_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.OnIdleState.BorderRadius = 30
        Me.themeplate_locate_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.themeplate_locate_btn.OnIdleState.BorderThickness = 1
        Me.themeplate_locate_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.themeplate_locate_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("themeplate_locate_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.themeplate_locate_btn.OnIdleState.IconRightImage = Nothing
        Me.themeplate_locate_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.themeplate_locate_btn.OnPressedState.BorderRadius = 30
        Me.themeplate_locate_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.themeplate_locate_btn.OnPressedState.BorderThickness = 1
        Me.themeplate_locate_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.themeplate_locate_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.themeplate_locate_btn.OnPressedState.IconLeftImage = Nothing
        Me.themeplate_locate_btn.OnPressedState.IconRightImage = Nothing
        Me.themeplate_locate_btn.Size = New System.Drawing.Size(126, 52)
        Me.themeplate_locate_btn.TabIndex = 10
        Me.themeplate_locate_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.themeplate_locate_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.themeplate_locate_btn.TextMarginLeft = 0
        Me.themeplate_locate_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.themeplate_locate_btn.UseDefaultRadiusAndThickness = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'themeplates_file_path_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 633)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "themeplates_file_path_form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.BunifuShadowPanel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Location_panel.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents BunifuShadowPanel1 As Bunifu.UI.WinForms.BunifuShadowPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents save_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents Location_panel As Bunifu.UI.WinForms.BunifuShadowPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel8 As TableLayoutPanel
    Friend WithEvents BunifuLabel3 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents save_path_locate_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents TableLayoutPanel9 As TableLayoutPanel
    Friend WithEvents save_path_locate_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
    Friend WithEvents BunifuLabel2 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents TableLayoutPanel7 As TableLayoutPanel
    Friend WithEvents themeplate_locate_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents themeplate_path_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents path_title_drpbx As Bunifu.UI.WinForms.BunifuDropdown
End Class
