﻿Imports MySql.Data.MySqlClient

Public Class themeplates_file_path_form
    Dim Honors As String

    Private Sub themeplates_file_path_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If save_btn.Text = "Save" Then

        Else
            load_themeplate()
        End If
    End Sub

    Private Sub disablebtn()
        File_path_dashboard.Document_delete_btn.Enabled = False
        File_path_dashboard.Edit_document_themeplate_path_btn.Enabled = False
        Me.Dispose()
    End Sub


    Private Sub load_themeplate()
        Try
            If opendb() Then
                Dim query As String = "SELECT
                    `id`,
                    `name`,
                    `path`,
                    `save_path`
                FROM
                    `themeplate_filepath`
                WHERE
                    id =  '" & Main_Dashboard.Theme_plate_pk & "'"

                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader

                    While dtreader.Read

                        path_title_drpbx.Text = dtreader.GetString("name")
                        themeplate_path_txtbox.Text = dtreader.GetString("path").Replace("\", "\\")
                        save_path_locate_txtbox.Text = dtreader.GetString("save_path").Replace("\", "\\")

                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()
                End Try
            Else
                MsgBox("NO database connections")
            End If

        Catch abc As Exception
            MsgBox(abc.Message)
        End Try
    End Sub

    Private Sub Honors_themeplate_locate_btn_Click(sender As Object, e As EventArgs) Handles themeplate_locate_btn.Click
        OpenFileDialog1.Filter = "word documents|*.docx"
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.Cancel Then

            themeplate_path_txtbox.Text = OpenFileDialog1.FileName.Replace("\", "\\")
        End If
    End Sub

    Private Sub Honors_save_path_locate_btn_Click(sender As Object, e As EventArgs) Handles save_path_locate_btn.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            save_path_locate_txtbox.Text = FolderBrowserDialog1.SelectedPath.Replace("\", "\\")
        End If
    End Sub

    Private Sub Honors_save_btn_Click(sender As Object, e As EventArgs) Handles save_btn.Click
        If save_btn.Text = "Save" Then
            If String.IsNullOrEmpty(themeplate_path_txtbox.Text) Or String.IsNullOrEmpty(themeplate_path_txtbox.Text) Or path_title_drpbx.Text = "" Then
                MessageBox.Show("themeplate path and save path need to be located")
            Else
                If opendb() Then
                    Dim query As String = "insert into `themeplate_filepath`(`name`, `path`, `save_path`) values ('" & path_title_drpbx.Text & "','" & themeplate_path_txtbox.Text & "', '" & save_path_locate_txtbox.Text & "')"
                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader

                    Try
                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("succesfuly added.",
                    "important message")
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()

                        disablebtn()
                    End Try
                Else
                    MsgBox("no database connections")
                End If
            End If
        Else
            If opendb() Then
                Dim query As String = "update `themeplate_filepath` set `path`='" & themeplate_path_txtbox.Text & "',`save_path`= '" & save_path_locate_txtbox.Text & "' where id = '" & Main_Dashboard.Theme_plate_pk & "'"
                Dim cmd As New MySqlCommand(query, conn)
                Dim dtreader As MySqlDataReader

                Try
                    dtreader = cmd.ExecuteReader
                    MessageBox.Show("succesfuly updated.",
                "important message")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    conn.Close()

                    disablebtn()
                End Try
            Else
                MsgBox("no database connections")
            End If
        End If

    End Sub

    Private Sub themeplates_file_path_form_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Dispose()
    End Sub
End Class