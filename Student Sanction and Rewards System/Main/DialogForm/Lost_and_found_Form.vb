﻿Imports MySql.Data.MySqlClient
Imports System.IO
Public Class Lost_and_found_Form
    Dim id_student As String
    Dim Retrieval_id_student As String
    Dim found As String
    Dim returned As String
    Dim surrender As String
    Dim Status As String
    Public Properties_primary_id As String

    Private Sub LostFound_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Properties_primary_id > 0 Then
            Edit_property()
        End If
    End Sub

    Private Sub Properties_studentid_txtbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Properties_studentid_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Properties_studentid_txtbox.Text.Trim(" ") = " " Then
                    Edit_property()
                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Properties_studentid_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Full_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                                Gender_txtbox.Text = dtreader.GetString("gender")
                                Age_txtbox.Text = dtreader.GetInt32("age")
                                Section_txtbox.Text = dtreader.GetString("section")
                                Course_txtbox.Text = dtreader.GetString("program_name")
                                id_student = dtreader.GetInt32("student_no")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub Retrieval_student_id_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Retrieval_student_id_txtbox.KeyPress
        Dim Ch As Char = e.KeyChar
        If Not Char.IsDigit(Ch) AndAlso Asc(Ch) <> 8 Then
            e.Handled = True
            Try
                If Retrieval_student_id_txtbox.Text.Trim(" ") = " " Then
                    Edit_property()
                Else
                    If opendb() Then
                        Dim query As String = "SELECT students.id , students.student_no, students.first_name, students.middle_name, students.last_name, students.age, students.gender, students.section, programs.abbreviation, programs.program_name FROM students JOIN programs ON students.program_id=programs.id WHERE students.student_no LIKE '%" & Retrieval_student_id_txtbox.Text & "%'"

                        Dim cmd As New MySqlCommand(query, conn)
                        Dim dtreader As MySqlDataReader

                        Try
                            dtreader = cmd.ExecuteReader

                            While dtreader.Read

                                Retrieval_student_name_txtbox.Text = dtreader.GetString("first_name") + " " + dtreader.GetString("middle_name") + " " + dtreader.GetString("last_name") + ","
                                Retrieval_gender_txtbox.Text = dtreader.GetString("gender")
                                Retrieval_age_txtbox.Text = dtreader.GetInt32("age")
                                Retrieval_section_txtbox.Text = dtreader.GetString("section")
                                Retrieval_course_txtbox.Text = dtreader.GetString("program_name")
                                Retrieval_id_student = dtreader.GetInt32("student_no")

                            End While
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally
                            conn.Close()
                        End Try
                    Else
                        MsgBox("NO database connections")
                    End If
                End If

            Catch abc As Exception
                MsgBox(abc.Message)
            End Try
        End If
    End Sub

    Private Sub Properties_save_btn_Click(sender As Object, e As EventArgs) Handles Properties_save_btn.Click

        If Properties_save_btn.Text = "Save" Then
            Dim ms As New MemoryStream
            Properties_image_picturebox.Image.Save(ms, Imaging.ImageFormat.Jpeg)

            Dim surrender As String = Surrendered_datepickers.Value.ToString("yyyy-M-d")
            Dim returned As String = Retrieved_datepicker.Value.ToString("yyyy-M-d")
            Dim found As String = Found_datepicker.Value.ToString("yyyy-M-d")

            If Returned_rdbtn.Checked Then
                If opendb() Then
                    Status = "Returned"

                    Dim query As String = "INSERT INTO `properties`( `student_id`, `retrieval_id`, `date_found`, `date_retrieved`, `date_surrendered`, `type`, `description`, `picture`, `remarks` ) VALUES ('" & id_student & "', '" & Retrieval_id_student & "', '" & found & "', '" & returned & "','" & surrender & "', '" & Item_type_txtbox.Text & "', '" & Description_txtbox.Text & "', @picture, '" & Status & "')"
                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader


                    cmd.Parameters.Add("@picture", MySqlDbType.Blob).Value = ms.ToArray()

                    Try
                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                        Me.Close()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Main_Dashboard.DisablePropertiesBTN()
                    End Try

                Else
                    MsgBox("NO database connections")
                End If
            Else
                If opendb() Then
                    Status = "Lost"

                    Dim query As String = "INSERT INTO `properties`( `student_id`, `retrieval_id`, `date_found`, `date_retrieved`, `date_surrendered`, `type`, `description`, `picture`, `remarks` ) VALUES ('" & id_student & "', '" & Retrieval_id_student & "', '" & found & "', '" & returned & "','" & surrender & "', '" & Item_type_txtbox.Text & "', '" & Description_txtbox.Text & "', @picture, '" & Status & "')"
                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader


                    cmd.Parameters.Add("@picture", MySqlDbType.Blob).Value = ms.ToArray()

                    Try
                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Added.",
                    "Important Message")

                        Me.Close()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Main_Dashboard.DisablePropertiesBTN()
                    End Try

                Else
                    MsgBox("NO database connections")
                End If
            End If

        ElseIf Properties_save_btn.Text = "Edit" Then

            Dim ms As New MemoryStream
            Properties_image_picturebox.Image.Save(ms, Imaging.ImageFormat.Jpeg)

            Dim surrender As String = Surrendered_datepickers.Value.ToString("yyyy-M-d")
            Dim returned As String = Retrieved_datepicker.Value.ToString("yyyy-M-d")
            Dim found As String = Found_datepicker.Value.ToString("yyyy-M-d")

            If Returned_rdbtn.Checked Then
                If opendb() Then
                    Status = "Returned"

                    Dim query As String = "UPDATE `properties` SET  `date_found` = '" & found & "', `date_retrieved` = '" & returned & "', `date_surrendered` = '" & surrender & "',`type`='" & Item_type_txtbox.Text & "',`description`='" & Description_txtbox.Text & "', `picture` = @picture, `remarks`='" & Status & "' WHERE id = '" & Properties_primary_id & "'"

                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader

                    cmd.Parameters.Add("@picture", MySqlDbType.Blob).Value = ms.ToArray()

                    Try

                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Edited.",
                        "Important Message")

                        Me.Close()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Main_Dashboard.DisablePropertiesBTN()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            Else
                If opendb() Then
                    Status = "Lost"
                    Dim query As String = "UPDATE `properties` SET  `date_found` = '" & found & "', `date_retrieved` = '" & returned & "', `date_surrendered` = '" & surrender & "',`type`='" & Item_type_txtbox.Text & "',`description`='" & Description_txtbox.Text & "', `picture` = @picture, `remarks`='" & Status & "' WHERE id = '" & Properties_primary_id & "'"

                    Dim cmd As New MySqlCommand(query, conn)
                    Dim dtreader As MySqlDataReader

                    cmd.Parameters.Add("@picture", MySqlDbType.Blob).Value = ms.ToArray()

                    Try

                        dtreader = cmd.ExecuteReader
                        MessageBox.Show("Succesfuly Edited.",
                        "Important Message")

                        Me.Close()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        conn.Close()
                        Main_Dashboard.DisablePropertiesBTN()
                    End Try
                Else
                    MsgBox("NO database connections")
                End If
            End If
        End If
    End Sub

    Private Sub Properties_insertpic_btn_Click(sender As Object, e As EventArgs) Handles Properties_insertpic_btn.Click
        Dim opf As New OpenFileDialog With {
            .Filter = "Choose Image(*.jpg;*.png)|*.jpg;*.png"
        }
        If opf.ShowDialog = DialogResult.OK Then
            Properties_image_picturebox.Image = Image.FromFile(opf.FileName)
        End If
    End Sub

    Private Sub Edit_property()
        If opendb() Then
            Dim query As String = "SELECT `student_id`, `retrieval_id`, `date_found`, `date_retrieved`, `date_surrendered`, `type`, `description`, `picture`, `remarks` FROM `properties` WHERE id = '" & Properties_primary_id & "'"

            Dim cmd As New MySqlCommand(query, conn)
            Dim dtreader As MySqlDataReader

            Dim img() As Byte

            Try
                dtreader = cmd.ExecuteReader

                While dtreader.Read

                    If dtreader.GetString("remarks") = "Lost" Then
                        Lost_rdbtn.Checked = True
                    Else
                        Returned_rdbtn.Checked = True
                    End If

                    Description_txtbox.Text = dtreader.GetString("description")
                    Item_type_txtbox.Text = dtreader.GetString("type")

                    Retrieval_student_id_txtbox.Text = dtreader.GetInt32("retrieval_id")
                    Properties_studentid_txtbox.Text = dtreader.GetInt32("student_id")

                    Surrendered_datepickers.Value = dtreader.GetDateTime("date_surrendered")
                    Retrieved_datepicker.Value = dtreader.GetDateTime("date_retrieved")
                    Found_datepicker.Value = dtreader.GetDateTime("date_found")

                    img = dtreader("picture")
                    Dim ms As New MemoryStream(img)

                    Properties_image_picturebox.Image = Image.FromStream(ms)

                End While

            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                conn.Close()
            End Try
        Else
            MsgBox("NO database connections")
        End If
    End Sub

    Private Sub Lost_and_found_Form_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
        Main_Dashboard.DisplayProperties()
        Main_Dashboard.DisablePropertiesBTN()
    End Sub
End Class