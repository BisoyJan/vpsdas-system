﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class design_panel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(design_panel))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuPanel1 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuPanel5 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.total_students_label = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.BunifuPanel2 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BunifuPanel3 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BunifuPanel4 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Rewards_chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Sanction_Chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuShadowPanel2 = New Bunifu.UI.WinForms.BunifuShadowPanel()
        Me.BunifuLabel7 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuPanel9 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.BunifuPanel8 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.BunifuPanel7 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BunifuPanel6 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.BunifuPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.BunifuPanel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel4.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.Rewards_chart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sanction_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.BunifuShadowPanel2.SuspendLayout()
        Me.BunifuPanel9.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel8.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel7.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BunifuPanel6.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel5, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BunifuPanel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1335, 784)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'BunifuPanel1
        '
        Me.BunifuPanel1.BackgroundColor = System.Drawing.Color.Transparent
        Me.BunifuPanel1.BackgroundImage = CType(resources.GetObject("BunifuPanel1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel1.BorderColor = System.Drawing.Color.Transparent
        Me.BunifuPanel1.BorderRadius = 3
        Me.BunifuPanel1.BorderThickness = 1
        Me.BunifuPanel1.Controls.Add(Me.TableLayoutPanel4)
        Me.BunifuPanel1.Controls.Add(Me.TableLayoutPanel3)
        Me.BunifuPanel1.Controls.Add(Me.TableLayoutPanel2)
        Me.BunifuPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel1.Location = New System.Drawing.Point(0, 48)
        Me.BunifuPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.BunifuPanel1.Name = "BunifuPanel1"
        Me.BunifuPanel1.ShowBorders = True
        Me.BunifuPanel1.Size = New System.Drawing.Size(1335, 736)
        Me.BunifuPanel1.TabIndex = 2
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuPanel4, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuPanel3, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuPanel2, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuPanel5, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1335, 211)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'BunifuPanel5
        '
        Me.BunifuPanel5.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel5.BackgroundImage = CType(resources.GetObject("BunifuPanel5.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel5.BorderRadius = 10
        Me.BunifuPanel5.BorderThickness = 1
        Me.BunifuPanel5.Controls.Add(Me.Label10)
        Me.BunifuPanel5.Controls.Add(Me.PictureBox1)
        Me.BunifuPanel5.Controls.Add(Me.total_students_label)
        Me.BunifuPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel5.Location = New System.Drawing.Point(15, 15)
        Me.BunifuPanel5.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel5.Name = "BunifuPanel5"
        Me.BunifuPanel5.ShowBorders = True
        Me.BunifuPanel5.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel5.TabIndex = 0
        '
        'total_students_label
        '
        Me.total_students_label.AutoSize = True
        Me.total_students_label.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.total_students_label.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.total_students_label.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.total_students_label.Location = New System.Drawing.Point(20, 15)
        Me.total_students_label.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.total_students_label.Name = "total_students_label"
        Me.total_students_label.Size = New System.Drawing.Size(92, 54)
        Me.total_students_label.TabIndex = 2
        Me.total_students_label.Text = "120"
        Me.total_students_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label10.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label10.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label10.Location = New System.Drawing.Point(45, 133)
        Me.Label10.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(213, 31)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Number of Students"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuPanel2
        '
        Me.BunifuPanel2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel2.BackgroundImage = CType(resources.GetObject("BunifuPanel2.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel2.BorderRadius = 10
        Me.BunifuPanel2.BorderThickness = 1
        Me.BunifuPanel2.Controls.Add(Me.Label1)
        Me.BunifuPanel2.Controls.Add(Me.PictureBox2)
        Me.BunifuPanel2.Controls.Add(Me.Label2)
        Me.BunifuPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel2.Location = New System.Drawing.Point(348, 15)
        Me.BunifuPanel2.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel2.Name = "BunifuPanel2"
        Me.BunifuPanel2.ShowBorders = True
        Me.BunifuPanel2.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label2.Location = New System.Drawing.Point(20, 15)
        Me.Label2.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 54)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "120"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label1.Location = New System.Drawing.Point(54, 133)
        Me.Label1.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 31)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Counsell Reports"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'BunifuPanel3
        '
        Me.BunifuPanel3.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel3.BackgroundImage = CType(resources.GetObject("BunifuPanel3.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel3.BorderRadius = 10
        Me.BunifuPanel3.BorderThickness = 1
        Me.BunifuPanel3.Controls.Add(Me.Label3)
        Me.BunifuPanel3.Controls.Add(Me.PictureBox3)
        Me.BunifuPanel3.Controls.Add(Me.Label4)
        Me.BunifuPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel3.Location = New System.Drawing.Point(681, 15)
        Me.BunifuPanel3.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel3.Name = "BunifuPanel3"
        Me.BunifuPanel3.ShowBorders = True
        Me.BunifuPanel3.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel3.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label4.Location = New System.Drawing.Point(20, 15)
        Me.Label4.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 54)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "120"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 3
        Me.PictureBox3.TabStop = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label3.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label3.Location = New System.Drawing.Point(45, 133)
        Me.Label3.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(158, 31)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Action Reports"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuPanel4
        '
        Me.BunifuPanel4.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel4.BackgroundImage = CType(resources.GetObject("BunifuPanel4.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel4.BorderRadius = 10
        Me.BunifuPanel4.BorderThickness = 1
        Me.BunifuPanel4.Controls.Add(Me.Label5)
        Me.BunifuPanel4.Controls.Add(Me.PictureBox4)
        Me.BunifuPanel4.Controls.Add(Me.Label6)
        Me.BunifuPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel4.Location = New System.Drawing.Point(1014, 15)
        Me.BunifuPanel4.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel4.Name = "BunifuPanel4"
        Me.BunifuPanel4.ShowBorders = True
        Me.BunifuPanel4.Size = New System.Drawing.Size(306, 181)
        Me.BunifuPanel4.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label6.Location = New System.Drawing.Point(20, 15)
        Me.Label6.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(92, 54)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "120"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(217, 15)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 3
        Me.PictureBox4.TabStop = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label5.Location = New System.Drawing.Point(45, 133)
        Me.Label5.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(189, 31)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Referred Students"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Sanction_Chart, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Rewards_chart, 0, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 422)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1335, 314)
        Me.TableLayoutPanel4.TabIndex = 5
        '
        'Rewards_chart
        '
        ChartArea2.Name = "ChartArea1"
        Me.Rewards_chart.ChartAreas.Add(ChartArea2)
        Me.Rewards_chart.Dock = System.Windows.Forms.DockStyle.Fill
        Legend2.Name = "Legend1"
        Me.Rewards_chart.Legends.Add(Legend2)
        Me.Rewards_chart.Location = New System.Drawing.Point(15, 15)
        Me.Rewards_chart.Margin = New System.Windows.Forms.Padding(15)
        Me.Rewards_chart.Name = "Rewards_chart"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Rewards Chart"
        Me.Rewards_chart.Series.Add(Series2)
        Me.Rewards_chart.Size = New System.Drawing.Size(637, 284)
        Me.Rewards_chart.TabIndex = 1
        Me.Rewards_chart.Text = "Rewards Chart"
        '
        'Sanction_Chart
        '
        ChartArea1.Name = "ChartArea1"
        Me.Sanction_Chart.ChartAreas.Add(ChartArea1)
        Me.Sanction_Chart.Dock = System.Windows.Forms.DockStyle.Fill
        Legend1.Name = "Legend1"
        Me.Sanction_Chart.Legends.Add(Legend1)
        Me.Sanction_Chart.Location = New System.Drawing.Point(682, 15)
        Me.Sanction_Chart.Margin = New System.Windows.Forms.Padding(15)
        Me.Sanction_Chart.Name = "Sanction_Chart"
        Series1.ChartArea = "ChartArea1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie
        Series1.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Series1.Legend = "Legend1"
        Series1.Name = "Sanction_Chart"
        Me.Sanction_Chart.Series.Add(Series1)
        Me.Sanction_Chart.Size = New System.Drawing.Size(638, 284)
        Me.Sanction_Chart.TabIndex = 0
        Me.Sanction_Chart.Text = "Sanctioning Chart"
        Title1.BackColor = System.Drawing.Color.White
        Title1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Title1.Name = "Title1"
        Title1.Text = "Sanctioning"
        Me.Sanction_Chart.Titles.Add(Title1)
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.Controls.Add(Me.BunifuLabel7, 0, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel5.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(1335, 48)
        Me.TableLayoutPanel5.TabIndex = 1
        '
        'BunifuShadowPanel2
        '
        Me.BunifuShadowPanel2.AutoSize = True
        Me.BunifuShadowPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BunifuShadowPanel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel2.BorderRadius = 10
        Me.BunifuShadowPanel2.BorderThickness = 0
        Me.BunifuShadowPanel2.Controls.Add(Me.TableLayoutPanel1)
        Me.BunifuShadowPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuShadowPanel2.FillStyle = Bunifu.UI.WinForms.BunifuShadowPanel.FillStyles.Gradient
        Me.BunifuShadowPanel2.GradientMode = Bunifu.UI.WinForms.BunifuShadowPanel.GradientModes.Horizontal
        Me.BunifuShadowPanel2.Location = New System.Drawing.Point(0, 0)
        Me.BunifuShadowPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.BunifuShadowPanel2.Name = "BunifuShadowPanel2"
        Me.BunifuShadowPanel2.PanelColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel2.PanelColor2 = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuShadowPanel2.ShadowColor = System.Drawing.Color.Gray
        Me.BunifuShadowPanel2.ShadowDept = 2
        Me.BunifuShadowPanel2.ShadowDepth = 3
        Me.BunifuShadowPanel2.ShadowStyle = Bunifu.UI.WinForms.BunifuShadowPanel.ShadowStyles.Surrounded
        Me.BunifuShadowPanel2.ShadowTopLeftVisible = False
        Me.BunifuShadowPanel2.Size = New System.Drawing.Size(1335, 784)
        Me.BunifuShadowPanel2.Style = Bunifu.UI.WinForms.BunifuShadowPanel.BevelStyles.Flat
        Me.BunifuShadowPanel2.TabIndex = 50
        '
        'BunifuLabel7
        '
        Me.BunifuLabel7.AllowParentOverrides = False
        Me.BunifuLabel7.AutoEllipsis = False
        Me.BunifuLabel7.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel7.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel7.Font = New System.Drawing.Font("Segoe UI Semibold", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel7.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.BunifuLabel7.Location = New System.Drawing.Point(55, 5)
        Me.BunifuLabel7.Margin = New System.Windows.Forms.Padding(50, 0, 3, 3)
        Me.BunifuLabel7.Name = "BunifuLabel7"
        Me.BunifuLabel7.Padding = New System.Windows.Forms.Padding(10)
        Me.BunifuLabel7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel7.Size = New System.Drawing.Size(135, 35)
        Me.BunifuLabel7.TabIndex = 6
        Me.BunifuLabel7.Text = "Dashboard"
        Me.BunifuLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel7.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuPanel9
        '
        Me.BunifuPanel9.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel9.BackgroundImage = CType(resources.GetObject("BunifuPanel9.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel9.BorderRadius = 10
        Me.BunifuPanel9.BorderThickness = 1
        Me.BunifuPanel9.Controls.Add(Me.Label14)
        Me.BunifuPanel9.Controls.Add(Me.PictureBox8)
        Me.BunifuPanel9.Controls.Add(Me.Label15)
        Me.BunifuPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel9.Location = New System.Drawing.Point(15, 15)
        Me.BunifuPanel9.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel9.Name = "BunifuPanel9"
        Me.BunifuPanel9.ShowBorders = True
        Me.BunifuPanel9.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel9.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label15.Location = New System.Drawing.Point(20, 15)
        Me.Label15.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(92, 54)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "120"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 3
        Me.PictureBox8.TabStop = False
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label14.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label14.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label14.Location = New System.Drawing.Point(54, 133)
        Me.Label14.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(171, 31)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Handover Items"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuPanel8
        '
        Me.BunifuPanel8.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel8.BackgroundImage = CType(resources.GetObject("BunifuPanel8.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel8.BorderRadius = 10
        Me.BunifuPanel8.BorderThickness = 1
        Me.BunifuPanel8.Controls.Add(Me.Label12)
        Me.BunifuPanel8.Controls.Add(Me.PictureBox7)
        Me.BunifuPanel8.Controls.Add(Me.Label13)
        Me.BunifuPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel8.Location = New System.Drawing.Point(348, 15)
        Me.BunifuPanel8.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel8.Name = "BunifuPanel8"
        Me.BunifuPanel8.ShowBorders = True
        Me.BunifuPanel8.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel8.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label13.Location = New System.Drawing.Point(20, 15)
        Me.Label13.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(92, 54)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "120"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox7.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 3
        Me.PictureBox7.TabStop = False
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label12.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label12.Location = New System.Drawing.Point(53, 133)
        Me.Label12.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(165, 31)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Honor Rewards"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuPanel7
        '
        Me.BunifuPanel7.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel7.BackgroundImage = CType(resources.GetObject("BunifuPanel7.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel7.BorderRadius = 10
        Me.BunifuPanel7.BorderThickness = 1
        Me.BunifuPanel7.Controls.Add(Me.Label9)
        Me.BunifuPanel7.Controls.Add(Me.PictureBox6)
        Me.BunifuPanel7.Controls.Add(Me.Label11)
        Me.BunifuPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel7.Location = New System.Drawing.Point(681, 15)
        Me.BunifuPanel7.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel7.Name = "BunifuPanel7"
        Me.BunifuPanel7.ShowBorders = True
        Me.BunifuPanel7.Size = New System.Drawing.Size(303, 181)
        Me.BunifuPanel7.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label11.Location = New System.Drawing.Point(20, 15)
        Me.Label11.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(92, 54)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "120"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(214, 15)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 3
        Me.PictureBox6.TabStop = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label9.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label9.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label9.Location = New System.Drawing.Point(45, 133)
        Me.Label9.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(215, 31)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Cum Laude Rewards"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BunifuPanel6
        '
        Me.BunifuPanel6.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel6.BackgroundImage = CType(resources.GetObject("BunifuPanel6.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.BunifuPanel6.BorderRadius = 10
        Me.BunifuPanel6.BorderThickness = 1
        Me.BunifuPanel6.Controls.Add(Me.Label7)
        Me.BunifuPanel6.Controls.Add(Me.PictureBox5)
        Me.BunifuPanel6.Controls.Add(Me.Label8)
        Me.BunifuPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuPanel6.Location = New System.Drawing.Point(1014, 15)
        Me.BunifuPanel6.Margin = New System.Windows.Forms.Padding(15)
        Me.BunifuPanel6.Name = "BunifuPanel6"
        Me.BunifuPanel6.ShowBorders = True
        Me.BunifuPanel6.Size = New System.Drawing.Size(306, 181)
        Me.BunifuPanel6.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label8.Location = New System.Drawing.Point(20, 15)
        Me.Label8.Margin = New System.Windows.Forms.Padding(20, 15, 15, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 54)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "120"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(217, 15)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(60, 60)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 3
        Me.PictureBox5.TabStop = False
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semilight", 17.0!)
        Me.Label7.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label7.Location = New System.Drawing.Point(45, 133)
        Me.Label7.Margin = New System.Windows.Forms.Padding(45, 25, 3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(210, 31)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Leadership Rewards"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.BunifuPanel6, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.BunifuPanel7, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.BunifuPanel8, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.BunifuPanel9, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 211)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 211.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1335, 211)
        Me.TableLayoutPanel3.TabIndex = 4
        '
        'design_panel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1335, 784)
        Me.Controls.Add(Me.BunifuShadowPanel2)
        Me.DoubleBuffered = True
        Me.Name = "design_panel"
        Me.Text = "design_panel"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.BunifuPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.BunifuPanel5.ResumeLayout(False)
        Me.BunifuPanel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel2.ResumeLayout(False)
        Me.BunifuPanel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel3.ResumeLayout(False)
        Me.BunifuPanel3.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel4.ResumeLayout(False)
        Me.BunifuPanel4.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.Rewards_chart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sanction_Chart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.BunifuShadowPanel2.ResumeLayout(False)
        Me.BunifuPanel9.ResumeLayout(False)
        Me.BunifuPanel9.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel8.ResumeLayout(False)
        Me.BunifuPanel8.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel7.ResumeLayout(False)
        Me.BunifuPanel7.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BunifuPanel6.ResumeLayout(False)
        Me.BunifuPanel6.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents BunifuLabel7 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuPanel1 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents Sanction_Chart As DataVisualization.Charting.Chart
    Friend WithEvents Rewards_chart As DataVisualization.Charting.Chart
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents BunifuPanel6 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label7 As Label
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents Label8 As Label
    Friend WithEvents BunifuPanel7 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label9 As Label
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents BunifuPanel8 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label12 As Label
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents Label13 As Label
    Friend WithEvents BunifuPanel9 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label14 As Label
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents Label15 As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents BunifuPanel4 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label5 As Label
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Label6 As Label
    Friend WithEvents BunifuPanel3 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label3 As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label4 As Label
    Friend WithEvents BunifuPanel2 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label2 As Label
    Friend WithEvents BunifuPanel5 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents Label10 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents total_students_label As Label
    Friend WithEvents BunifuShadowPanel2 As Bunifu.UI.WinForms.BunifuShadowPanel
End Class
