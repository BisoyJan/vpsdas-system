﻿Imports System.Windows.Forms.DataVisualization.Charting
Public Class design_panel

    Private Sub DisplayPieChartSanctions()
        Dim SeriesSY As Series = Sanction_Chart.Series("Sanction_Chart")
        SeriesSY.ChartType = SeriesChartType.Pie
        SeriesSY.Palette = ChartColorPalette.Pastel
        SeriesSY.Name = "Sanctioning Chart"
        SeriesSY.Points.AddXY("Referral", 20)
        SeriesSY.Points.AddXY("Action", 15)
        SeriesSY.Points.AddXY("Counselling", 10)
    End Sub

    Private Sub DisplayBarChartRewards()
        Dim SeriesSY As Series = Rewards_chart.Series("Rewards Chart")
        SeriesSY.ChartType = SeriesChartType.Column
        SeriesSY.Palette = ChartColorPalette.Pastel
        SeriesSY.Name = "Rewards Chart"
        SeriesSY.Points.AddXY("Referral", 20)
        SeriesSY.Points.AddXY("Action", 15)
        SeriesSY.Points.AddXY("Counselling", 10)
    End Sub

    'Private Sub charts()
    '    Dim yValues As Integer() = {CDbl(str_total.Text), CDbl(Totalmale.Text), CDbl(Totalfemale.Text), CDbl(Totalengagedact.Text), CDbl(Totalmaterial.Text), CDbl(Totalpattaholder.Text)} ' Getting values from Textboxes 

    '    Dim xValues As String() = {"Total Beneficiary", "Male", "Female", "Engaged in same activity before", "Material Support received", "Patta holder"} ' Headings
    '    Dim seriesName As String = Nothing

    '    ' Note 1 : Clear chart before fill - VERY IMPORTANT and can generate exception if you are generating
    '    '          multiple charts in loop and have not included below lines !
    '    ' Note 2 : Chrt variable here is the Name of your Chart 
    '    Chart1.Series.Clear()
    '    Chart1.Titles.Clear()

    '    ' Give unique Series Name
    '    seriesName = "ChartInv"
    '    Chart1.Series.Add(seriesName)

    '    ' Bind X and Y values
    '    Chart1.Series(seriesName).Points.DataBindXY(xValues, yValues)

    '    ' Define Custom Chart Colors
    '    Chart1.Series(seriesName).Points(0).Color = Color.MediumSeaGreen
    '    Chart1.Series(seriesName).Points(1).Color = Color.PaleGreen
    '    Chart1.Series(seriesName).Points(2).Color = Color.LawnGreen
    '    Chart1.Series(seriesName).Points(3).Color = Color.Blue
    '    Chart1.Series(seriesName).Points(4).Color = Color.Red
    '    Chart1.Series(seriesName).Points(5).Color = Color.Yellow

    '    ' Define Chart Type
    '    Chart1.Series(seriesName).ChartType = DataVisualization.Charting.SeriesChartType.Pie
    '    Chart1.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True

    '    ' If you want to show Chart Legends
    '    Chart1.Legends(0).Enabled = True

    '    For Each dp As DataPoint In Chart1.Series(seriesName).Points
    '        If dp.YValues(0) = 0 Then
    '            dp.LabelForeColor = Color.Transparent
    '        End If
    '    Next

    '    Chart1.Series("ChartInv").IsValueShownAsLabel = True
    'End Sub

    Private Sub design_panel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DisplayPieChartSanctions()
        DisplayBarChartRewards()
    End Sub
End Class

