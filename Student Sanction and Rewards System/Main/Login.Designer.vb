﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Dim StateProperties1 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties2 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties3 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties4 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties5 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties6 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties7 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim StateProperties8 As Bunifu.UI.WinForms.BunifuTextBox.StateProperties = New Bunifu.UI.WinForms.BunifuTextBox.StateProperties()
        Dim BorderEdges1 As Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges = New Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges()
        Me.BunifuPanel1 = New Bunifu.UI.WinForms.BunifuPanel()
        Me.BunifuLabel2 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuPictureBox1 = New Bunifu.UI.WinForms.BunifuPictureBox()
        Me.BunifuLabel1 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuPictureBox2 = New Bunifu.UI.WinForms.BunifuPictureBox()
        Me.Password_show_chckbox = New Bunifu.UI.WinForms.BunifuCheckBox()
        Me.BunifuLabel9 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Password_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.BunifuLabel4 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.BunifuLabel3 = New Bunifu.UI.WinForms.BunifuLabel()
        Me.Email_txtbox = New Bunifu.UI.WinForms.BunifuTextBox()
        Me.Login_btn = New Bunifu.UI.WinForms.BunifuButton.BunifuButton()
        Me.BunifuPanel1.SuspendLayout()
        CType(Me.BunifuPictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BunifuPictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BunifuPanel1
        '
        Me.BunifuPanel1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuPanel1.BackgroundImage = CType(resources.GetObject("BunifuPanel1.BackgroundImage"), System.Drawing.Image)
        Me.BunifuPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.BunifuPanel1.BorderRadius = 0
        Me.BunifuPanel1.BorderThickness = 1
        Me.BunifuPanel1.Controls.Add(Me.BunifuLabel2)
        Me.BunifuPanel1.Controls.Add(Me.BunifuPictureBox1)
        Me.BunifuPanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.BunifuPanel1.Location = New System.Drawing.Point(0, 0)
        Me.BunifuPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.BunifuPanel1.Name = "BunifuPanel1"
        Me.BunifuPanel1.ShowBorders = True
        Me.BunifuPanel1.Size = New System.Drawing.Size(568, 748)
        Me.BunifuPanel1.TabIndex = 1
        '
        'BunifuLabel2
        '
        Me.BunifuLabel2.AllowParentOverrides = False
        Me.BunifuLabel2.AutoEllipsis = False
        Me.BunifuLabel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel2.Font = New System.Drawing.Font("Malgun Gothic", 17.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel2.ForeColor = System.Drawing.Color.White
        Me.BunifuLabel2.Location = New System.Drawing.Point(52, 458)
        Me.BunifuLabel2.Name = "BunifuLabel2"
        Me.BunifuLabel2.Padding = New System.Windows.Forms.Padding(10)
        Me.BunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel2.Size = New System.Drawing.Size(459, 98)
        Me.BunifuLabel2.TabIndex = 9
        Me.BunifuLabel2.Text = "Office of the Vice President for Student " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Development and Auxiliary Services"
        Me.BunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.BunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuPictureBox1
        '
        Me.BunifuPictureBox1.AllowFocused = False
        Me.BunifuPictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BunifuPictureBox1.AutoSizeHeight = True
        Me.BunifuPictureBox1.BorderRadius = 153
        Me.BunifuPictureBox1.Image = CType(resources.GetObject("BunifuPictureBox1.Image"), System.Drawing.Image)
        Me.BunifuPictureBox1.IsCircle = True
        Me.BunifuPictureBox1.Location = New System.Drawing.Point(128, 79)
        Me.BunifuPictureBox1.Name = "BunifuPictureBox1"
        Me.BunifuPictureBox1.Size = New System.Drawing.Size(307, 307)
        Me.BunifuPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuPictureBox1.TabIndex = 8
        Me.BunifuPictureBox1.TabStop = False
        Me.BunifuPictureBox1.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Circle
        '
        'BunifuLabel1
        '
        Me.BunifuLabel1.AllowParentOverrides = False
        Me.BunifuLabel1.AutoEllipsis = False
        Me.BunifuLabel1.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel1.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel1.Font = New System.Drawing.Font("Malgun Gothic", 17.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel1.ForeColor = System.Drawing.Color.Gray
        Me.BunifuLabel1.Location = New System.Drawing.Point(756, 79)
        Me.BunifuLabel1.Name = "BunifuLabel1"
        Me.BunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel1.Size = New System.Drawing.Size(101, 31)
        Me.BunifuLabel1.TabIndex = 10
        Me.BunifuLabel1.Text = "Welcome"
        Me.BunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopCenter
        Me.BunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuPictureBox2
        '
        Me.BunifuPictureBox2.AllowFocused = False
        Me.BunifuPictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BunifuPictureBox2.AutoSizeHeight = True
        Me.BunifuPictureBox2.BorderRadius = 189
        Me.BunifuPictureBox2.Image = CType(resources.GetObject("BunifuPictureBox2.Image"), System.Drawing.Image)
        Me.BunifuPictureBox2.IsCircle = True
        Me.BunifuPictureBox2.Location = New System.Drawing.Point(617, 43)
        Me.BunifuPictureBox2.Name = "BunifuPictureBox2"
        Me.BunifuPictureBox2.Size = New System.Drawing.Size(379, 379)
        Me.BunifuPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BunifuPictureBox2.TabIndex = 9
        Me.BunifuPictureBox2.TabStop = False
        Me.BunifuPictureBox2.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Circle
        '
        'Password_show_chckbox
        '
        Me.Password_show_chckbox.AllowBindingControlAnimation = True
        Me.Password_show_chckbox.AllowBindingControlColorChanges = False
        Me.Password_show_chckbox.AllowBindingControlLocation = True
        Me.Password_show_chckbox.AllowCheckBoxAnimation = False
        Me.Password_show_chckbox.AllowCheckmarkAnimation = True
        Me.Password_show_chckbox.AllowOnHoverStates = True
        Me.Password_show_chckbox.AutoCheck = True
        Me.Password_show_chckbox.BackColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.BackgroundImage = CType(resources.GetObject("Password_show_chckbox.BackgroundImage"), System.Drawing.Image)
        Me.Password_show_chckbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Password_show_chckbox.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right
        Me.Password_show_chckbox.BorderRadius = 12
        Me.Password_show_chckbox.Checked = False
        Me.Password_show_chckbox.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked
        Me.Password_show_chckbox.Cursor = System.Windows.Forms.Cursors.Default
        Me.Password_show_chckbox.CustomCheckmarkImage = Nothing
        Me.Password_show_chckbox.Location = New System.Drawing.Point(685, 541)
        Me.Password_show_chckbox.Margin = New System.Windows.Forms.Padding(30, 10, 3, 3)
        Me.Password_show_chckbox.MinimumSize = New System.Drawing.Size(17, 17)
        Me.Password_show_chckbox.Name = "Password_show_chckbox"
        Me.Password_show_chckbox.OnCheck.BorderColor = System.Drawing.Color.DodgerBlue
        Me.Password_show_chckbox.OnCheck.BorderRadius = 12
        Me.Password_show_chckbox.OnCheck.BorderThickness = 2
        Me.Password_show_chckbox.OnCheck.CheckBoxColor = System.Drawing.Color.DodgerBlue
        Me.Password_show_chckbox.OnCheck.CheckmarkColor = System.Drawing.Color.White
        Me.Password_show_chckbox.OnCheck.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnDisable.BorderColor = System.Drawing.Color.LightGray
        Me.Password_show_chckbox.OnDisable.BorderRadius = 12
        Me.Password_show_chckbox.OnDisable.BorderThickness = 2
        Me.Password_show_chckbox.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray
        Me.Password_show_chckbox.OnDisable.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverChecked.BorderRadius = 12
        Me.Password_show_chckbox.OnHoverChecked.BorderThickness = 2
        Me.Password_show_chckbox.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White
        Me.Password_show_chckbox.OnHoverChecked.CheckmarkThickness = 2
        Me.Password_show_chckbox.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_show_chckbox.OnHoverUnchecked.BorderRadius = 12
        Me.Password_show_chckbox.OnHoverUnchecked.BorderThickness = 1
        Me.Password_show_chckbox.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.OnUncheck.BorderColor = System.Drawing.Color.DarkGray
        Me.Password_show_chckbox.OnUncheck.BorderRadius = 12
        Me.Password_show_chckbox.OnUncheck.BorderThickness = 1
        Me.Password_show_chckbox.OnUncheck.CheckBoxColor = System.Drawing.Color.Transparent
        Me.Password_show_chckbox.Size = New System.Drawing.Size(21, 21)
        Me.Password_show_chckbox.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu
        Me.Password_show_chckbox.TabIndex = 61
        Me.Password_show_chckbox.ThreeState = False
        Me.Password_show_chckbox.ToolTipText = Nothing
        '
        'BunifuLabel9
        '
        Me.BunifuLabel9.AllowParentOverrides = False
        Me.BunifuLabel9.AutoEllipsis = False
        Me.BunifuLabel9.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel9.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel9.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BunifuLabel9.ForeColor = System.Drawing.Color.Gray
        Me.BunifuLabel9.Location = New System.Drawing.Point(714, 541)
        Me.BunifuLabel9.Margin = New System.Windows.Forms.Padding(5, 5, 0, 0)
        Me.BunifuLabel9.Name = "BunifuLabel9"
        Me.BunifuLabel9.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
        Me.BunifuLabel9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel9.Size = New System.Drawing.Size(95, 22)
        Me.BunifuLabel9.TabIndex = 62
        Me.BunifuLabel9.Text = "Show Password"
        Me.BunifuLabel9.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel9.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Password_txtbox
        '
        Me.Password_txtbox.AcceptsReturn = False
        Me.Password_txtbox.AcceptsTab = False
        Me.Password_txtbox.AnimationSpeed = 200
        Me.Password_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Password_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Password_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Password_txtbox.BackgroundImage = CType(resources.GetObject("Password_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Password_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Password_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Password_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Password_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Password_txtbox.BorderRadius = 10
        Me.Password_txtbox.BorderThickness = 1
        Me.Password_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Password_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 12.25!)
        Me.Password_txtbox.DefaultText = ""
        Me.Password_txtbox.FillColor = System.Drawing.Color.White
        Me.Password_txtbox.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Password_txtbox.HideSelection = True
        Me.Password_txtbox.IconLeft = Nothing
        Me.Password_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.IconPadding = 10
        Me.Password_txtbox.IconRight = Nothing
        Me.Password_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Password_txtbox.Lines = New String(-1) {}
        Me.Password_txtbox.Location = New System.Drawing.Point(685, 494)
        Me.Password_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Password_txtbox.MaxLength = 32767
        Me.Password_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Password_txtbox.Modified = False
        Me.Password_txtbox.Multiline = False
        Me.Password_txtbox.Name = "Password_txtbox"
        StateProperties1.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties1.FillColor = System.Drawing.Color.Empty
        StateProperties1.ForeColor = System.Drawing.Color.Empty
        StateProperties1.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnActiveState = StateProperties1
        StateProperties2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties2.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties2.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Password_txtbox.OnDisabledState = StateProperties2
        StateProperties3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties3.FillColor = System.Drawing.Color.Empty
        StateProperties3.ForeColor = System.Drawing.Color.Empty
        StateProperties3.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnHoverState = StateProperties3
        StateProperties4.BorderColor = System.Drawing.Color.Silver
        StateProperties4.FillColor = System.Drawing.Color.White
        StateProperties4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        StateProperties4.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Password_txtbox.OnIdleState = StateProperties4
        Me.Password_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Password_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Password_txtbox.PlaceholderText = ""
        Me.Password_txtbox.ReadOnly = False
        Me.Password_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Password_txtbox.SelectedText = ""
        Me.Password_txtbox.SelectionLength = 0
        Me.Password_txtbox.SelectionStart = 0
        Me.Password_txtbox.ShortcutsEnabled = True
        Me.Password_txtbox.Size = New System.Drawing.Size(242, 42)
        Me.Password_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Password_txtbox.TabIndex = 59
        Me.Password_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Password_txtbox.TextMarginBottom = 0
        Me.Password_txtbox.TextMarginLeft = 3
        Me.Password_txtbox.TextMarginTop = 0
        Me.Password_txtbox.TextPlaceholder = ""
        Me.Password_txtbox.UseSystemPasswordChar = False
        Me.Password_txtbox.WordWrap = True
        '
        'BunifuLabel4
        '
        Me.BunifuLabel4.AllowParentOverrides = False
        Me.BunifuLabel4.AutoEllipsis = False
        Me.BunifuLabel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel4.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel4.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel4.ForeColor = System.Drawing.Color.Gray
        Me.BunifuLabel4.Location = New System.Drawing.Point(670, 463)
        Me.BunifuLabel4.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel4.Name = "BunifuLabel4"
        Me.BunifuLabel4.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel4.Size = New System.Drawing.Size(73, 31)
        Me.BunifuLabel4.TabIndex = 58
        Me.BunifuLabel4.Text = "Password:"
        Me.BunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'BunifuLabel3
        '
        Me.BunifuLabel3.AllowParentOverrides = False
        Me.BunifuLabel3.AutoEllipsis = False
        Me.BunifuLabel3.Cursor = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.CursorType = System.Windows.Forms.Cursors.Default
        Me.BunifuLabel3.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BunifuLabel3.ForeColor = System.Drawing.Color.Gray
        Me.BunifuLabel3.Location = New System.Drawing.Point(670, 373)
        Me.BunifuLabel3.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.BunifuLabel3.Name = "BunifuLabel3"
        Me.BunifuLabel3.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.BunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BunifuLabel3.Size = New System.Drawing.Size(42, 31)
        Me.BunifuLabel3.TabIndex = 57
        Me.BunifuLabel3.Text = "Email:"
        Me.BunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.BunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.[Default]
        '
        'Email_txtbox
        '
        Me.Email_txtbox.AcceptsReturn = False
        Me.Email_txtbox.AcceptsTab = False
        Me.Email_txtbox.AnimationSpeed = 200
        Me.Email_txtbox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.Email_txtbox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.Email_txtbox.BackColor = System.Drawing.Color.Transparent
        Me.Email_txtbox.BackgroundImage = CType(resources.GetObject("Email_txtbox.BackgroundImage"), System.Drawing.Image)
        Me.Email_txtbox.BorderColorActive = System.Drawing.Color.DodgerBlue
        Me.Email_txtbox.BorderColorDisabled = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Email_txtbox.BorderColorHover = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Email_txtbox.BorderColorIdle = System.Drawing.Color.Silver
        Me.Email_txtbox.BorderRadius = 10
        Me.Email_txtbox.BorderThickness = 1
        Me.Email_txtbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Email_txtbox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.DefaultFont = New System.Drawing.Font("Segoe UI", 12.25!)
        Me.Email_txtbox.DefaultText = ""
        Me.Email_txtbox.FillColor = System.Drawing.Color.White
        Me.Email_txtbox.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Email_txtbox.HideSelection = True
        Me.Email_txtbox.IconLeft = Nothing
        Me.Email_txtbox.IconLeftCursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.IconPadding = 10
        Me.Email_txtbox.IconRight = Nothing
        Me.Email_txtbox.IconRightCursor = System.Windows.Forms.Cursors.IBeam
        Me.Email_txtbox.Lines = New String(-1) {}
        Me.Email_txtbox.Location = New System.Drawing.Point(685, 404)
        Me.Email_txtbox.Margin = New System.Windows.Forms.Padding(30, 0, 10, 0)
        Me.Email_txtbox.MaxLength = 32767
        Me.Email_txtbox.MinimumSize = New System.Drawing.Size(1, 1)
        Me.Email_txtbox.Modified = False
        Me.Email_txtbox.Multiline = False
        Me.Email_txtbox.Name = "Email_txtbox"
        StateProperties5.BorderColor = System.Drawing.Color.DodgerBlue
        StateProperties5.FillColor = System.Drawing.Color.Empty
        StateProperties5.ForeColor = System.Drawing.Color.Empty
        StateProperties5.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnActiveState = StateProperties5
        StateProperties6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        StateProperties6.FillColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        StateProperties6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        StateProperties6.PlaceholderForeColor = System.Drawing.Color.DarkGray
        Me.Email_txtbox.OnDisabledState = StateProperties6
        StateProperties7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(255, Byte), Integer))
        StateProperties7.FillColor = System.Drawing.Color.Empty
        StateProperties7.ForeColor = System.Drawing.Color.Empty
        StateProperties7.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnHoverState = StateProperties7
        StateProperties8.BorderColor = System.Drawing.Color.Silver
        StateProperties8.FillColor = System.Drawing.Color.White
        StateProperties8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        StateProperties8.PlaceholderForeColor = System.Drawing.Color.Empty
        Me.Email_txtbox.OnIdleState = StateProperties8
        Me.Email_txtbox.Padding = New System.Windows.Forms.Padding(3)
        Me.Email_txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Email_txtbox.PlaceholderForeColor = System.Drawing.Color.Silver
        Me.Email_txtbox.PlaceholderText = ""
        Me.Email_txtbox.ReadOnly = False
        Me.Email_txtbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Email_txtbox.SelectedText = ""
        Me.Email_txtbox.SelectionLength = 0
        Me.Email_txtbox.SelectionStart = 0
        Me.Email_txtbox.ShortcutsEnabled = True
        Me.Email_txtbox.Size = New System.Drawing.Size(242, 42)
        Me.Email_txtbox.Style = Bunifu.UI.WinForms.BunifuTextBox._Style.Bunifu
        Me.Email_txtbox.TabIndex = 56
        Me.Email_txtbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Email_txtbox.TextMarginBottom = 0
        Me.Email_txtbox.TextMarginLeft = 3
        Me.Email_txtbox.TextMarginTop = 0
        Me.Email_txtbox.TextPlaceholder = ""
        Me.Email_txtbox.UseSystemPasswordChar = False
        Me.Email_txtbox.WordWrap = True
        '
        'Login_btn
        '
        Me.Login_btn.AllowAnimations = True
        Me.Login_btn.AllowMouseEffects = True
        Me.Login_btn.AllowToggling = False
        Me.Login_btn.AnimationSpeed = 200
        Me.Login_btn.AutoGenerateColors = False
        Me.Login_btn.AutoRoundBorders = False
        Me.Login_btn.AutoSizeLeftIcon = True
        Me.Login_btn.AutoSizeRightIcon = True
        Me.Login_btn.BackColor = System.Drawing.Color.Transparent
        Me.Login_btn.BackColor1 = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Login_btn.BackgroundImage = CType(resources.GetObject("Login_btn.BackgroundImage"), System.Drawing.Image)
        Me.Login_btn.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Login_btn.ButtonText = "Login"
        Me.Login_btn.ButtonTextMarginLeft = 0
        Me.Login_btn.ColorContrastOnClick = 45
        Me.Login_btn.ColorContrastOnHover = 45
        Me.Login_btn.Cursor = System.Windows.Forms.Cursors.Default
        BorderEdges1.BottomLeft = True
        BorderEdges1.BottomRight = True
        BorderEdges1.TopLeft = True
        BorderEdges1.TopRight = True
        Me.Login_btn.CustomizableEdges = BorderEdges1
        Me.Login_btn.DialogResult = System.Windows.Forms.DialogResult.None
        Me.Login_btn.DisabledBorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Login_btn.DisabledFillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Login_btn.DisabledForecolor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Login_btn.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Idle
        Me.Login_btn.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Login_btn.ForeColor = System.Drawing.Color.White
        Me.Login_btn.IconLeftAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Login_btn.IconLeftCursor = System.Windows.Forms.Cursors.Default
        Me.Login_btn.IconLeftPadding = New System.Windows.Forms.Padding(35, 3, 3, 3)
        Me.Login_btn.IconMarginLeft = 11
        Me.Login_btn.IconPadding = 10
        Me.Login_btn.IconRightAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Login_btn.IconRightCursor = System.Windows.Forms.Cursors.Default
        Me.Login_btn.IconRightPadding = New System.Windows.Forms.Padding(3, 3, 7, 3)
        Me.Login_btn.IconSize = 25
        Me.Login_btn.IdleBorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Login_btn.IdleBorderRadius = 30
        Me.Login_btn.IdleBorderThickness = 1
        Me.Login_btn.IdleFillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Login_btn.IdleIconLeftImage = CType(resources.GetObject("Login_btn.IdleIconLeftImage"), System.Drawing.Image)
        Me.Login_btn.IdleIconRightImage = Nothing
        Me.Login_btn.IndicateFocus = True
        Me.Login_btn.Location = New System.Drawing.Point(731, 596)
        Me.Login_btn.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.Login_btn.Name = "Login_btn"
        Me.Login_btn.OnDisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Login_btn.OnDisabledState.BorderRadius = 30
        Me.Login_btn.OnDisabledState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Login_btn.OnDisabledState.BorderThickness = 1
        Me.Login_btn.OnDisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.Login_btn.OnDisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(168, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.Login_btn.OnDisabledState.IconLeftImage = Nothing
        Me.Login_btn.OnDisabledState.IconRightImage = Nothing
        Me.Login_btn.onHoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Login_btn.onHoverState.BorderRadius = 30
        Me.Login_btn.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Login_btn.onHoverState.BorderThickness = 1
        Me.Login_btn.onHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Login_btn.onHoverState.ForeColor = System.Drawing.Color.White
        Me.Login_btn.onHoverState.IconLeftImage = Nothing
        Me.Login_btn.onHoverState.IconRightImage = Nothing
        Me.Login_btn.OnIdleState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Login_btn.OnIdleState.BorderRadius = 30
        Me.Login_btn.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Login_btn.OnIdleState.BorderThickness = 1
        Me.Login_btn.OnIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(39, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Login_btn.OnIdleState.ForeColor = System.Drawing.Color.White
        Me.Login_btn.OnIdleState.IconLeftImage = CType(resources.GetObject("Login_btn.OnIdleState.IconLeftImage"), System.Drawing.Image)
        Me.Login_btn.OnIdleState.IconRightImage = Nothing
        Me.Login_btn.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Login_btn.OnPressedState.BorderRadius = 30
        Me.Login_btn.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid
        Me.Login_btn.OnPressedState.BorderThickness = 1
        Me.Login_btn.OnPressedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(27, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(69, Byte), Integer))
        Me.Login_btn.OnPressedState.ForeColor = System.Drawing.Color.White
        Me.Login_btn.OnPressedState.IconLeftImage = Nothing
        Me.Login_btn.OnPressedState.IconRightImage = Nothing
        Me.Login_btn.Size = New System.Drawing.Size(151, 52)
        Me.Login_btn.TabIndex = 63
        Me.Login_btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Login_btn.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Login_btn.TextMarginLeft = 0
        Me.Login_btn.TextPadding = New System.Windows.Forms.Padding(25, 0, 0, 0)
        Me.Login_btn.UseDefaultRadiusAndThickness = True
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1037, 748)
        Me.Controls.Add(Me.Login_btn)
        Me.Controls.Add(Me.Password_show_chckbox)
        Me.Controls.Add(Me.BunifuLabel9)
        Me.Controls.Add(Me.Password_txtbox)
        Me.Controls.Add(Me.BunifuLabel4)
        Me.Controls.Add(Me.BunifuLabel3)
        Me.Controls.Add(Me.Email_txtbox)
        Me.Controls.Add(Me.BunifuLabel1)
        Me.Controls.Add(Me.BunifuPictureBox2)
        Me.Controls.Add(Me.BunifuPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        Me.BunifuPanel1.ResumeLayout(False)
        Me.BunifuPanel1.PerformLayout()
        CType(Me.BunifuPictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BunifuPictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BunifuPanel1 As Bunifu.UI.WinForms.BunifuPanel
    Friend WithEvents BunifuLabel2 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuPictureBox1 As Bunifu.UI.WinForms.BunifuPictureBox
    Friend WithEvents BunifuLabel1 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuPictureBox2 As Bunifu.UI.WinForms.BunifuPictureBox
    Friend WithEvents Password_show_chckbox As Bunifu.UI.WinForms.BunifuCheckBox
    Friend WithEvents BunifuLabel9 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Password_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents BunifuLabel4 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents BunifuLabel3 As Bunifu.UI.WinForms.BunifuLabel
    Friend WithEvents Email_txtbox As Bunifu.UI.WinForms.BunifuTextBox
    Friend WithEvents Login_btn As Bunifu.UI.WinForms.BunifuButton.BunifuButton
End Class
